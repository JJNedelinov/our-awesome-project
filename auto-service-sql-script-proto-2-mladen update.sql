-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.5.9-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for auto_service
CREATE DATABASE IF NOT EXISTS `auto_service` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `auto_service`;

-- Dumping structure for table auto_service.additional_auth_answers
CREATE TABLE IF NOT EXISTS `additional_auth_answers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `answer` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table auto_service.additional_auth_answers: ~0 rows (approximately)
/*!40000 ALTER TABLE `additional_auth_answers` DISABLE KEYS */;
/*!40000 ALTER TABLE `additional_auth_answers` ENABLE KEYS */;

-- Dumping structure for table auto_service.additional_auth_questions
CREATE TABLE IF NOT EXISTS `additional_auth_questions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question` varchar(255) NOT NULL,
  `additional_auth_answers_id` int(11) NOT NULL,
  PRIMARY KEY (`id`,`additional_auth_answers_id`),
  KEY `fk_additional_auth_questions_additional_auth_answers1_idx` (`additional_auth_answers_id`),
  CONSTRAINT `fk_additional_auth_questions_additional_auth_answers1` FOREIGN KEY (`additional_auth_answers_id`) REFERENCES `additional_auth_answers` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table auto_service.additional_auth_questions: ~0 rows (approximately)
/*!40000 ALTER TABLE `additional_auth_questions` DISABLE KEYS */;
/*!40000 ALTER TABLE `additional_auth_questions` ENABLE KEYS */;

-- Dumping structure for table auto_service.brands
CREATE TABLE IF NOT EXISTS `brands` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table auto_service.brands: ~5 rows (approximately)
/*!40000 ALTER TABLE `brands` DISABLE KEYS */;
INSERT INTO `brands` (`id`, `name`) VALUES
	(5, 'all'),
	(3, 'BMW'),
	(4, 'Mercedes-Benz'),
	(2, 'Opel'),
	(1, 'Skoda');
/*!40000 ALTER TABLE `brands` ENABLE KEYS */;

-- Dumping structure for table auto_service.cars
CREATE TABLE IF NOT EXISTS `cars` (
  `vin` varchar(17) NOT NULL,
  `brand` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  `year` datetime NOT NULL,
  `coupe` varchar(45) NOT NULL,
  `engine` varchar(45) NOT NULL,
  `transmission` varchar(50) NOT NULL,
  `horse_power` int(11) NOT NULL,
  `registration_plate` varchar(20) NOT NULL,
  `car_image` text NOT NULL,
  `customers_id` int(11) NOT NULL,
  PRIMARY KEY (`vin`,`customers_id`),
  UNIQUE KEY `id_UNIQUE` (`vin`),
  KEY `fk_cars_customers1_idx` (`customers_id`),
  CONSTRAINT `fk_cars_customers1` FOREIGN KEY (`customers_id`) REFERENCES `customers` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table auto_service.cars: ~8 rows (approximately)
/*!40000 ALTER TABLE `cars` DISABLE KEYS */;
INSERT INTO `cars` (`vin`, `brand`, `model`, `year`, `coupe`, `engine`, `transmission`, `horse_power`, `registration_plate`, `car_image`, `customers_id`) VALUES
	('KMHMH81VR7U301016', 'Skoda', 'Octavia', '2016-05-17 11:12:34', 'Estate', 'diesel', 'manual', 150, 'CB8899HH', '/link/link', 4),
	('SAJBBABX1JCY66560', 'Mercedes-Benz', 'E-class', '2011-05-17 11:12:34', 'Coupe', 'petrol', 'automatic', 330, 'PK8976TT', '/link/link', 4),
	('VF1RFD00456055504', 'Mercedes-Benz', 'S-class', '2015-05-17 11:12:34', 'Limousine', 'petrol', 'automatic', 415, 'B7777TP', '/link/link', 3),
	('WBAHE11040GE13653', 'BMW', 'M5', '2019-05-17 11:12:34', 'Limousine', 'petrol', 'automatic', 500, 'CA1111CA', '/link/link', 2),
	('WP0ZZZ98ZES110332', 'BMW', '330d', '2018-05-17 11:12:34', 'Estate', 'diesel', 'manual', 250, 'KH4321MM', '/link/link', 3),
	('WVWZZZ1HZXK027782', 'Opel', 'Zafira', '2008-05-17 11:12:34', 'Mini-van', 'petrol', 'manual', 125, 'CB4616BP', '/link/link', 2),
	('YV1AS985681073449', 'Opel', 'Corsa', '2016-05-17 11:12:34', 'Hatch-back', 'diesel', 'manual', 80, 'A5674EE', '/link/link', 4),
	('ZFA1990000P131952', 'Skoda', 'Superb', '2018-05-17 11:12:34', 'Limousine', 'petrol', 'automatic', 180, 'EH2431EE', '/link/link', 2);
/*!40000 ALTER TABLE `cars` ENABLE KEYS */;

-- Dumping structure for table auto_service.customers
CREATE TABLE IF NOT EXISTS `customers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` text NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`) USING HASH
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table auto_service.customers: ~3 rows (approximately)
/*!40000 ALTER TABLE `customers` DISABLE KEYS */;
INSERT INTO `customers` (`id`, `email`, `first_name`, `last_name`, `password`, `created_at`) VALUES
	(2, 'ivan.ivanov@gmail.com', 'Ivan', 'Ivanov', '123', '2021-05-17 11:04:05'),
	(3, 'maria.marinova@gmail.com', 'Maria', 'Marinova', '123', '2021-05-17 11:04:35'),
	(4, 'petya.petrova@gmail.com', 'Petya', 'Petrova', '123', '2021-05-17 11:05:03');
/*!40000 ALTER TABLE `customers` ENABLE KEYS */;

-- Dumping structure for table auto_service.employees
CREATE TABLE IF NOT EXISTS `employees` (
  `id` int(11) NOT NULL,
  `email` text NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table auto_service.employees: ~4 rows (approximately)
/*!40000 ALTER TABLE `employees` DISABLE KEYS */;
INSERT INTO `employees` (`id`, `email`, `first_name`, `last_name`, `password`, `role`) VALUES
	(1, 'niki.nikolov@bestservice.bg', 'Niki', 'Nikolov', '123', 'employee'),
	(2, 'petar.petrov@bestservice.bg', 'Petar', 'Petrov', '123', 'employee'),
	(3, 'zhulien.zhivkov@gmail.com', 'Zhulien', 'Zhivkov', '123', 'admin'),
	(4, 'mladen.tsvetkov@gmail.com', 'Mladen', 'Tsvetkov', '123', 'admin');
/*!40000 ALTER TABLE `employees` ENABLE KEYS */;

-- Dumping structure for table auto_service.models
CREATE TABLE IF NOT EXISTS `models` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `year` datetime NOT NULL,
  `brands_id` int(11) NOT NULL,
  PRIMARY KEY (`id`,`brands_id`),
  KEY `fk_models_brands_idx` (`brands_id`),
  CONSTRAINT `fk_models_brands` FOREIGN KEY (`brands_id`) REFERENCES `brands` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- Dumping data for table auto_service.models: ~9 rows (approximately)
/*!40000 ALTER TABLE `models` DISABLE KEYS */;
INSERT INTO `models` (`id`, `name`, `year`, `brands_id`) VALUES
	(1, 'Superb', '2018-04-17 11:06:23', 1),
	(2, 'Octavia', '2016-05-17 11:06:52', 1),
	(3, 'E-class', '2011-05-17 11:07:22', 4),
	(4, 'S-class', '2015-05-17 11:07:37', 4),
	(5, 'Zafira', '2008-05-17 11:07:53', 2),
	(6, 'Corsa', '2016-05-17 11:08:10', 2),
	(7, '330d', '2018-05-17 11:08:31', 3),
	(8, 'M5', '2019-05-17 11:08:44', 3),
	(9, 'all', '2010-05-17 11:32:36', 5);
/*!40000 ALTER TABLE `models` ENABLE KEYS */;

-- Dumping structure for table auto_service.parts
CREATE TABLE IF NOT EXISTS `parts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `unit` varchar(45) DEFAULT NULL,
  `unit_price` int(11) unsigned DEFAULT NULL,
  `Brand` varchar(45) DEFAULT NULL,
  `product` varchar(45) DEFAULT NULL,
  `car_specific` tinyint(1) DEFAULT NULL,
  `models_id1` int(11) NOT NULL,
  `models_brands_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_parts_models1_idx` (`models_id1`,`models_brands_id`),
  CONSTRAINT `fk_parts_models1` FOREIGN KEY (`models_id1`, `models_brands_id`) REFERENCES `models` (`id`, `brands_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Dumping data for table auto_service.parts: ~6 rows (approximately)
/*!40000 ALTER TABLE `parts` DISABLE KEYS */;
INSERT INTO `parts` (`id`, `name`, `unit`, `unit_price`, `Brand`, `product`, `car_specific`, `models_id1`, `models_brands_id`) VALUES
	(1, 'Brake pads', 'pcs', 220, 'Brembo', '345FSQ', 1, 5, 2),
	(2, 'Engine oil', 'litres', 15, 'Bardal', '10W-40', 0, 9, 5),
	(3, 'Gasket', 'pcs', 5, 'SKF', 'XCVXC565', 0, 9, 5),
	(4, 'Front bumper', 'pcs', 550, 'Mercedes-Benz', 'SXA333', 1, 4, 4),
	(5, 'Summer tire', 'pcs', 180, 'Continental', 'SportMax 4', 0, 9, 5),
	(6, 'Transmission', 'pcs', 2500, 'Skoda', 'BRE123', 1, 1, 1);
/*!40000 ALTER TABLE `parts` ENABLE KEYS */;

-- Dumping structure for table auto_service.parts_history
CREATE TABLE IF NOT EXISTS `parts_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `unit` varchar(45) DEFAULT NULL,
  `unit_price` int(11) unsigned DEFAULT NULL,
  `brand` varchar(45) DEFAULT NULL,
  `product` varchar(45) DEFAULT NULL,
  `service_orders_id` int(11) NOT NULL,
  `total_units` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_parts_history_service_orders1_idx` (`service_orders_id`),
  CONSTRAINT `fk_parts_history_service_orders1` FOREIGN KEY (`service_orders_id`) REFERENCES `service_orders` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table auto_service.parts_history: ~4 rows (approximately)
/*!40000 ALTER TABLE `parts_history` DISABLE KEYS */;
INSERT INTO `parts_history` (`id`, `name`, `unit`, `unit_price`, `brand`, `product`, `service_orders_id`, `total_units`) VALUES
	(1, 'Brake pads', 'pcs', 220, 'Brembo', '345FSQ', 3, 2),
	(2, 'Engine oil', 'litres', 15, 'Bardal', '10W-40', 1, 4),
	(3, 'Gaskets', 'pcs', 5, 'SKF', 'XCVXC565', 1, 2),
	(4, 'Transmission', 'pcs', 2500, 'Mercedes-Benz', 'SXA333', 2, 1);
/*!40000 ALTER TABLE `parts_history` ENABLE KEYS */;

-- Dumping structure for table auto_service.rates
CREATE TABLE IF NOT EXISTS `rates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `price` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table auto_service.rates: ~3 rows (approximately)
/*!40000 ALTER TABLE `rates` DISABLE KEYS */;
INSERT INTO `rates` (`id`, `name`, `price`) VALUES
	(1, 'standard', 50),
	(2, 'advanced', 80),
	(3, 'expert', 120);
/*!40000 ALTER TABLE `rates` ENABLE KEYS */;

-- Dumping structure for table auto_service.rates_history
CREATE TABLE IF NOT EXISTS `rates_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `price` int(11) unsigned DEFAULT NULL,
  `works_history_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_rates_history_works_history1_idx` (`works_history_id`),
  CONSTRAINT `fk_rates_history_works_history1` FOREIGN KEY (`works_history_id`) REFERENCES `works_history` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table auto_service.rates_history: ~5 rows (approximately)
/*!40000 ALTER TABLE `rates_history` DISABLE KEYS */;
INSERT INTO `rates_history` (`id`, `name`, `price`, `works_history_id`) VALUES
	(1, 'standard', 50, 1),
	(2, 'standard', 50, 2),
	(3, 'advanced', 80, 3),
	(4, 'standard', 50, 1),
	(5, 'expert', 120, 5);
/*!40000 ALTER TABLE `rates_history` ENABLE KEYS */;

-- Dumping structure for table auto_service.service_orders
CREATE TABLE IF NOT EXISTS `service_orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` enum('New','Scheduled','In Progress','Quality Check','RFA','Accepted','Paid') DEFAULT NULL,
  `description` text DEFAULT NULL,
  `visit_date` date DEFAULT NULL,
  `cars_vin` varchar(17) NOT NULL,
  `cars_customers_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_service_orders_cars1_idx` (`cars_vin`,`cars_customers_id`),
  CONSTRAINT `fk_service_orders_cars1` FOREIGN KEY (`cars_vin`, `cars_customers_id`) REFERENCES `cars` (`vin`, `customers_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table auto_service.service_orders: ~3 rows (approximately)
/*!40000 ALTER TABLE `service_orders` DISABLE KEYS */;
INSERT INTO `service_orders` (`id`, `status`, `description`, `visit_date`, `cars_vin`, `cars_customers_id`) VALUES
	(1, 'New', 'Regular engine oil change.', '2021-05-17', 'KMHMH81VR7U301016', 4),
	(2, 'Scheduled', 'The car starts but cannot drive.', '2021-05-17', 'SAJBBABX1JCY66560', 4),
	(3, 'Paid', 'Car is unstable when breaking hard.', '2021-05-11', 'KMHMH81VR7U301016', 4);
/*!40000 ALTER TABLE `service_orders` ENABLE KEYS */;

-- Dumping structure for table auto_service.works
CREATE TABLE IF NOT EXISTS `works` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `min_man_hours` int(11) unsigned DEFAULT NULL,
  `rates_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_works_rates1_idx` (`rates_id`),
  CONSTRAINT `fk_works_rates1` FOREIGN KEY (`rates_id`) REFERENCES `rates` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table auto_service.works: ~5 rows (approximately)
/*!40000 ALTER TABLE `works` DISABLE KEYS */;
INSERT INTO `works` (`id`, `name`, `min_man_hours`, `rates_id`) VALUES
	(1, 'Engine oil change', 2, 1),
	(2, 'Brake pads change', 3, 1),
	(3, 'Transmission replacement', 50, 2),
	(4, 'Gasket replacement', 1, 1),
	(5, 'General inspection', 2, 3);
/*!40000 ALTER TABLE `works` ENABLE KEYS */;

-- Dumping structure for table auto_service.works_history
CREATE TABLE IF NOT EXISTS `works_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `man_hours` int(11) unsigned DEFAULT NULL,
  `service_orders_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_works_history_service_orders1_idx` (`service_orders_id`),
  CONSTRAINT `fk_works_history_service_orders1` FOREIGN KEY (`service_orders_id`) REFERENCES `service_orders` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table auto_service.works_history: ~5 rows (approximately)
/*!40000 ALTER TABLE `works_history` DISABLE KEYS */;
INSERT INTO `works_history` (`id`, `name`, `man_hours`, `service_orders_id`) VALUES
	(1, 'Engine oil change', 2, 1),
	(2, 'Brake pads change', 3, 3),
	(3, 'Transmission replacement', 50, 2),
	(4, 'Gasket replacement', 1, 1),
	(5, 'General inspection', 2, 2);
/*!40000 ALTER TABLE `works_history` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
