import constants from "../common/constants.js";

export default {
  id: (value) =>
    value && typeof value === "number" && value > 0 && Number.isInteger(value),
  cars_id: (value) =>
    value && typeof value === "number" && value > 0 && Number.isInteger(value),
  description: (value) =>
    value &&
    typeof value === "string" &&
    value.length <= constants.SERVICE_ORDER.descriptionMaxLength &&
    value.length >= constants.SERVICE_ORDER.descriptionMinLength,
  status: (value) => constants.SERVICE_ORDER.status.has(value),
};
