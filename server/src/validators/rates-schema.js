import constants from "../common/constants.js";


export default {
  id: (value) =>
    value && typeof value === "number" && value > 0 && Number.isInteger(value),
  name: (value) =>
    value &&
    typeof value === "string" &&
    value.length <= constants.CATALOGUE_RATES.nameMaxLength &&
    value.length >= constants.CATALOGUE_RATES.nameMinLength,
  price: (value) =>
    value && typeof value === "number" && value > 0 && Number.isInteger(value),
  deleted: (value) =>
    value &&
    typeof value === "number" &&
    value >= 0 &&
    value <= 1 &&
    Number.isInteger(value),
};
