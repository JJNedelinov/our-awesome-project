import constants from "../common/constants.js";

export default {
  id: (value) =>
    value && typeof value === "number" && value > 0 && Number.isInteger(value),
  name: (value) =>
    value &&
    typeof value === "string" &&
    value.length <= constants.CATALOGUE_WORKS.nameMaxLength &&
    value.length >= constants.CATALOGUE_WORKS.nameMinLength,
  min_man_hours: (value) =>
    value && typeof value === "number" && value > 0 && Number.isInteger(value),
  rates_id: (value) =>
    value && typeof value === "number" && value > 0 && Number.isInteger(value),
  deleted: (value) =>
    value &&
    typeof value === "number" &&
    value >= 0 &&
    value <= 1 &&
    Number.isInteger(value),
};
