import errors from "../common/errors.js";

export default (resource, schema, reqProp) => (req, res, next) => {
  const currErrors = [];

  Object.keys(schema)
    .filter((key) => req.body[key] !== undefined)
    .forEach((key) => {
      if (!schema[key](req[reqProp][key]))
        currErrors.push(errors[resource][key]);
    });

  if (currErrors.length > 0)
    return res.status(400).json({ errors: currErrors });

  next();
};
