import { pool } from './pool.js';

const getAll = async () => {
    try {
        return {
            error: [],
            data: await pool.query('SELECT * FROM parts_history'),
        }
    } catch (error) {
        console.log(error);
        return {
            error: [error],
            data: []
        };
    }
};

const getBy = async (columns, values) => {
    try {
        return {
            error: [],
            data: await pool.query(`SELECT * FROM parts_history WHERE ${columns.map(col => `${col} = ?`).join(' and ')}`, [...values])
        }
    } catch (error) {
        console.log(error);
        return {
            error: [error],
            data: []
        };
    }
};

const searchBy = async (column, value) => {
    try {
        return {
            error: [],
            data: await pool.query(`SELECT * FROM parts_history WHERE ${column} LIKE '%${value}%`),
        }
    } catch (error) {
        console.log(error);
        return {
            error: [error],
            data: []
        };
    }
};

const create = async (name, unit, unitPrice, brand, product, serviceOrdersId, totalUnits) => {
    try {
        const result = await pool.query(
            'INSERT INTO parts_history (name, unit, unit_price, brand, product, service_orders_id, total_units) VALUES (?, ?, ?)',
            [name, unit, unitPrice, brand, product, serviceOrdersId, totalUnits]
        );
        return {
            error: [],
            data: [{
                id: result.insertId,
                name,
                unit,
                unitPrice,
                brand,
                product,
                serviceOrdersId,
                totalUnits
            }]
        }
    } catch (error) {
        console.log(error);
        return {
            error: [error],
            data: []
        };
    }
};

const update = async (columns, id) => {
    try {
        const result = await pool.query(
            `UPDATE parts_history SET ${columns.map(column => `${column} = ?`).join(', ')} insertId=LAST_INSERT_ID(insertId) WHERE id = ?`,
            [columns, id]
        );
        return {
            error: [],
            data: [{
                id: result.insertId,
                //     name,
                //     manHours,
                //     serviceOrderId,
                // }]
            }]
        }
    } catch (error) {
        console.log(error);
        return {
            error: [error],
            data: []
        };
    }
};

const remove = async (id) => {
    try {
        const result = await pool.query(
            `UPDATE parts_history SET deleted = 1 insertId=LAST_INSERT_ID(insertId) WHERE id = ?`,
            [id]
        );
        return {
            error: [],
            data: [{
                id: result.insertId,
                // name,
                // manHours,
                // serviceOrderId,
            }]
        }
    } catch (error) {
        console.log(error);
        return {
            error: [error],
            data: []
        };
    }
};


export default {
    getAll,
    getBy,
    searchBy,
    create,
    update,
    remove
};
