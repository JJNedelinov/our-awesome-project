import { pool } from './pool.js';
import helperFunctions from '../common/helper-functions.js';

const getAll = async () => {
  let sql = `SELECT DISTINCT(cu.id), cu.first_name, cu.last_name, cu.email, cu.phone, cu.deleted
  FROM customers cu\n`;

  try {
    return {
      data: await pool.query(sql),
      error: [],
    };
  } catch (error) {
    return {
      data: [],
      error: [error.message],
    };
  }
};

const getAllByQuery = async (query) => {
  let sql = `SELECT DISTINCT(cu.id), cu.first_name, cu.last_name, cu.email, cu.phone, cu.deleted
  FROM customers cu\n`;

  sql = structureSQL(sql, query);

  try {
    return {
      data: await pool.query(sql),
      error: [],
    };
  } catch (error) {
    return {
      data: [],
      error: [error.message],
    };
  }
};

const getBy = async (columns) => {
  const sql = `SELECT * FROM customers WHERE ${Object.keys(columns)
    .map((col) => `${col} = ?`)
    .join(' AND ')}`;

  try {
    return {
      data: await pool.query(sql, [...Object.values(columns)]),
      error: [],
    };
  } catch (error) {
    return {
      data: [],
      error: [error.message],
    };
  }
};

const create = async ({ email, first_name, last_name, password, phone }) => {
  try {
    const createResult = await pool.query(
      'INSERT INTO customers (email, first_name, last_name, password, phone) VALUES (?, ?, ?, ?, ?)',
      [email, first_name, last_name, password, phone]
    );

    return await getById(createResult.insertId);
  } catch (error) {
    return {
      data: [],
      error: [error.message],
    };
  }
};

const getById = async (customerId) => {
  const sql = `SELECT DISTINCT(cu.id), cu.first_name, cu.last_name, cu.email, cu.phone, cu.deleted, cu.created_at
  FROM customers cu WHERE cu.id = ?`;

  try {
    return {
      data: await pool.query(sql, customerId),
      error: [],
    };
  } catch (error) {
    return {
      data: [],
      error: [error.message],
    };
  }
};

const getByIdFullInfo = async (customerId) => {
  const sql = `SELECT * FROM customers cu WHERE cu.id = ?`;

  try {
    return {
      data: await pool.query(sql, customerId),
      error: [],
    };
  } catch (error) {
    return {
      data: [],
      error: [error.message],
    };
  }
};

const update = async (body, id) => {
  // * everything could be updated ??
  const columns = Object.keys(body).map((key) => `${key} = ?`);

  const sql = `UPDATE customers SET ${columns.join(', ')} WHERE id = ?`;

  try {
    await pool.query(sql, [...Object.values(body), id]);

    return await getById(id);
  } catch (error) {
    return {
      data: [],
      error: [error.message],
    };
  }
};

const remove = async (id) => {
  const sql = 'UPDATE customers SET deleted=1 WHERE id = ?';

  try {
    await pool.query(sql, id);

    return await getById(id);
  } catch (error) {
    return {
      data: [],
      error: [error.message],
    };
  }
};

const renewal = async (id) => {
  const sql = 'UPDATE customers SET deleted=0 WHERE id = ?';

  try {
    await pool.query(sql, id);

    return await getById(id);
  } catch (error) {
    return {
      data: [],
      error: [error.message],
    };
  }
};

const structureSQL = (sql, query) => {
  const userWhere = Object.keys(query).reduce((acc, curr) => {
    switch (curr) {
      case 'name':
        acc.push(
          `CONCAT(cu.first_name, ' ', cu.last_name) LIKE "%${query[curr]}%"`
        );
        return acc;
      case 'email':
      case 'phone':
        acc.push(`cu.${curr} LIKE "%${query[curr]}%"`);
        return acc;
      default:
        return acc;
    }
  }, []);

  const carWhere = Object.keys(query).reduce((acc, curr) => {
    switch (curr) {
      case 'model':
      case 'brand':
        acc.push(`ca.${curr} LIKE "%${query[curr]}%"`);
        return acc;
      default:
        return acc;
    }
  }, []);

  const visitDateWhere = Object.keys(query).reduce((acc, curr) => {
    let year = null;

    if (curr === 'from' || curr === 'to') {
      year = helperFunctions.transformYearInCorrectSQLFormat(query[curr]);
    }

    switch (curr) {
      case 'from':
      case 'to': {
        acc.push(`CAST("${year}" AS DATE)`);
        return acc;
      }
      default:
        return acc;
    }
  }, []);

  // * JOINING THE TABLES

  if (carWhere.length > 0) {
    sql += `
      LEFT JOIN cars ca ON ca.customers_id = cu.id
  `;
  }

  if (visitDateWhere.length > 0) {
    sql += `
      LEFT JOIN service_orders seo ON seo.customers_id = cu.id
    `;
  }

  const userCarWhereClause = `WHERE ${
    userWhere.length > 0 ? userWhere.join(' AND ') : ''
  } ${carWhere.length && userWhere.length > 0 ? ' AND ' : ''} ${
    carWhere.length > 0 ? carWhere.join(' AND ') : ''
  }`;

  let finalWhereClause = '';

  if (userWhere.length > 0 || carWhere.length > 0) {
    finalWhereClause += `${userCarWhereClause} ${
      visitDateWhere.length > 0
        ? ' AND seo.visit_date ' +
          (visitDateWhere.length > 1
            ? `BETWEEN ${visitDateWhere.join(' AND ')}`
            : `${query.from ? '>=' : '<='} ${visitDateWhere[0]}`)
        : ''
    }`;
  } else if (
    userWhere.length <= 0 &&
    carWhere.length <= 0 &&
    visitDateWhere.length > 0
  ) {
    finalWhereClause += `WHERE seo.visit_date ${
      visitDateWhere.length > 1
        ? `BETWEEN (${visitDateWhere.join(' AND ')})`
        : `${query.from ? '>=' : '<='} ${visitDateWhere[0]}`
    }`;
  }

  // sql += finalWhereClause;

  // * SORTING
  // query['order-by'] will be a string
  if (query.hasOwnProperty('order-by')) {
    if (query['order-by'].includes('visit_date')) {
      sql += sql.includes('service_orders')
        ? '\n' + finalWhereClause
        : '\nLEFT JOIN service_orders seo \nON seo.customers_id = cu.id\n' +
          finalWhereClause;
    } else {
      sql += finalWhereClause;
    }

    let conditions = null;

    if (typeof query['order-by'] === 'string') {
      conditions = query['order-by'].split(',').map((condition) => {
        if (condition === 'visit_date') {
          return `seo.${condition}`;
        }
        return `cu.${condition}`;
      });
    } else {
      conditions = query['order-by'].map((condition) => {
        if (condition === 'visit_date') {
          return `seo.${condition}`;
        }
        return `cu.${condition}`;
      });
    }

    sql += `\nORDER BY (${conditions.join(' AND ')})`;

    if (query.hasOwnProperty('order-style')) {
      sql += ` ${query['order-style']}`;
    }
  } else {
    sql += finalWhereClause;
  }

  // * PAGINATION
  sql += `\nLIMIT ${query.offset ?? 0}, ${query.limit ?? 12}`;

  return sql;
};

export default {
  getAll,
  getAllByQuery,
  getById,
  getBy,
  getByIdFullInfo,
  create,
  update,
  remove,
  renewal,
};
