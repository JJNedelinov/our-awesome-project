import { pool } from "./pool.js";

const getAllData = async (tableName, pageStart, pageLimit, isExact, params) => {
  try {
    return {
      error: [],
      data: isExact
        ? await pool.query(
            `SELECT * FROM ${tableName} ${
              params
                ? `WHERE ${Object.entries(params)
                    .filter(([_, value]) => !!value)
                    .map(([key, _]) => `${key} = ?` + " AND ")
                    .join("")}`
                : `WHERE`
            } (deleted IS NULL OR deleted <> 1) LIMIT ${pageStart}, ${
              pageLimit === 0 ? pageStart + 100 : pageStart + pageLimit
            } `,
            params ? Object.values(params).filter((item) => !!item) : []
          )
        : await pool.query(
            `SELECT * FROM ${tableName} ${
              params
                ? `WHERE ${Object.entries(params)
                    .map(([col, value]) => `${col} LIKE '%${value}%'` + " AND ")
                    .join("")} `
                : `WHERE`
            } (deleted IS NULL OR deleted <> 1) LIMIT ${pageStart}, ${
              pageLimit === 0 ? pageStart + 100 : pageStart + pageLimit
            } `
          ),
    };
  } catch (error) {
    console.log(error);
    return {
      error: [error.message],
      data: [],
    };
  }
};

// very alpha version!!! error handling, promise all...
const searchData = async (tableName, searchTerm, pageStart, pageLimit) => {
  const colNames = await pool.query(
    `SELECT COLUMN_NAME 
        FROM INFORMATION_SCHEMA.COLUMNS 
        WHERE TABLE_SCHEMA='auto_service' 
        AND TABLE_NAME='${tableName}';`
  );

  // const getData = () => Promise.all(colNames.map(async item => await pool.query(`SELECT * FROM ${tableName} WHERE ${item.COLUMN_NAME} like '%${searchTerm}%'`)));
  return Promise.all(
    colNames.map(
      async (item) =>
        await pool.query(
          `SELECT * FROM ${tableName}
            WHERE ${item.COLUMN_NAME}
            LIKE '%${searchTerm}%'  AND (deleted IS NULL OR deleted <> 1) `
        )
    )
  )
    .then((data) => ({
      error: [],
      data: data
        .flat()
        .reduce(
          (acc, curr) =>
            acc.find((item) => item.id === curr.id) ? acc : acc.concat(curr),
          []
        )
        .filter(
          (_, index) =>
            index + 1 >= pageStart && index + 1 < pageStart + pageLimit
        ),
    }))
    .catch((response) => ({
      error: [response],
      data: [],
    }));
};

const addData = async (tableName, params) => {
  try {
    const result = await pool.query(
      `INSERT INTO \`${tableName}\` (${Object.keys(params).join(
        ","
      )}) VALUES (${Array.from(
        { length: Object.keys(params).length },
        () => "?"
      ).join(",")})`,
      [...Object.values(params)]
    );

    if (result.affectedRows) {
      return {
        error: [],
        data: [{ ...params, id: result.insertId }],
      };
    }
  } catch (error) {
    console.log(error);
    return {
      error: [error.message],
      data: [],
    };
  }
};

const updateData = async (tableName, id, params) => {
  try {
    const result = await pool.query(
      `UPDATE \`${tableName}\` SET ${Object.entries(params)
        .map(([col, value]) => `\`${col}\` = '${value}'`)
        .join(",")} WHERE id=?`,
      [id]
    );

    if (result.affectedRows) {
      return {
        error: [],
        data: [{ ...params, id }],
      };
    }
  } catch (error) {
    console.log(error);
    return {
      error: [error.message],
      data: [],
    };
  }
};

export default {
  getAllData,
  searchData,
  addData,
  updateData,
};
