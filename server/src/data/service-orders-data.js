import { pool } from './pool.js';

const getAll = async () => {
    try {
        return await pool.query('SELECT * FROM service_orders');
    } catch (error) {
        console.log(error);
        return error;
    }
};

const getBy = async (columns, values) => {
    // console.log(columns);
    // console.log(values);
    // console.log(`SELECT * FROM service_orders WHERE ${columns.map(col => `${col} = ?`).join(' and ')}`);
    // console.log([...values]);
    try {
        return {
            error: [],
            data: await pool.query(`SELECT * FROM service_orders WHERE ${columns.map(col => `${col} = ?`).join(' and ')}`, [...values])
        }
        // console.log(await pool.query(`SELECT * FROM service_orders WHERE ${columns.map(col => `${col} = ?`).join(' and ')}`, [...values]));
    } catch (error) {
        console.log(error);
        return {
            error: [error],
            data: []
        };
    }
};

const searchBy = async (column, value) => {
    try {
        return await pool.query(`SELECT * FROM service_orders WHERE ${column} LIKE '%${value}%`);
    } catch (error) {
        console.log(error);
        return error;
    }
};

const create = async (status, description, visitDate, carId, customerId) => {
    try {
        const result = await pool.query(
            'INSERT INTO service_orders (status, description, visitDate, carId, customerId) VALUES (?, ?, ?, ?)',
            [status, description, visitDate, carId, customerId]
        );
        return {
            id: result.insertId,
            status,
            description,
            visitDate,
            carId,
            customerId
        }
    } catch (error) {
        console.log(error);
        return error;
    }
};

const update = async (columns, id) => {
    try {
        const result = await pool.query(
            `UPDATE service_orders SET ${columns.map(column => `${column} = ?`).join(', ')} insertId=LAST_INSERT_ID(insertId) WHERE id = ?`,
            [columns, id]
        );
        return {
            id: result.insertId,
            status,
            description,
            visitDate,
            carId,
            customerId
        }
    } catch (error) {
        console.log(error);
        return error;
    }
};

const remove = async (id) => {
    try {
        const result = await pool.query(
            `UPDATE service_orders SET deleted = 1 insertId=LAST_INSERT_ID(insertId) WHERE id = ?`,
            [id]
        );
        return {
            id: result.insertId,
            status,
            description,
            visitDate,
            carId,
            customerId
        }
    } catch (error) {
        console.log(error);
        return error;
    }

};


export default {
    getAll,
    getBy,
    searchBy,
    create,
    update,
    remove
};
