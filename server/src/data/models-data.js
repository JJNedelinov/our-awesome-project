import { pool } from './pool.js';

const getAll = async (brands_id) => {
  const sql = `SELECT * FROM models WHERE brands_id = ?`;

  try {
    return {
      data: await pool.query(sql, +brands_id),
      error: [],
    };
  } catch (error) {
    return {
      data: [],
      error: [error.message],
    };
  }
};

const getById = async (modelId) => {
  const sql = `SELECT * FROM models WHERE id = ?`;

  try {
    return {
      data: await pool.query(sql, modelId),
      error: [],
    };
  } catch (error) {
    return {
      data: [],
      error: [error.message],
    };
  }
};

const create = async (body, brands_id) => {
  const sql = `INSERT INTO models SET ${Object.keys({ ...body, brands_id })
    .map((key) => `${key} = ?`)
    .join(', ')}`;

  try {
    const result = await pool.query(sql, [...Object.values(body), brands_id]);

    return await getById(result.insertId);
  } catch (error) {
    return {
      data: [],
      error: [error.message],
    };
  }
};

export default {
  getAll,
  create,
};
