import { pool } from './pool.js';

const getAll = async () => {
    try {
        return {
            error: [],
            data: await pool.query('SELECT * FROM rates'),
        }
    } catch (error) {
        console.log(error);
        return {
            error: [error],
            data: []
        };
    }
};

const getBy = async (columns, values) => {
    try {
        return {
            error: [],
            data: await pool.query(`SELECT * FROM rates WHERE ${columns.map(col => `${col} = ?`).join(' and ')}`, [...values])
        }
    } catch (error) {
        console.log(error);
        return {
            error: [error],
            data: []
        };
    }
};

const searchBy = async (column, value) => {
    try {
        return {
            error: [],
            data: await pool.query(`SELECT * FROM rates WHERE ${column} LIKE '%${value}%`),
        }
    } catch (error) {
        console.log(error);
        return {
            error: [error],
            data: []
        };
    }
};

const create = async (name, manHours, serviceOrderId) => {
    try {
        const result = await pool.query(
            'INSERT INTO rates (name, man_hours, service_order_id) VALUES (?, ?, ?)',
            [name, manHours, serviceOrderId]
        );
        return {
            error: [],
            data: [{
                id: result.insertId,
                name,
                manHours,
                serviceOrderId,
            }]
        }
    } catch (error) {
        console.log(error);
        return {
            error: [error],
            data: []
        };
    }
};

const update = async (columns, id) => {
    try {
        const result = await pool.query(
            `UPDATE rates SET ${columns.map(column => `${column} = ?`).join(', ')} insertId=LAST_INSERT_ID(insertId) WHERE id = ?`,
            [columns, id]
        );
        return {
            error: [],
            data: [{
                id: result.insertId,
                //     name,
                //     manHours,
                //     serviceOrderId,
                // }]
            }]
        }
    } catch (error) {
        console.log(error);
        return {
            error: [error],
            data: []
        };
    }
};

const remove = async (id) => {
    try {
        const result = await pool.query(
            `UPDATE rates SET deleted = 1 insertId=LAST_INSERT_ID(insertId) WHERE id = ?`,
            [id]
        );
        return {
            error: [],
            data: [{
                id: result.insertId,
                // name,
                // manHours,
                // serviceOrderId,
            }]
        }
    } catch (error) {
        console.log(error);
        return {
            error: [error],
            data: []
        };
    }
};


export default {
    getAll,
    getBy,
    searchBy,
    create,
    update,
    remove
};
