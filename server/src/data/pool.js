import mariadb from 'mariadb';
import { DOT_ENV_CONFIG } from '../config.js';

export const pool = mariadb.createPool({
  host: DOT_ENV_CONFIG.DB_HOST,
  port: DOT_ENV_CONFIG.DB_PORT,
  user: DOT_ENV_CONFIG.DB_USER,
  password: DOT_ENV_CONFIG.DB_PASSWORD,
  database: DOT_ENV_CONFIG.DB_NAME,
  connectionLimit: 100,
});
