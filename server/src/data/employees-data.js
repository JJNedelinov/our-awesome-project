import { pool } from './pool.js';

const getAll = async () => {
  let sql = `SELECT * FROM employees \n`;

  try {
    return {
      data: await pool.query(sql),
      error: [],
    };
  } catch (error) {
    return {
      data: [],
      error: [error.message],
    };
  }
};

const getAllByQuery = async (query) => {
  let sql = `SELECT * FROM employees \n`;

  sql = structureSQL(sql, query);

  try {
    return {
      data: await pool.query(sql),
      error: [],
    };
  } catch (error) {
    return {
      data: [],
      error: [error.message],
    };
  }
};

const create = async ({ email, first_name, last_name, phone, password }) => {
  try {
    const createResult = await pool.query(
      'INSERT INTO employees (email, first_name, last_name, phone, password) VALUES (?, ?, ?, ?, ?)',
      [email, first_name, last_name, phone, password]
    );

    return await getById(createResult.insertId);
  } catch (error) {
    return {
      data: {
        employees: [],
      },
      error: [error.message],
    };
  }
};

const getById = async (employeesIdId) => {
  const sql = `SELECT * FROM employees em WHERE em.id = ?`;

  try {
    return {
      data: await pool.query(sql, employeesIdId),
      error: [],
    };
  } catch (error) {
    return {
      data: [],
      error: [error.message],
    };
  }
};

const getBy = async (columns) => {
  const sql = `SELECT * FROM employees WHERE ${Object.keys(columns)
    .map((col) => `${col} = ?`)
    .join(' AND ')}`;

  try {
    return {
      data: await pool.query(sql, [...Object.values(columns)]),
      error: [],
    };
  } catch (error) {
    return {
      data: [],
      error: [error.message],
    };
  }
};

const update = async (body, id) => {
  // * everything could be updated ??
  const columns = Object.keys(body).map((key) => `${key} = ?`);

  const sql = `UPDATE employees SET ${columns.join(', ')} WHERE id = ?`;

  try {
    await pool.query(sql, [...Object.values(body), id]);

    return await getById(id);
  } catch (error) {
    return {
      data: [],
      error: [error.message],
    };
  }
};

const remove = async (id) => {
  const sql = 'UPDATE employees SET deleted=1 WHERE id = ?';

  try {
    await pool.query(sql, id);

    return await getById(id);
  } catch (error) {
    return {
      data: [],
      error: [error.message],
    };
  }
};

const renewal = async (id) => {
  const sql = 'UPDATE employees SET deleted=0 WHERE id = ?';

  try {
    await pool.query(sql, id);

    return await getById(id);
  } catch (error) {
    return {
      data: [],
      error: [error.message],
    };
  }
};

const structureSQL = (sql, query) => {
  const userWhere = Object.keys(query).reduce((acc, emrr) => {
    switch (emrr) {
      case 'name':
        acc.push(`CONCAT(first_name, ' ', last_name) LIKE "%${query[emrr]}%"`);
        return acc;
      case 'email':
      case 'phone':
        acc.push(`${emrr} LIKE "%${query[emrr]}%"`);
        return acc;
      default:
        return acc;
    }
  }, []);

  sql += `${userWhere.length > 0 ? `WHERE ${userWhere.join(' AND ')}` : ''}`;

  // * SORTING
  // query['order-by'] will be a string
  if (query.hasOwnProperty('order-by')) {
    sql += `\nORDER BY (${query['order-by'].split(', ').join(' AND ')})`;

    if (query.hasOwnProperty('order-style')) {
      sql += ` ${query['order-style']}`;
    }
  }

  // * PAGINATION
  sql += `\nLIMIT ${query.offset ?? 0}, ${query.limit ?? 12}`;

  return sql;
};

export default {
  getAll,
  getAllByQuery,
  getById,
  getBy,
  create,
  update,
  remove,
  renewal,
};
