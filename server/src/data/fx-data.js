// manny@currencyconverterapi.com
// 3:06 PM (0 minutes ago)
// to me

import constants from "../common/constants.js";
import fetch from "node-fetch";

// Hi,
// Before you can use the API key, you need to verify your email. Click here to verify.

// Here's your free API key: 9d4b8ee7766526a6fb18

// Example usage:
// https://free.currconv.com/api/v7/convert?q=USD_PHP&compact=ultra&apiKey=9d4b8ee7766526a6fb18

// Best regards,
// Manny

// https://free.currconv.com/api/v7/convert?q=BGN_EUR,BGN_USD&compact=ultra&apiKey=9d4b8ee7766526a6fb18

const getEurUsd = async () => {
  const myHeaders = new fetch.Headers();
  //   myHeaders.append(
  //     "Cookie",
  //     "express:sess=eyJmbGFzaCI6e319; express:sess.sig=43Qe0s9XO9jo_LLT_t1C6HRdmOI"
  //   );

  const requestOptions = {
    method: "GET",
    headers: myHeaders,
    redirect: "follow",
  };
  return (await fetch(
    `https://free.currconv.com/api/v7/convert?q=BGN_EUR,BGN_USD&compact=ultra&apiKey=${constants.FX_API_KEY}`,
    requestOptions
  )).json();
};

export default {
  getEurUsd,
};
