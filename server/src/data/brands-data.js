import helperFunctions from '../common/helper-functions.js';
import { pool } from './pool.js';

const getAll = async () => {
  try {
    const data = await pool.query('SELECT * FROM brands');
    return helperFunctions.createReturnObject(data, []);
  } catch (error) {
    return helperFunctions.createReturnObject([], [error.message]);
  }
};

const getAllByName = async (name) => {
  try {
    const data = await pool.query(
      `SELECT * FROM brands WHERE name LIKE "%${name}%"`
    );
    return helperFunctions.createReturnObject(data, []);
  } catch (error) {
    return helperFunctions.createReturnObject([], [error.message]);
  }
};

const getById = async (id) => {
  try {
    const data = await pool.query('SELECT * FROM brands WHERE id = ?', id);
    return helperFunctions.createReturnObject(data, []);
  } catch (error) {
    return helperFunctions.createReturnObject([], [error.message]);
  }
};

const create = async ({ name }) => {
  try {
    const newBrand = await pool.query(
      'INSERT INTO brands (name) VALUES (?)',
      name
    );

    return await getById(newBrand.insertId);
  } catch (error) {
    return helperFunctions.createReturnObject([], [error.message]);
  }
};

export default {
  getAll,
  getAllByName,
  getById,
  create,
};
