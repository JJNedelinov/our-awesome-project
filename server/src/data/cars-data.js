import helperFunctions from '../common/helper-functions.js';
import { pool } from './pool.js';

const getAll = async () => {
  let sql = 'SELECT * FROM cars';

  try {
    return {
      data: await pool.query(sql),
      error: [],
    };
  } catch (error) {
    return {
      data: [],
      error: [error.message],
    };
  }
};

const getAllByQuery = async (query) => {
  const sql = `SELECT DISTINCT(cu.id) AS customer_id, cu.first_name, cu.last_name, cu.email,
  ca.id AS car_id, ca.brand, ca.model, ca.year, ca.coupe, ca.engine, ca.transmission, ca.horse_power, 
  ca.registration_plate, ca.vin, ca.car_image
  FROM cars ca
  LEFT JOIN customers cu
  ON ca.customers_id = cu.id
  WHERE CONCAT(cu.first_name, ' ', cu.last_name) like "%${query.owner}%";`;

  try {
    return {
      data: await pool.query(sql),
      error: [],
    };
  } catch (error) {
    return {
      data: [],
      error: [error.message],
    };
  }
};

const getAllByCustomerId = async (query) => {
  const sql = `SELECT DISTINCT(cu.id) AS customer_id, cu.first_name, cu.last_name, cu.email,
  ca.id AS car_id, ca.brand, ca.model, ca.year, ca.coupe, ca.engine, ca.transmission, ca.horse_power, 
  ca.registration_plate, ca.vin, ca.car_image
  FROM cars ca
  LEFT JOIN customers cu
  ON ca.customers_id = cu.id
  WHERE ca.customers_id = ?;`;

  try {
    return {
      data: await pool.query(sql, query.customers_id),
      error: [],
    };
  } catch (error) {
    return {
      data: [],
      error: [error.message],
    };
  }
};

const getById = async (id) => {
  const sql = 'SELECT * FROM cars WHERE id = ?';

  try {
    return {
      data: await pool.query(sql, id),
      error: [],
    };
  } catch (error) {
    return {
      data: [],
      error: [error.message],
    };
  }
};

const create = async (body, customers_id) => {
  const {
    brand,
    model,
    coupe,
    engine,
    transmission,
    horse_power,
    registration_plate,
    vin,
    car_image,
  } = body;

  let { year } = body;

  try {
    year = helperFunctions.transformYearInCorrectSQLFormat(year);

    return {
      data: await pool.query(
        'INSERT INTO cars (brand, model, year, coupe, engine, transmission, horse_power, registration_plate, vin, car_image, customers_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)',
        [
          brand,
          model,
          year,
          coupe,
          engine,
          transmission,
          horse_power,
          registration_plate,
          vin,
          car_image,
          Number(customers_id),
        ]
      ),
      error: [],
    };
  } catch (error) {
    return {
      data: [],
      error: [error.message],
    };
  }
};

const update = async (body, carId) => {
  const columns = Object.keys(body).map((key) => `${key} = ?`);

  const sql = `UPDATE cars SET ${columns.join(', ')} WHERE id = ?`;

  try {
    return {
      data: await pool.query(sql, [...Object.values(body), carId]),
      error: [],
    };
  } catch (error) {
    return {
      data: [],
      error: [error.message],
    };
  }
};

export default {
  getAll,
  getAllByQuery,
  getAllByCustomerId,
  getById,
  create,
  update,
};
