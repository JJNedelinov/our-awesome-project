import express from 'express';
import {
  authMiddleware,
  employeeAdminMiddleware,
} from '../auth/auth.middleware.js';
import brandsData from '../data/brands-data.js';
import brandServices from '../services/brand-services.js';

const brandsController = express.Router();

brandsController.get('/', async (req, res) =>
  res.json(await brandServices.getAll(brandsData)(req.query))
);

brandsController.post(
  '/',
  authMiddleware,
  employeeAdminMiddleware,
  async (req, res) => res.json(await brandServices.create(brandsData)(req.body))
);

brandsController.get('/:id', async (req, res) =>
  res.json(await brandServices.getById(brandsData)(req.params.id))
);

export default brandsController;
