import express from "express";
import { authMiddleware } from "../auth/auth.middleware.js";
import RatesHistoryServices from "../services/rates-history-services.js";

const ratesHistoryController = express.Router();

ratesHistoryController.get("/", authMiddleware, async (req, res) => {
  const ratesHistoryItems = await RatesHistoryServices.get();

  res.status(200).send(ratesHistoryItems);
});

ratesHistoryController.post("/", authMiddleware, async (req, res) => {
  const params = req.body;
  const ratesHistoryItems = await RatesHistoryServices.add(params);

  res.status(200).send(ratesHistoryItems);
});

export default ratesHistoryController;
