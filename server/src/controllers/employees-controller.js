import express from 'express';
import bcrypt from 'bcrypt';
import employeesData from '../data/employees-data.js';
import connector from '../email-sender-test/email-sender-test.js';
import employeeServices from '../services/employee-services.js';
import { v4 } from 'uuid';
import { loginInfoTemplate } from '../email-templates/loginInfoTemplate.js';
import { DOT_ENV_CONFIG } from '../config.js';
import createToken from '../auth/create-token.js';
import {
  adminMiddleware,
  authMiddleware,
  employeeAdminMiddleware,
} from '../auth/auth.middleware.js';

const employeesController = express.Router();

employeesController.get(
  '/',
  authMiddleware,
  employeeAdminMiddleware,
  async (req, res) => {
    const result = await employeeServices.getAll(employeesData)(req.query);

    if (result.error.length) return res.status(400).send(result);

    return res.status(200).send(result);
  }
);

employeesController.post(
  '/',
  // authMiddleware,
  // adminMiddleware,
  async (req, res) => {
    const { email, first_name, last_name, phone } = req.body;

    let password = v4().slice(0, 16);

    if (!email || !first_name || !last_name || !password || !phone)
      return res.status(400).send({
        data: [],
        error: ['Аll fields required'],
      });

    const infoObject = { email, name: `${first_name} ${last_name}` };
    infoObject.password = password;

    password = await bcrypt.hash(password, +DOT_ENV_CONFIG.BCRYPT_SALT);

    req.body.password = password;

    const result = await employeeServices.create(employeesData)(req.body);

    if (result.error.length) return res.status(400).send(result);

    await connector
      .post('send', { version: 'v3.1' })
      .request({
        Messages: [
          {
            From: {
              // current authenticated employee/admin details here ??
              Email: 'tip.top.garage007@gmail.com',
              Name: 'JJ & MadJo',
            },
            To: [
              {
                Email: email,
                Name: `${first_name} ${last_name}`,
              },
            ],
            Subject: 'Welcome to TikTop Auto Services.',
            // TextPart: 'Login Information',
            HTMLPart: loginInfoTemplate(infoObject),
            CustomID: 'AppGettingStartedTest',
          },
        ],
      })
      .then(() => console.log('finished'))
      .catch((e) => console.error(e));

    return res.status(200).send(result);
  }
);

employeesController.post('/login', async (req, res) => {
  // ! IF USER DELETED DO NOT ALLOW ACCESS

  if (!Object.keys(req.body).length)
    return res.status(400).send({ data: [], error: ['All fields required!'] });

  const { email, password } = req.body;

  const result = await employeesData.getBy({ email });

  if (!result.data.length)
    return res
      .status(404)
      .send({ data: [], error: ['The email or password is incorrect!'] });

  if (result.data[0].deleted) {
    return res.status(400).send({
      data: [],
      error: [
        'You cannot log in to your account because it was deleted from our database!',
      ],
    });
  }

  const [employee] = result.data;

  const employeePass = employee.password;
  const areEqual = await bcrypt.compare(password, employeePass);

  if (!areEqual)
    return res
      .status(404)
      .send({ data: [], error: ['The email or password is incorrect!'] });

  const payload = {
    sub: employee.id,
    fullName: `${employee.first_name} ${employee.last_name}`,
    role: employee.role,
  };

  const token = createToken(payload);

  return res.status(200).send({ data: [{ token }], error: [] });
});

employeesController.get('/:id', authMiddleware, async (req, res) => {
  const user = req.user;

  if (user.role !== 'admin' && +user.id !== +req.params.id) {
    return res.status(400).send({
      data: [],
      error: ['You are not an admin or the user with that id!'],
    });
  }

  const result = await employeeServices.getById(employeesData)(req.params.id);

  if (result.error.length) return res.status(400).send(result);

  return res.status(200).send(result);
});

employeesController.put(
  '/:id',
  authMiddleware,
  employeeAdminMiddleware,
  async (req, res) => {
    if (!Object.keys(req.body).length) {
      return res.status(400).send({
        data: [],
        error: ['At least one field is required'],
      });
    }

    if (
      'current_password' in req.body &&
      'new_password' in req.body &&
      'repeat_password' in req.body
    ) {
      const employee = await employeeServices.getById(employeesData)(
        req.user.id
      );

      if (
        !(await bcrypt.compare(
          req.body.current_password,
          employee.data[0].password
        ))
      ) {
        return res.status(400).send({
          data: [],
          error: ['Wrong current password!'],
        });
      }

      req.body.password = await bcrypt.hash(
        req.body.new_password,
        +DOT_ENV_CONFIG.BCRYPT_SALT
      );

      delete req.body.current_password;
      delete req.body.new_password;
      delete req.body.repeat_password;
    }

    // check if user role is admin/the user that answers to that id

    const result = await employeeServices.update(employeesData)(
      req.body,
      req.params.id
    );

    if (result.error.length) {
      return res.status(400).send(result);
    }

    return res.status(200).send(result);
  }
);

employeesController.delete(
  '/:id',
  authMiddleware,
  employeeAdminMiddleware,
  async (req, res) => {
    const result = await employeeServices.remove(employeesData)(req.params.id);

    if (result.error.length) {
      return res.status(400).send(result);
    }

    return res.status(200).send(result);
  }
);

employeesController.put(
  '/:id/renewal',
  authMiddleware,
  adminMiddleware,
  async (req, res) => {
    const result = await employeeServices.renewal(employeesData)(req.params.id);

    if (result.error.length) {
      return res.status(400).send(result);
    }

    return res.status(200).send(result);
  }
);

export default employeesController;
