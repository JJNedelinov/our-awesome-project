import express from 'express';
import { authMiddleware } from '../auth/auth.middleware.js';
import FxServices from '../services/fx-services.js';

const fxController = express.Router();
fxController.get('/', authMiddleware, async (_, res) => {
  const result = await FxServices.getEurUsd();

  // console.log(result)

  return res.status(200).send(result);
});

export default fxController;
