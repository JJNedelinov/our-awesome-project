import express from 'express';
import ServiceOrdersServices from '../services/service-orders-services.js';
import serviceOrderSchema from '../validators/service-order-schema.js';
import {
  authMiddleware,
  employeeAdminMiddleware,
} from '../auth/auth.middleware.js';
import validateRequest from '../middlewares/validateRequest.js';

const serviceOrdersController = express.Router();

serviceOrdersController.get('/', authMiddleware, async (req, res) => {
  const params = req.query;
  const search = req.query.search || '';
  const pageStart = +req.query.pageStart || 0;
  const pageLimit = +req.query.pageLimit || 1000;
  const visitDateMin = req.query.visitDateMin
    ? new Date(req.query.visitDateMin)
    : new Date(0);
  const visitDateMax = req.query.visitDateMax
    ? new Date(req.query.visitDateMax)
    : new Date('2100-12-31');
  const isExact = req.query.isExact === 'true' ? true : false;

  delete params.search;
  delete params.pageStart;
  delete params.pageLimit;
  delete params.isExact;
  delete params.visitDateMin;
  delete params.visitDateMax;

  console.log(
    search,
    pageStart,
    pageLimit,
    visitDateMin,
    visitDateMax,
    isExact,
    params
  );

  const serviceOrders = await ServiceOrdersServices.get(
    search,
    pageStart,
    pageLimit,
    visitDateMin,
    visitDateMax,
    isExact,
    params
  );

  res.status(200).send(serviceOrders);
});

serviceOrdersController.post(
  '/',
  authMiddleware,
  employeeAdminMiddleware,
  validateRequest('serviceOrder', serviceOrderSchema, 'body'),
  async (req, res) => {
    const params = req.body;
    const result = await ServiceOrdersServices.add(params);

    res.status(200).send(result);
  }
);

serviceOrdersController.put(
  '/:id',
  authMiddleware,
  employeeAdminMiddleware,
  validateRequest('serviceOrder', serviceOrderSchema, 'body'),
  async (req, res) => {
    const params = req.body;
    const id = req.params.id;
    const result = await ServiceOrdersServices.update(id, params);

    res.status(200).send(result);
  }
);
export default serviceOrdersController;
