import express from 'express';
import {
  authMiddleware,
  employeeAdminMiddleware,
} from '../auth/auth.middleware.js';
import carsData from '../data/cars-data.js';
import carServices from '../services/car-services.js';

const carsController = express.Router();

carsController.get('/', authMiddleware, async (req, res) => {
  const result = await carServices.getAll(carsData)(req.query);

  if (result.error.length) return res.status(400).send(result);

  return res.json(result);
});

carsController.post(
  '/',
  authMiddleware,
  employeeAdminMiddleware,
  async (req, res) => {
    const {
      brand,
      model,
      year,
      coupe,
      engine,
      transmission,
      horse_power,
      registration_plate,
      vin,
      car_image,
    } = req.body;

    const { customers_id } = req.query;

    if (
      !brand ||
      !model ||
      !year ||
      !coupe ||
      !engine ||
      !transmission ||
      !horse_power ||
      !registration_plate ||
      !vin ||
      !car_image
    )
      return res
        .status(400)
        .send({ cars: [], error: { message: 'All fields required' } });

    const result = await carServices.create(carsData)(req.body, customers_id);

    console.log(result);

    if (result.error.length) return res.status(400).send(result);

    return res.status(201).send(result);
  }
);

carsController.get('/:id', authMiddleware, async (req, res) => {
  const carId = +req.params.id;

  return res.json(await carServices.getById(carsData)(carId));
});

carsController.put(
  '/:id',
  authMiddleware,
  employeeAdminMiddleware,
  async (req, res) => {
    const carId = +req.params.id;
    // const { customers_id } = req.query;

    if (!Object.keys(req.body).length) {
      return status(400).json({
        cars: [],
        error: { message: 'All fields required' },
      });
    }

    return res
      .status(201)
      .send(await carServices.update(carsData)(req.body, carId));
  }
);

export default carsController;
