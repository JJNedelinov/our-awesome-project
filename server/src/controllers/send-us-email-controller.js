import express from 'express';
import connector from '../email-sender-test/email-sender-test.js';

const SendUsEmailController = express.Router();

SendUsEmailController.post('/', async (req, res) => {
  const { email, question } = req.body;

  await connector
    .post('send', { version: 'v3.1' })
    .request({
      Messages: [
        {
          From: {
            Email: email,
          },
          To: [
            {
              Email: 'tip.top.garage007@gmail.com',
            },
          ],
          Subject: 'Question',
          TextPart: question,
          CustomID: 'AppGettingStartedTest',
        },
      ],
    })
    .then(() => console.log('finished'))
    .catch((e) => console.error(e));

  return res
    .status(200)
    .send({ data: ['Message was sent successfuly!'], error: [] });
});

export default SendUsEmailController;
