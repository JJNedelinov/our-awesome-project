import express from 'express';
import {
  authMiddleware,
  employeeAdminMiddleware,
} from '../auth/auth.middleware.js';
import helperFunctions from '../common/helper-functions.js';
import modelsData from '../data/models-data.js';
import modelServices from '../services/model-services.js';

const modelsController = express.Router();

modelsController.get('/', async (req, res) => {
  return res.json(await modelServices.getAll(modelsData)(req.query.brands_id));
});

modelsController.post(
  '/',
  authMiddleware,
  employeeAdminMiddleware,
  async (req, res) => {
    if (!req.body.name || !req.body.year) {
      return res
        .status(400)
        .send({ data: {}, error: ['All fields are required'] });
    }

    req.body.year = helperFunctions.transformYearInCorrectSQLFormat(
      req.body.year
    );

    const result = await modelServices.create(modelsData)(
      req.body,
      req.query.brands_id
    );

    if (result.error.length) return res.status(400).send(result);

    return res.status(200).send(result);
  }
);

modelsController.get('/:id', async (req, res) => {
  const result = await modelServices.getById(modelsData)(req.params.id);

  if (result.error.length) return res.status(404).send(result);

  return res.status(200).send(result);
});

export default modelsController;
