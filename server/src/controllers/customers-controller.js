import express from 'express';
import bcrypt from 'bcrypt';
import customersData from '../data/customers-data.js';
import connector from '../email-sender-test/email-sender-test.js';
import customerServices from '../services/customer-services.js';
import { v4 } from 'uuid';
import { loginInfoTemplate } from '../email-templates/loginInfoTemplate.js';
import { DOT_ENV_CONFIG } from '../config.js';
import createToken from '../auth/create-token.js';
import {
  authMiddleware,
  employeeAdminMiddleware,
} from '../auth/auth.middleware.js';

const customersController = express.Router();

customersController.get(
  '/',
  authMiddleware,
  employeeAdminMiddleware,
  async (req, res) => {
    const result = await customerServices.getAll(customersData)(req.query);

    if (result.error.length) return res.status(400).send(result);

    return res.status(200).send(result);
  }
);

customersController.post(
  '/',
  authMiddleware,
  employeeAdminMiddleware,
  async (req, res) => {
    const { email, first_name, last_name, phone } = req.body;

    let password = v4().slice(0, 16);

    if (!email || !first_name || !last_name || !password || !phone)
      return res.status(400).send({
        data: [],
        error: ['Аll fields required'],
      });

    const infoObject = { email, name: `${first_name} ${last_name}` };
    infoObject.password = password;

    password = await bcrypt.hash(password, +DOT_ENV_CONFIG.BCRYPT_SALT);

    req.body.password = password;

    const result = await customerServices.create(customersData)(req.body);

    if (result.error.length) return res.status(400).send(result);

    await connector
      .post('send', { version: 'v3.1' })
      .request({
        Messages: [
          {
            From: {
              Email: 'tip.top.garage007@gmail.com',
              Name: 'JJ & MadJo',
            },
            To: [
              {
                Email: email,
                Name: `${first_name} ${last_name}`,
              },
            ],
            Subject: 'Welcome to TikTop Auto Services.',
            // TextPart: 'Login Information',
            HTMLPart: loginInfoTemplate(infoObject),
            CustomID: 'AppGettingStartedTest',
          },
        ],
      })
      .then(() => console.log('finished'))
      .catch((e) => console.error(e));

    return res.status(200).send(result);
  }
);

customersController.post('/login', async (req, res) => {
  // ! IF USER DELETED DO NOT ALLOW ACCESS

  if (!Object.keys(req.body).length)
    return res.status(400).send({ data: [], error: ['All fields required!'] });

  const { email, password } = req.body;

  const result = await customerServices.getBy(customersData)({ email });

  if (!result.data.length)
    return res
      .status(404)
      .send({ data: [], error: ['The email or password is incorrect!'] });

  if (result.data[0].deleted) {
    return res.status(400).send({
      data: [],
      error: [
        'You cannot log in to your account because it was deleted from our database!',
      ],
    });
  }

  const [customer] = result.data;

  const customerPass = customer.password;
  const areEqual = await bcrypt.compare(password, customerPass);

  if (!areEqual)
    return res
      .status(404)
      .send({ data: [], error: ['The email or password is incorrect!'] });

  const payload = {
    sub: customer.id,
    fullName: `${customer.first_name} ${customer.last_name}`,
    role: customer.role ?? 'customer',
  };

  const token = createToken(payload);

  return res.status(200).send({ data: [{ token }], error: [] });
});

customersController.get('/:id', authMiddleware, async (req, res) => {
  const user = req.user;

  if (
    user.role !== 'admin' &&
    user.role !== 'employee' &&
    +user.id !== +req.params.id
  ) {
    return res.status(400).send({
      data: [],
      error: ['You are not an admin/employee or the user with that id!'],
    });
  }

  const result = await customerServices.getById(customersData)(req.params.id);

  if (result.error.length) return res.status(400).send(result);

  return res.status(200).send(result);
});

customersController.put(
  '/:id',
  authMiddleware,
  // employeeAdminMiddleware,
  async (req, res) => {
    const user = req.user;

    if (user.role !== 'employee' && user.role !== 'admin') {
      if (+user.id !== +req.params.id) {
        return res.status(400).send({
          data: [],
          error: ['You are not allowed to make changes on this user!'],
        });
      }
    }

    if (!Object.keys(req.body).length) {
      return res.status(400).send({
        data: [],
        error: ['At least one fields is required'],
      });
    }

    if (
      'current_password' in req.body &&
      'new_password' in req.body &&
      'repeat_password' in req.body
    ) {
      const customer = await customerServices.getByIdFullInfo(customersData)(
        user.id
      );

      if (
        !(await bcrypt.compare(
          req.body.current_password,
          customer.data[0].password
        ))
      ) {
        return res.status(400).send({
          data: [],
          error: ['Wrong current password!'],
        });
      }

      req.body.password = await bcrypt.hash(
        req.body.new_password,
        +DOT_ENV_CONFIG.BCRYPT_SALT
      );

      delete req.body.current_password;
      delete req.body.new_password;
      delete req.body.repeat_password;
    }

    const result = await customerServices.update(customersData)(
      req.body,
      req.params.id
    );

    if (result.error.length) {
      return res.status(400).send(result);
    }

    return res.status(200).send(result);
  }
);

customersController.delete(
  '/:id',
  authMiddleware,
  employeeAdminMiddleware,
  async (req, res) => {
    const result = await customerServices.remove(customersData)(req.params.id);

    if (result.error.length) {
      return res.status(400).send(result);
    }

    return res.status(200).send(result);
  }
);

customersController.put(
  '/:id/renewal',
  authMiddleware,
  employeeAdminMiddleware,
  async (req, res) => {
    const result = await customerServices.renewal(customersData)(req.params.id);

    if (result.error.length) {
      return res.status(400).send(result);
    }

    return res.status(200).send(result);
  }
);

export default customersController;
