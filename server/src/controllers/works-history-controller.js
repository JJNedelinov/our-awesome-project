import express from "express";
import { authMiddleware } from "../auth/auth.middleware.js";
import WorksHistoryServices from "../services/works-history-services.js";

const worksHistoryController = express.Router();
worksHistoryController.get("/", authMiddleware, async (req, res) => {
  const params = req.query;
  const search = req.query.search || "";
  const pageStart = +req.query.pageStart || 0;
  const pageLimit = +req.query.pageLimit || 1000;
  const isExact = req.query.isExact === "true" ? true : false;

  delete params.search;
  delete params.pageStart;
  delete params.pageLimit;
  delete params.isExact;
  const worksHistoryItems = await WorksHistoryServices.get(
    search,
    pageStart,
    pageLimit,
    isExact,
    params
  );

  res.status(200).send(worksHistoryItems);
});

worksHistoryController.post("/", authMiddleware, async (req, res) => {
  const params = req.body;
  const result = await WorksHistoryServices.add(params);

  res.status(200).send(result);
});
export default worksHistoryController;
