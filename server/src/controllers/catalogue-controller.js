import express from "express";
import {
  authMiddleware,
  employeeAdminMiddleware,
} from "../auth/auth.middleware.js";
import WorksServices from "../services/works-services.js";
import PartsServices from "../services/parts-services.js";
import RatesServices from "../services/rates-services.js";
import EditCreateRate from "../services/edit-create-rate.js";
import EditCreateWork from "../services/edit-create-work.js";
import validateRequest from "../middlewares/validateRequest.js";
import ratesSchema from "../validators/rates-schema.js";
import worksSchema from "../validators/works-schema.js";

const catalogueController = express.Router();

catalogueController.get(
  "/parts",
  authMiddleware,
  employeeAdminMiddleware,
  async (req, res) => {
    const params = req.query;
    const search = req.query.search || "";
    const pageStart = +req.query.pageStart || 0;
    const pageLimit = +req.query.pageLimit || 1000;

    const isExact = req.query.isExact === "true" ? true : false;

    delete params.search;
    delete params.pageStart;
    delete params.pageLimit;
    delete params.isExact;

    const partsItems = await PartsServices.get(
      search,
      pageStart,
      pageLimit,
      isExact,
      params
    );

    res.status(200).send(partsItems);
  }
);

catalogueController.get(
  "/rates",
  authMiddleware,
  employeeAdminMiddleware,
  async (req, res) => {
    const params = req.query;
    const search = req.query.search || "";
    const pageStart = +req.query.pageStart || 0;
    const pageLimit = +req.query.pageLimit || 1000;

    const isExact = req.query.isExact === "true" ? true : false;

    delete params.search;
    delete params.pageStart;
    delete params.pageLimit;
    delete params.isExact;

    const ratesItems = await RatesServices.get(
      search,
      pageStart,
      pageLimit,
      isExact,
      params
    );

    res.status(200).send(ratesItems);
  }
);

catalogueController.post(
  "/rates",
  authMiddleware,
  employeeAdminMiddleware,
  validateRequest("rates", ratesSchema, "body"),
  async (req, res) => {
    const params = req.body;
    const result = await EditCreateRate.add(params);

    res.status(200).send(result);
  }
);

catalogueController.put(
  "/rates/:id",
  authMiddleware,
  employeeAdminMiddleware,
  validateRequest("rates", ratesSchema, "body"),
  async (req, res) => {
    const id = Number(req.params.id);
    const params = req.body;
    const result = await EditCreateRate.update(id, params);

    res.status(200).send(result);
  }
);

catalogueController.get(
  "/works",
  authMiddleware,
  employeeAdminMiddleware,
  async (req, res) => {
    const params = req.query;
    const search = req.query.search || "";
    const pageStart = +req.query.pageStart || 0;
    const pageLimit = +req.query.pageLimit || 1000;

    const isExact = req.query.isExact === "true" ? true : false;

    delete params.search;
    delete params.pageStart;
    delete params.pageLimit;
    delete params.isExact;

    const worksItems = await WorksServices.get(
      search,
      pageStart,
      pageLimit,
      isExact,
      params
    );

    res.status(200).send(worksItems);
  }
);
catalogueController.post(
  "/works",
  authMiddleware,
  employeeAdminMiddleware,
  validateRequest("works", worksSchema, "body"),
  async (req, res) => {
    const params = req.body;
    const result = await EditCreateWork.add(params);

    res.status(200).send(result);
  }
);

catalogueController.put(
  "/works/:id",
  authMiddleware,
  employeeAdminMiddleware,
  validateRequest("works", worksSchema, "body"),
  async (req, res) => {
    const id = Number(req.params.id);
    const params = req.body;
    const result = await EditCreateWork.update(id, params);

    res.status(200).send(result);
  }
);

export default catalogueController;
