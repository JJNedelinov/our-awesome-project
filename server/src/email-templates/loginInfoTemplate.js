/*  
  - ще направя template-a като JS функция, която ще връща темплтейт с дадените данни
  - ще имам един основен темплейт, тоест header and footer и container, който ще пълним
  с различната информация, която може да бъде логин инфото или report, или дори рекламка*/

export const loginInfoTemplate = (info) => {
  return `
  <!DOCTYPE html>
  <html lang="en">
    <head>
      <meta charset="UTF-8" />
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <base href="/" />
      <title>Document</title>
      <style>
        @import url('https://fonts.googleapis.com/css2?family=Chivo:ital,wght@0,300;0,400;0,700;0,900;1,300;1,400;1,700;1,900&display=swap');
  
        *,
        *::before,
        *::after {
          margin: 0;
          padding: 0;
          box-sizing: border-box;
          font-family: 'Chivo', sans-serif;
          color: #434e52;
          margin: auto;
          text-align: center;
        }
        html,
        body {
          max-width: 100%;
          min-width: 100vw;
          max-height: 100%;
          min-height: 100vh;
          margin: 0 auto;
        }
        body {
          padding: 1em;
          display: flex;
          justify-content: center;
          align-items: center;
          text-align: center;
          font-size: 1.2rem;
        }
        hr {
          border-color: #434e52;
          margin: 0.75em auto;
          max-width: 90%;
        }
        h1,
        h2,
        h3,
        h4,
        h5,
        h6 {
          margin: 0.75em auto;
        }
        p {
          font-size: 1.3em;
        }
        .container {
          margin: 0 auto;
          padding: 1em;
          max-width: 100%;
          width: 1000px;
          display: grid;
          grid-template-rows: 200px auto 200px 50px;
          justify-content: center;
          align-items: center;
          border: 2px solid #434e52;
          border-radius: 0.3em;
        }
        .image-container {
          max-width: 100%;
          width: 100%;
          height: auto;
          margin: 0 auto;
        }
        .image-container a {
          display: inline-block;
          width: 100%;
          margin: 0 auto
        }
        .image-container img {
          max-width: 100%;
          width: 500px;
          margin: 0 auto
        }
        .welcoming-words p {
          max-width: 76%;
          line-height: 1.5em;
          margin: 0 auto;
        }
        .welcoming-words-content {
          margin-bottom: 1em;
        }
        p.by-who {
          align-self: flex-end;
        }
        .login-info h1 {
          margin-bottom: 1em;
        }
        .redirect-link a {
          padding: 0.7em 1.3em;
          display: inline-block;
          width: 200px;
          text-decoration: none;
          border-radius: 0.3em;
          color: #434e52;
          font-weight: bold;
          background-color: #88d9fa;
          transition: .1s all linear;
        }
        .redirect-link a:hover {
          color: white;
          box-shadow: 0 0 5px 1px #434e52;
        }
      </style>
    </head>
    <body>
      <div class="container">
        <div class="image-container">
          <a href="http://localhost:3000"
            ><img
              src="https://i.ibb.co/71WhLBS/tip-Top-Garage-Logo.png"
              alt="tip-Top-Garage-Logo"
              border="0"
          /></a>
        </div>
        <!-- <h1>Welcome to TipTop Auto Services</h1> -->
        <div class="welcoming-words">
          <h1>
            Hello and Welcome to <br />One of the Greatest and Most Inovative
            Garages!
          </h1>
          <!-- <hr /> -->
          <div class="welcoming-words-content">
            <p>
              With hands on <b>our hearts</b>, we are happy to tell you that we
              are very <b>pleased</b> and <b>excited</b> that you chose us, <b>${info.name}</b>! It
              could not get any better!
            </p>
            <br />
            <p>
              (<b>if you visit one of the pool parties we host, then it certanly can
                get better</b>)
            </p>
            <br />
            <p>
              If you chose us, you probably already now that the <b>trust</b> here
              is on a pedestal.
            </p>
            <br />
            <p>
              The <b>relationship</b> with the customers and between employees
              inside the company is very <b>important</b> to us. If we were not
              one big <b>family</b>, we could <b>never</b> be here talking with you.
            </p>
            <br />
            <p>
              In this welcoming email we are sending you <b>your</b> login
              information with whcih you will be able to <b>log in</b> to our
              application and follow your made orders.
            </p>
          </div>
          <p class="by-who">by <b>JJ & MadJo</b></p>
        </div>
        <div class="login-info">
          <h1>Your Login Information:</h1>
          <!-- <hr /> -->
          <h2>Email: ${info.email}</h2>
          <h2>Password: ${info.password}</h2>
        </div>
        <div class="redirect-link">
          <a href="http://localhost:3000">Go To Website!</a>
        </div>
      </div>
    </body>
  </html>
  
`;
};
