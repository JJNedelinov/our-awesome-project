const transformYearInCorrectSQLFormat = (year) => {
  year = new Date(year);

  if (year.toString().includes('Invalid'))
    throw new Error(
      'The year you are trying to convert is not an instance of Date'
    );

  return `${year.getFullYear()}-${year.getMonth() + 1}-${year.getDate()}`;
};

const createReturnObject = (data, error) => ({
  data,
  error,
});

export default {
  transformYearInCorrectSQLFormat,
  createReturnObject,
};
