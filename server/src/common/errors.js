const errors = {
  rates: {
    id: "Rate id should be a positive integer.",
    name: "Rate name should be a string with less than 255 characters and more than 3 characters.",
    price: "Price should be a positive integer.",
    deleted: "Price deletion flag should be 0 or 1.",
  },
  works: {
    id: "Work id should be a positive integer.",
    name: "Work name should be a string with less than 255 characters and more than 3 characters.",
    min_man_hours: "Minimum man hours id should be a positive integer.",
    rates_id: "Rate id should be a positive integer.",
    deleted: "Work deletion flag should be 0 or 1.",
  },
  serviceOrder: {
    id: "Service order id should be a positive integer.",
    description:
      "Service order description  should be a string with less than 1000 characters and more than 10 characters.",
    cars_id: "Car id should be a positive integer.",
    status:
      "Status should be one of the following: 'New','Scheduled','In Progress','Quality Check','RFA','Accepted','Paid'.",
  },
};

export default errors;
