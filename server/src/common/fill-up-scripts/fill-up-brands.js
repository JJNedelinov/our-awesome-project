const brands = [
  'Aston Martin',
  'Audi',
  'Bentley',
  'BMW',
  'Buick',
  'Cadillac',
  'Chevrolet',
  'Chrysler',
  'Citroen',
  'Dacia',
  'Dodge',
  'Ferrari',
  'Fiat',
  'Ford',
  'GMC',
  'Honda',
  'Hummer',
  'Hyundai',
  'Infiniti',
  'Isuzu',
  'Jaguar',
  'Jeep',
  'Kia',
  'Lamborghini',
  'Lancia',
  'Land Rover',
  'Lexus',
  'Lincoln',
  'Lotus',
  'Maserati',
  'Mazda',
  'Mercedes-Benz',
  'Mercury',
  'Mini',
  'Mitsubishi',
  'Nissan',
  'Opel',
  'Peugeot',
  'Pontiac',
  'Porsche',
  'Ram',
  'Renault',
  'Renault',
  'Saab',
  'Saturn',
  'Scion',
  'Seat',
  'Skoda',
  'Smart',
  'SsangYong',
  'Subaru',
  'Suzuki',
  'Tesla',
  'Toyota',
  'Volkswagen',
  'Volvo',
  'Wiesmann',
];

const BASE_URL = 'http://localhost:8888';

const fillUpBrands = (brands) => {
  brands.forEach(
    async((brand) => {
      await fetch(`${BASE_URL}/brands`, {
        method: 'POST',
        headers: {
          'Content-type': 'application/json',
        },
        body: JSON.stringify({ name: brand }),
      })
        .then((res) => res.json())
        .then(console.log)
        .catch(console.log);
    })
  );
};
