const TABLES = {
  RATES: "rates",
  WORKS: "works",
  CUSTOMERS: "customers",
  PARTS: "parts",
  WORKS_HISTORY: "works_history",
  PARTS_HISTORY: "parts_history",
  RATES_HISTORY: "rates_history",
  SERVICE_ORDERS: "service_orders",
};

const FX_API_KEY = "f802173c5d1420bcae25";

const CATALOGUE_WORKS = {
  nameMaxLength: 255,
  nameMinLength: 3,
};

const CATALOGUE_RATES = {
  nameMaxLength: 255,
  nameMinLength: 3,
};

const SERVICE_ORDER = {
  descriptionMaxLength: 1000,
  descriptionMinLength: 10,
  status: new Set([
    "New",
    "Scheduled",
    "In Progress",
    "Quality Check",
    "RFA",
    "Accepted",
    "Paid",
  ]),
};

export default {
  TABLES,
  FX_API_KEY,
  CATALOGUE_WORKS,
  CATALOGUE_RATES,
  SERVICE_ORDER,
};
