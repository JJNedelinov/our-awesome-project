import mailjet from 'node-mailjet';
import { DOT_ENV_CONFIG } from '../config.js';

export const connector = mailjet.connect(
  DOT_ENV_CONFIG.MAILJET_ONE,
  DOT_ENV_CONFIG.MAILJET_TWO
);

export default connector;
