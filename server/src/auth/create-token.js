import jwt from 'jsonwebtoken';
import { DOT_ENV_CONFIG } from '../config.js';

const createToken = (payload) => {
  const token = jwt.sign(payload, DOT_ENV_CONFIG.PRIVATE_KEY, {
    expiresIn: `${DOT_ENV_CONFIG.TOKEN_LIFETIME}h`,
  });

  return token;
};

export default createToken;
