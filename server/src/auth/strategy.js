import passportJwt from 'passport-jwt';
import { DOT_ENV_CONFIG } from '../config.js';

const options = {
  secretOrKey: DOT_ENV_CONFIG.PRIVATE_KEY,
  jwtFromRequest: passportJwt.ExtractJwt.fromAuthHeaderAsBearerToken()
};

const jwtStrategy = new passportJwt.Strategy(options, async (payload, done) => {
  const user = {
    id: payload.sub,
    fullName: payload.fullName,
    role: payload.role
  };

  done(null, user);
});

export default jwtStrategy;