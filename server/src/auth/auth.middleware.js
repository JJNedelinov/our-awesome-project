import passport from 'passport';

const authMiddleware = passport.authenticate('jwt', { session: false });

const adminMiddleware = (req, res, next) => {
  if (req.user.role !== 'admin') {
    return res
      .status(403)
      .send({ data: [], error: ['Only admins are allowed to see this page!'] });
  }

  next();
};

const employeeAdminMiddleware = (req, res, next) => {
  if (req.user.role !== 'employee') {
    if (req.user.role !== 'admin') {
      return res.status(403).send({
        data: [],
        error: ['Only admins and employees are allowed to see this page!'],
      });
    }
  }

  next();
};

export { authMiddleware, adminMiddleware, employeeAdminMiddleware };
