const getAll = (modelsData) => async (brands_id) => {
  return await modelsData.getAll(brands_id);
};

const create = (modelsData) => async (body, brands_id) => {
  return await modelsData.create(body, brands_id);
};

const getById = (modelsData) => async (modelId) => {
  return await modelsData.getById(modelId);
};

export default {
  getAll,
  create,
  getById,
};
