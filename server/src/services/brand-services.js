const getAll =
  (brandsData) =>
  async ({ name }) => {
    if (name) return brandsData.getAllByName(name);

    return brandsData.getAll();
  };

const getById = (brandsData) => async (id) => brandsData.getById(id);

const create = (brandsData) => async (body) => {
  return brandsData.create(body);
};

export default {
  getAll,
  getById,
  create,
};
