const getAll = (employeesData) => async (query) => {
  // return await employeesData.getAll(query);

  const queryLength = Object.keys(query).length;

  if (queryLength) {
    return employeesData.getAllByQuery(query);
  }

  return await employeesData.getAll();
};

const getById = (employeesData) => async (id) => {
  return await employeesData.getById(id);
};

const create = (employeesData) => async (body) => {
  return await employeesData.create(body);
};

const update = (employeesData) => async (body, id) => {
  return await employeesData.update(body, id);
};

const remove = (employeesData) => async (id) => {
  return await employeesData.remove(id);
};

const renewal = (employeesData) => async (id) => {
  return await employeesData.renewal(id);
};

export default {
  getAll,
  getById,
  create,
  update,
  remove,
  renewal,
};
