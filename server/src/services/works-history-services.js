import CONSTANTS from "../common/constants.js";
import DataAccess from "../data/data-access.js";

const get = async (
  search = "",
  pageStart = 0,
  pageLimit = 0,
  isExact = false,
  params = null
) => {
  if (search) {
    return await DataAccess.searchData(
      CONSTANTS.TABLES.WORKS_HISTORY,
      search,
      pageStart,
      pageLimit
    );
  } else {
    return await DataAccess.getAllData(
      CONSTANTS.TABLES.WORKS_HISTORY,
      pageStart,
      pageLimit,
      isExact,
      params
    );
  }
};

const add = async (params) => {
  return await DataAccess.addData(CONSTANTS.TABLES.WORKS_HISTORY, params);
};

const update = async (id, params) => {
  return await DataAccess.updateData(
    CONSTANTS.TABLES.WORKS_HISTORY,
    id,
    params
  );
};
export default {
  get,
  add,
  update,
};
