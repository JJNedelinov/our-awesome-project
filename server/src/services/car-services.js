const getAll = (carsData) => async (query) => {
  if (query && Object.keys(query).includes('owner'))
    return await carsData.getAllByQuery(query);

  if (query && query.hasOwnProperty('customers_id'))
    return await carsData.getAllByCustomerId(query);

  if (query && Object.keys(query).length) {
    return { data: [], error: ['Query parameter we do not handle!'] };
  }

  return await carsData.getAll();
};

const getById = (carsData) => async (id) => {
  return await carsData.getById(id);
};

const create = (carsData) => async (body, customers_id) => {
  return await carsData.create(body, customers_id);
};

const update = (carsData) => async (body, customers_id, carId) => {
  return await carsData.update(body, customers_id, carId);
};

export default {
  getAll,
  getById,
  create,
  update,
};
