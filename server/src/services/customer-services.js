const getAll = (customersData) => async (query) => {
  const queryLength = query ? Object.keys(query).length : null;

  if (queryLength) {
    return customersData.getAllByQuery(query);
  }

  return await customersData.getAll();
};

const getBy = (customersData) => async (columns) => {
  return await customersData.getBy(columns);
};

const getById = (customersData) => async (id) => {
  return await customersData.getById(id);
};

const getByIdFullInfo = (customersData) => async (id) => {
  return await customersData.getByIdFullInfo(id);
};

const create = (customersData) => async (body) => {
  return await customersData.create(body);
};

const update = (customersData) => async (body, id) => {
  return await customersData.update(body, id);
};

const remove = (customersData) => async (id) => {
  return await customersData.remove(id);
};

const renewal = (customersData) => async (id) => {
  return await customersData.renewal(id);
};

export default {
  getAll: getAll,
  getBy,
  getById,
  getByIdFullInfo,
  create,
  update,
  remove,
  renewal,
};
