import CONSTANTS from '../common/constants.js';
import DataAccess from '../data/data-access.js';

const get = async (search, pageStart, pageLimit, isExact, params) => {
    if (search) {
        return await DataAccess.searchData(CONSTANTS.TABLES.PARTS, search, pageStart, pageLimit);
    } else {
        return await DataAccess.getAllData(CONSTANTS.TABLES.PARTS, pageStart, pageLimit, isExact, params);
    }
};

export default {
    get,
}