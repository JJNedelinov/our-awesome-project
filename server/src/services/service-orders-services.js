// import ServiceOrdersData from '../data/service-orders-data.js';
import PartsHistoryServices from "./parts-history-services.js";
import WorksHistoryServices from "./works-history-services.js";
import CONSTANTS from "../common/constants.js";
import DataAccess from "../data/data-access.js";
import CarServices from "./car-services.js";
import CustomerServices from "./customer-services.js";
import carsData from "../data/cars-data.js";
import customersData from "../data/customers-data.js";
import RatesHistoryServices from "./rates-history-services.js";

// search, pageStart, pageLimit, visitDateMin, visitDateMax, isExact, params

const get = async (
  search,
  pageStart,
  pageLimit,
  visitDateMin,
  visitDateMax,
  isExact,
  params
) => {
  const serviceOrderObjects = (
    search
      ? await DataAccess.searchData(
          CONSTANTS.TABLES.SERVICE_ORDERS,
          search,
          pageStart,
          pageLimit
        )
      : await DataAccess.getAllData(
          CONSTANTS.TABLES.SERVICE_ORDERS,
          pageStart,
          pageLimit,
          isExact,
          params
        )
  ).data.filter(
    (serviceOrder) =>
      serviceOrder.visit_date.setHours(0, 0, 0, 0) >=
        visitDateMin.setHours(0, 0, 0, 0) &&
      serviceOrder.visit_date.setHours(0, 0, 0, 0) <=
        visitDateMax.setHours(0, 0, 0, 0)
  );

  const worksHistoryObjects = [...(await WorksHistoryServices.get()).data];
  const partsHistoryObjects = [...(await PartsHistoryServices.get()).data];
  const ratesHistoryObjects = [...(await RatesHistoryServices.get()).data];
  const carsObjects = await CarServices.getAll(carsData)();
  const customersObjects = await CustomerServices.getAll(customersData)();

  return {
    error: [],
    data: serviceOrderObjects.map((serviceOrder) => {
      return {
        serviceOrder: serviceOrder,
        car: carsObjects.data.filter(
          (carItem) => carItem.id === serviceOrder.cars_id
        )[0],
        customer: customersObjects.data.filter(
          (customerItem) => customerItem.id === serviceOrder.customers_id
        )[0],
        works: worksHistoryObjects.filter(
          (worksHistoryItem) =>
            worksHistoryItem.service_orders_id === serviceOrder.id
        ),
        rates: worksHistoryObjects
          .filter(
            (worksHistoryItem) =>
              worksHistoryItem.service_orders_id === serviceOrder.id
          )
          .map((fltWork) =>
            ratesHistoryObjects.filter(
              (ratItem) => ratItem.works_history_id === fltWork.id
            )
          )
          .flat(),
        parts: partsHistoryObjects.filter(
          (partsHistoryItem) =>
            partsHistoryItem.service_orders_id === serviceOrder.id
        ),
      };
    }),
  };
};
const add = async (params) => {
  return await DataAccess.addData(CONSTANTS.TABLES.SERVICE_ORDERS, params);
};

const update = async (id, params) => {
  return await DataAccess.updateData(
    CONSTANTS.TABLES.SERVICE_ORDERS,
    id,
    params
  );
};
export default {
  get,
  add,
  update,
};
