import FxData from "../data/fx-data.js";

const getEurUsd = async () => {
  const result = await FxData.getEurUsd();

  return {
    data: [
      {
        bgnToUSD: result.BGN_USD,
        bgnToEUR: result.BGN_EUR,
        timeStamp: new Date().toLocaleString(),
      },
    ],
    error: [],
  };
};

export default {
  getEurUsd,
};
