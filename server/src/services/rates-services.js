import CONSTANTS from '../common/constants.js';
import DataAccess from '../data/data-access.js';

const get = async (search, pageStart, pageEnd, isExact, params) => {
    if (search) {
        return await DataAccess.searchData(CONSTANTS.TABLES.RATES, search, pageStart, pageEnd);
    } else {
        return await DataAccess.getAllData(CONSTANTS.TABLES.RATES, pageStart, pageEnd, isExact, params);
    }
};

export default {
    get,
}