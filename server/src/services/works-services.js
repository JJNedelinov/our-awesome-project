import CONSTANTS from '../common/constants.js';
import DataAccess from '../data/data-access.js';
import RatesServices from './rates-services.js';

const get = async (search, pageStart, pageEnd, isExact, params) => {
    if (search) {
        return await DataAccess.searchData(CONSTANTS.TABLES.WORKS, search, pageStart, pageEnd);
    } else {
        return await DataAccess.getAllData(CONSTANTS.TABLES.WORKS, pageStart, pageEnd, isExact, params);
    }
};

export default {
    get,
}