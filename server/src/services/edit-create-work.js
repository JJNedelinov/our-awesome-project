import CONSTANTS from '../common/constants.js';
import DataAccess from '../data/data-access.js';

const add = async (params) => {
    return await DataAccess.addData(CONSTANTS.TABLES.WORKS, params);
};

const update = async (id, params) => {
    return await DataAccess.updateData(CONSTANTS.TABLES.WORKS, id, params);
};
export default {
    add,
    update
}