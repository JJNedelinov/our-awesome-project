import express from 'express';
import cors from 'cors';
import helmet from 'helmet';
import passport from 'passport';
import jwtStrategy from './auth/strategy.js';
import customersController from './controllers/customers-controller.js';
import employeesController from './controllers/employees-controller.js';
import carsController from './controllers/cars-controller.js';
import brandsController from './controllers/brands-controller.js';
import serviceOrdersController from './controllers/service-orders-controller.js';
import modelsController from './controllers/models-controller.js';
import worksHistoryController from './controllers/works-history-controller.js';
import partsHistoryController from './controllers/parts-history-controller.js';
import catalogueController from './controllers/catalogue-controller.js';
import fxController from './controllers/fx-controller.js';
import ratesHistoryController from './controllers/rates-history-controller.js';
import SendUsEmailController from './controllers/send-us-email-controller.js';

const port = 8888;

const app = express();

passport.use(jwtStrategy);
app.use(passport.initialize());

app.use(express.json());
app.use(cors());
app.use(helmet());

app.use('/customers', customersController);
app.use('/employees', employeesController);
app.use('/cars', carsController);
app.use('/brands', brandsController);
app.use('/models', modelsController);
app.use('/service-orders', serviceOrdersController);
app.use('/works-history', worksHistoryController);
app.use('/parts-history', partsHistoryController);
app.use('/rates-history', ratesHistoryController);
app.use('/catalogue', catalogueController);
app.use('/fx', fxController);
app.use('/send-us-email', SendUsEmailController);

app.get('/', (req, res) => res.json({ message: 'hi' }));

app.listen(port, 'localhost', () =>
  console.log('Server is listening on port:', port)
);
