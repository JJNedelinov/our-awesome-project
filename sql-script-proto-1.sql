-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema auto_service
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema auto_service
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `auto_service` DEFAULT CHARACTER SET latin1 ;
-- -----------------------------------------------------
-- Schema auto_service_prototype
-- -----------------------------------------------------
USE `auto_service` ;

-- -----------------------------------------------------
-- Table `auto_service`.`cars`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `auto_service`.`cars` (
  `vin` VARCHAR(17) NOT NULL,
  `brand` VARCHAR(100) NOT NULL,
  `model` VARCHAR(100) NOT NULL,
  `year` DATETIME NOT NULL,
  `coupe` VARCHAR(45) NOT NULL,
  `engine` VARCHAR(45) NOT NULL,
  `transmission` VARCHAR(50) NOT NULL,
  `horse_power` INT NOT NULL,
  `registration_plate` VARCHAR(20) NOT NULL,
  `customers_id` INT NOT NULL,
  PRIMARY KEY (`vin`, `customers_id`),
  UNIQUE INDEX `id_UNIQUE` (`vin` ASC) VISIBLE,
  INDEX `fk_cars_customers1_idx` (`customers_id` ASC) VISIBLE,
  CONSTRAINT `fk_cars_customers1`
    FOREIGN KEY (`customers_id`)
    REFERENCES `auto_service`.`customers` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `auto_service`.`customers`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `auto_service`.`customers` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `email` TEXT(320) NOT NULL,
  `first_name` VARCHAR(100) NOT NULL,
  `last_name` VARCHAR(100) NOT NULL,
  `password` VARCHAR(255) NOT NULL,
  `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `auto_service`.`cars`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `auto_service`.`cars` (
  `vin` VARCHAR(17) NOT NULL,
  `brand` VARCHAR(100) NOT NULL,
  `model` VARCHAR(100) NOT NULL,
  `year` DATETIME NOT NULL,
  `coupe` VARCHAR(45) NOT NULL,
  `engine` VARCHAR(45) NOT NULL,
  `transmission` VARCHAR(50) NOT NULL,
  `horse_power` INT NOT NULL,
  `registration_plate` VARCHAR(20) NOT NULL,
  `customers_id` INT NOT NULL,
  PRIMARY KEY (`vin`, `customers_id`),
  UNIQUE INDEX `id_UNIQUE` (`vin` ASC) VISIBLE,
  INDEX `fk_cars_customers1_idx` (`customers_id` ASC) VISIBLE,
  CONSTRAINT `fk_cars_customers1`
    FOREIGN KEY (`customers_id`)
    REFERENCES `auto_service`.`customers` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `auto_service`.`brands`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `auto_service`.`brands` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `auto_service`.`models`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `auto_service`.`models` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `brands_id` INT NOT NULL,
  PRIMARY KEY (`id`, `brands_id`),
  INDEX `fk_models_brands_idx` (`brands_id` ASC) VISIBLE,
  CONSTRAINT `fk_models_brands`
    FOREIGN KEY (`brands_id`)
    REFERENCES `auto_service`.`brands` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `auto_service`.`additional_auth_answers`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `auto_service`.`additional_auth_answers` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `answer` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `auto_service`.`additional_auth_questions`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `auto_service`.`additional_auth_questions` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `question` VARCHAR(255) NOT NULL,
  `additional_auth_answers_id` INT NOT NULL,
  PRIMARY KEY (`id`, `additional_auth_answers_id`),
  INDEX `fk_additional_auth_questions_additional_auth_answers1_idx` (`additional_auth_answers_id` ASC) VISIBLE,
  CONSTRAINT `fk_additional_auth_questions_additional_auth_answers1`
    FOREIGN KEY (`additional_auth_answers_id`)
    REFERENCES `auto_service`.`additional_auth_answers` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `auto_service`.`employees`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `auto_service`.`employees` (
  `id` INT NOT NULL,
  `email` TEXT(320) NOT NULL,
  `first_name` VARCHAR(100) NOT NULL,
  `last_name` VARCHAR(100) NOT NULL,
  `password` VARCHAR(255) NOT NULL,
  `role` VARCHAR(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
