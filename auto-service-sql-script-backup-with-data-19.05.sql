-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.5.9-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for auto_service
CREATE DATABASE IF NOT EXISTS `auto_service` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `auto_service`;

-- Dumping structure for table auto_service.additional_auth_answers
CREATE TABLE IF NOT EXISTS `additional_auth_answers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `answer` varchar(255) NOT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table auto_service.additional_auth_answers: ~0 rows (approximately)
/*!40000 ALTER TABLE `additional_auth_answers` DISABLE KEYS */;
/*!40000 ALTER TABLE `additional_auth_answers` ENABLE KEYS */;

-- Dumping structure for table auto_service.additional_auth_questions
CREATE TABLE IF NOT EXISTS `additional_auth_questions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question` varchar(255) NOT NULL,
  `additional_auth_answers_id` int(11) NOT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`,`additional_auth_answers_id`),
  KEY `fk_additional_auth_questions_additional_auth_answers1_idx` (`additional_auth_answers_id`),
  CONSTRAINT `fk_additional_auth_questions_additional_auth_answers1` FOREIGN KEY (`additional_auth_answers_id`) REFERENCES `additional_auth_answers` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table auto_service.additional_auth_questions: ~0 rows (approximately)
/*!40000 ALTER TABLE `additional_auth_questions` DISABLE KEYS */;
/*!40000 ALTER TABLE `additional_auth_questions` ENABLE KEYS */;

-- Dumping structure for table auto_service.brands
CREATE TABLE IF NOT EXISTS `brands` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=351 DEFAULT CHARSET=latin1;

-- Dumping data for table auto_service.brands: ~0 rows (approximately)
/*!40000 ALTER TABLE `brands` DISABLE KEYS */;
INSERT INTO `brands` (`id`, `name`, `deleted`) VALUES
	(1, 'Skoda', NULL),
	(2, 'Opel', NULL),
	(3, 'BMW', NULL),
	(4, 'Mercedes-Benz', NULL),
	(5, 'all', NULL),
	(296, 'Abarth', NULL),
	(297, 'Acura', NULL),
	(298, 'Alfa Romeo', NULL),
	(299, 'Aston Martin', NULL),
	(300, 'Audi', NULL),
	(301, 'Bentley', NULL),
	(302, 'Buick', NULL),
	(303, 'Cadillac', NULL),
	(304, 'Chevrolet', NULL),
	(305, 'Chrysler', NULL),
	(306, 'Citroen', NULL),
	(307, 'Dacia', NULL),
	(308, 'Dodge', NULL),
	(309, 'Ferrari', NULL),
	(310, 'Fiat', NULL),
	(311, 'Ford', NULL),
	(312, 'GMC', NULL),
	(313, 'Honda', NULL),
	(314, 'Hummer', NULL),
	(315, 'Hyundai', NULL),
	(316, 'Infiniti', NULL),
	(317, 'Isuzu', NULL),
	(318, 'Jaguar', NULL),
	(319, 'Jeep', NULL),
	(320, 'Kia', NULL),
	(321, 'Lamborghini', NULL),
	(322, 'Lancia', NULL),
	(323, 'Land Rover', NULL),
	(324, 'Lexus', NULL),
	(325, 'Lincoln', NULL),
	(326, 'Lotus', NULL),
	(327, 'Maserati', NULL),
	(328, 'Mazda', NULL),
	(329, 'Mercury', NULL),
	(330, 'Mini', NULL),
	(331, 'Mitsubishi', NULL),
	(332, 'Nissan', NULL),
	(333, 'Peugeot', NULL),
	(334, 'Pontiac', NULL),
	(335, 'Porsche', NULL),
	(336, 'Ram', NULL),
	(337, 'Renault', NULL),
	(338, 'Saab', NULL),
	(339, 'Saturn', NULL),
	(340, 'Scion', NULL),
	(341, 'Seat', NULL),
	(342, 'Smart', NULL),
	(343, 'SsangYong', NULL),
	(344, 'Subaru', NULL),
	(345, 'Suzuki', NULL),
	(346, 'Tesla', NULL),
	(347, 'Toyota', NULL),
	(348, 'Volkswagen', NULL),
	(349, 'Volvo', NULL),
	(350, 'Wiesmann', NULL);
/*!40000 ALTER TABLE `brands` ENABLE KEYS */;

-- Dumping structure for table auto_service.cars
CREATE TABLE IF NOT EXISTS `cars` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `brand` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  `year` datetime NOT NULL,
  `coupe` varchar(45) NOT NULL,
  `engine` varchar(45) NOT NULL,
  `transmission` varchar(50) NOT NULL,
  `horse_power` int(11) NOT NULL,
  `registration_plate` varchar(20) NOT NULL,
  `vin` varchar(17) NOT NULL,
  `car_image` text NOT NULL,
  `customers_id` int(11) NOT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`vin`),
  UNIQUE KEY `registration_plate_UNIQUE` (`registration_plate`),
  UNIQUE KEY `cars_id_UNIQUE` (`id`),
  KEY `fk_cars_customers1_idx` (`customers_id`),
  CONSTRAINT `fk_cars_customers1` FOREIGN KEY (`customers_id`) REFERENCES `customers` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

-- Dumping data for table auto_service.cars: ~0 rows (approximately)
/*!40000 ALTER TABLE `cars` DISABLE KEYS */;
INSERT INTO `cars` (`id`, `brand`, `model`, `year`, `coupe`, `engine`, `transmission`, `horse_power`, `registration_plate`, `vin`, `car_image`, `customers_id`, `deleted`) VALUES
	(12, 'Skoda', 'Octavia', '2016-05-17 11:12:34', 'Estate', 'diesel', 'manual', 150, 'CB8899HH', 'KMHMH81VR7U301016', '/link/link', 4, NULL),
	(13, 'Mercedes-Benz', 'E-class', '2011-05-17 11:12:34', 'Coupe', 'petrol', 'automatic', 330, 'PK8976TT', 'SAJBBABX1JCY66560', '/link/link', 4, NULL),
	(14, 'Mercedes-Benz', 'S-class', '2015-05-17 11:12:34', 'Limousine', 'petrol', 'automatic', 415, 'B7777TP', 'VF1RFD00456055504', '/link/link', 3, NULL),
	(15, 'BMW', 'M5', '2019-05-17 11:12:34', 'Limousine', 'petrol', 'automatic', 500, 'CA1111CA', 'WBAHE11040GE13653', '/link/link', 2, NULL),
	(16, 'BMW', '330d', '2018-05-17 11:12:34', 'Estate', 'diesel', 'manual', 250, 'KH4321MM', 'WP0ZZZ98ZES110332', '/link/link', 3, NULL),
	(17, 'Opel', 'Zafira', '2008-05-17 11:12:34', 'Mini-van', 'petrol', 'manual', 125, 'CB4616BP', 'WVWZZZ1HZXK027782', '/link/link', 2, NULL),
	(18, 'Opel', 'Corsa', '2016-05-17 11:12:34', 'Hatch-back', 'diesel', 'manual', 80, 'A5674EE', 'YV1AS985681073449', '/link/link', 4, NULL),
	(19, 'Skoda', 'Superb', '2018-05-17 11:12:34', 'Limousine', 'petrol', 'automatic', 180, 'EH2431EE', 'ZFA1990000P131952', '/link/link', 2, NULL);
/*!40000 ALTER TABLE `cars` ENABLE KEYS */;

-- Dumping structure for table auto_service.customers
CREATE TABLE IF NOT EXISTS `customers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` text NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `phone` varchar(10) NOT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `phone_UNIQUE` (`phone`),
  UNIQUE KEY `email_UNIQUE` (`email`) USING HASH
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table auto_service.customers: ~0 rows (approximately)
/*!40000 ALTER TABLE `customers` DISABLE KEYS */;
INSERT INTO `customers` (`id`, `email`, `first_name`, `last_name`, `password`, `phone`, `created_at`, `deleted`) VALUES
	(2, 'ivan.ivanov@gmail.com', 'Ivan', 'Ivanov', '123', '0888222222', '2021-05-17 11:04:05', NULL),
	(3, 'maria.marinova@gmail.com', 'Maria', 'Marinova', '123', '0888333333', '2021-05-17 11:04:35', NULL),
	(4, 'petya.petrova@gmail.com', 'Petya', 'Petrova', '123', '0888444444', '2021-05-17 11:05:03', NULL);
/*!40000 ALTER TABLE `customers` ENABLE KEYS */;

-- Dumping structure for table auto_service.employees
CREATE TABLE IF NOT EXISTS `employees` (
  `id` int(11) NOT NULL,
  `email` text NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` varchar(20) NOT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table auto_service.employees: ~0 rows (approximately)
/*!40000 ALTER TABLE `employees` DISABLE KEYS */;
INSERT INTO `employees` (`id`, `email`, `first_name`, `last_name`, `password`, `role`, `deleted`) VALUES
	(1, 'niki.nikolov@bestservice.bg', 'Niki', 'Nikolov', '123', 'employee', NULL),
	(2, 'petar.petrov@bestservice.bg', 'Petar', 'Petrov', '123', 'employee', NULL),
	(3, 'zhulien.zhivkov@gmail.com', 'Zhulien', 'Zhivkov', '123', 'admin', NULL),
	(4, 'mladen.tsvetkov@gmail.com', 'Mladen', 'Tsvetkov', '123', 'admin', NULL);
/*!40000 ALTER TABLE `employees` ENABLE KEYS */;

-- Dumping structure for table auto_service.models
CREATE TABLE IF NOT EXISTS `models` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `year` datetime NOT NULL,
  `brands_id` int(11) NOT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_models_brands_idx` (`brands_id`),
  CONSTRAINT `fk_models_brands` FOREIGN KEY (`brands_id`) REFERENCES `brands` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- Dumping data for table auto_service.models: ~0 rows (approximately)
/*!40000 ALTER TABLE `models` DISABLE KEYS */;
INSERT INTO `models` (`id`, `name`, `year`, `brands_id`, `deleted`) VALUES
	(1, 'Superb', '2018-04-17 11:06:23', 1, NULL),
	(2, 'Octavia', '2016-05-17 11:06:52', 1, NULL),
	(3, 'E-class', '2011-05-17 11:07:22', 4, NULL),
	(4, 'S-class', '2015-05-17 11:07:37', 4, NULL),
	(5, 'Zafira', '2008-05-17 11:07:53', 2, NULL),
	(6, 'Corsa', '2016-05-17 11:08:10', 2, NULL),
	(7, '330d', '2018-05-17 11:08:31', 3, NULL),
	(8, 'M5', '2019-05-17 11:08:44', 3, NULL),
	(9, 'all', '2010-05-17 11:32:36', 5, NULL);
/*!40000 ALTER TABLE `models` ENABLE KEYS */;

-- Dumping structure for table auto_service.parts
CREATE TABLE IF NOT EXISTS `parts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `unit` varchar(45) DEFAULT NULL,
  `unit_price` int(11) unsigned DEFAULT NULL,
  `Brand` varchar(45) DEFAULT NULL,
  `product` varchar(45) DEFAULT NULL,
  `car_specific` tinyint(1) DEFAULT NULL,
  `models_id` int(11) NOT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_parts_models1_idx` (`models_id`),
  CONSTRAINT `fk_parts_models1` FOREIGN KEY (`models_id`) REFERENCES `models` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Dumping data for table auto_service.parts: ~0 rows (approximately)
/*!40000 ALTER TABLE `parts` DISABLE KEYS */;
INSERT INTO `parts` (`id`, `name`, `unit`, `unit_price`, `Brand`, `product`, `car_specific`, `models_id`, `deleted`) VALUES
	(1, 'Brake pads', 'pcs', 220, 'Brembo', '345FSQ', 1, 5, NULL),
	(2, 'Engine oil', 'litres', 15, 'Bardal', '10W-40', 0, 9, NULL),
	(3, 'Gasket', 'pcs', 5, 'SKF', 'XCVXC565', 0, 9, NULL),
	(4, 'Front bumper', 'pcs', 550, 'Mercedes-Benz', 'SXA333', 1, 4, NULL),
	(5, 'Summer tire', 'pcs', 180, 'Continental', 'SportMax 4', 0, 9, NULL),
	(6, 'Transmission', 'pcs', 2500, 'Skoda', 'BRE123', 1, 1, NULL);
/*!40000 ALTER TABLE `parts` ENABLE KEYS */;

-- Dumping structure for table auto_service.parts_history
CREATE TABLE IF NOT EXISTS `parts_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `unit` varchar(45) DEFAULT NULL,
  `unit_price` int(11) unsigned DEFAULT NULL,
  `brand` varchar(45) DEFAULT NULL,
  `product` varchar(45) DEFAULT NULL,
  `service_orders_id` int(11) NOT NULL,
  `total_units` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_parts_history_service_orders1_idx` (`service_orders_id`),
  CONSTRAINT `fk_parts_history_service_orders1` FOREIGN KEY (`service_orders_id`) REFERENCES `service_orders` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table auto_service.parts_history: ~0 rows (approximately)
/*!40000 ALTER TABLE `parts_history` DISABLE KEYS */;
INSERT INTO `parts_history` (`id`, `name`, `unit`, `unit_price`, `brand`, `product`, `service_orders_id`, `total_units`, `deleted`) VALUES
	(1, 'Brake pads', 'pcs', 220, 'Brembo', '345FSQ', 3, 2, NULL),
	(2, 'Engine oil', 'litres', 15, 'Bardal', '10W-40', 1, 4, NULL),
	(3, 'Gaskets', 'pcs', 5, 'SKF', 'XCVXC565', 1, 2, NULL),
	(4, 'Transmission', 'pcs', 2500, 'Mercedes-Benz', 'SXA333', 2, 1, NULL);
/*!40000 ALTER TABLE `parts_history` ENABLE KEYS */;

-- Dumping structure for table auto_service.rates
CREATE TABLE IF NOT EXISTS `rates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `price` int(11) unsigned DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table auto_service.rates: ~0 rows (approximately)
/*!40000 ALTER TABLE `rates` DISABLE KEYS */;
INSERT INTO `rates` (`id`, `name`, `price`, `deleted`) VALUES
	(1, 'standard', 50, NULL),
	(2, 'advanced', 80, NULL),
	(3, 'expert', 120, NULL);
/*!40000 ALTER TABLE `rates` ENABLE KEYS */;

-- Dumping structure for table auto_service.rates_history
CREATE TABLE IF NOT EXISTS `rates_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `price` int(11) unsigned DEFAULT NULL,
  `works_history_id` int(11) NOT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_rates_history_works_history1_idx` (`works_history_id`),
  CONSTRAINT `fk_rates_history_works_history1` FOREIGN KEY (`works_history_id`) REFERENCES `works_history` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table auto_service.rates_history: ~0 rows (approximately)
/*!40000 ALTER TABLE `rates_history` DISABLE KEYS */;
INSERT INTO `rates_history` (`id`, `name`, `price`, `works_history_id`, `deleted`) VALUES
	(1, 'standard', 50, 1, NULL),
	(2, 'standard', 50, 2, NULL),
	(3, 'advanced', 80, 3, NULL),
	(4, 'standard', 50, 1, NULL),
	(5, 'expert', 120, 5, NULL);
/*!40000 ALTER TABLE `rates_history` ENABLE KEYS */;

-- Dumping structure for table auto_service.service_orders
CREATE TABLE IF NOT EXISTS `service_orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` enum('New','Scheduled','In Progress','Quality Check','RFA','Accepted','Paid') DEFAULT NULL,
  `description` text DEFAULT NULL,
  `visit_date` date DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `cars_id` int(11) NOT NULL,
  `customers_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_service_orders_cars1_idx` (`cars_id`),
  KEY `fk_service_orders_customers1_idx` (`customers_id`),
  CONSTRAINT `fk_service_orders_cars1` FOREIGN KEY (`cars_id`) REFERENCES `cars` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_service_orders_customers1` FOREIGN KEY (`customers_id`) REFERENCES `customers` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table auto_service.service_orders: ~0 rows (approximately)
/*!40000 ALTER TABLE `service_orders` DISABLE KEYS */;
INSERT INTO `service_orders` (`id`, `status`, `description`, `visit_date`, `deleted`, `cars_id`, `customers_id`) VALUES
	(1, 'New', 'Regular engine oil change.', '2021-05-17', NULL, 12, 4),
	(2, 'Scheduled', 'The car starts but cannot drive.', '2021-05-17', NULL, 13, 4),
	(3, 'Paid', 'Car is unstable when breaking hard.', '2021-05-11', NULL, 18, 4);
/*!40000 ALTER TABLE `service_orders` ENABLE KEYS */;

-- Dumping structure for table auto_service.works
CREATE TABLE IF NOT EXISTS `works` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `min_man_hours` int(11) unsigned DEFAULT NULL,
  `rates_id` int(11) NOT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_works_rates1_idx` (`rates_id`),
  CONSTRAINT `fk_works_rates1` FOREIGN KEY (`rates_id`) REFERENCES `rates` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table auto_service.works: ~0 rows (approximately)
/*!40000 ALTER TABLE `works` DISABLE KEYS */;
INSERT INTO `works` (`id`, `name`, `min_man_hours`, `rates_id`, `deleted`) VALUES
	(1, 'Engine oil change', 2, 1, NULL),
	(2, 'Brake pads change', 3, 1, NULL),
	(3, 'Transmission replacement', 50, 2, NULL),
	(4, 'Gasket replacement', 1, 1, NULL),
	(5, 'General inspection', 2, 3, NULL);
/*!40000 ALTER TABLE `works` ENABLE KEYS */;

-- Dumping structure for table auto_service.works_history
CREATE TABLE IF NOT EXISTS `works_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `man_hours` int(11) unsigned DEFAULT NULL,
  `service_orders_id` int(11) NOT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_works_history_service_orders1_idx` (`service_orders_id`),
  CONSTRAINT `fk_works_history_service_orders1` FOREIGN KEY (`service_orders_id`) REFERENCES `service_orders` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table auto_service.works_history: ~0 rows (approximately)
/*!40000 ALTER TABLE `works_history` DISABLE KEYS */;
INSERT INTO `works_history` (`id`, `name`, `man_hours`, `service_orders_id`, `deleted`) VALUES
	(1, 'Engine oil change', 2, 1, NULL),
	(2, 'Brake pads change', 3, 3, NULL),
	(3, 'Transmission replacement', 50, 2, NULL),
	(4, 'Gasket replacement', 1, 1, NULL),
	(5, 'General inspection', 2, 2, NULL);
/*!40000 ALTER TABLE `works_history` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
