import { createContext } from "react";

export const FxContext = createContext({
  fxData: {},
  setFxData: () => {},
});

// {bgnToUsd: 0.62525,
// bgnToEur: 0.512176,
// timeStamp: "2021-06-02T07:14:23.007Z"}
