import { Route, Redirect } from 'react-router-dom';

const GuardedRouteEmployee = ({
  component: Component,
  isLoggedIn,
  user,
  ...rest
}) => (
  <Route
    {...rest}
    render={(props) =>
      isLoggedIn && (user.role === 'employee' || user.role === 'admin') ? (
        <Component {...props} />
      ) : (
        <Redirect to="/" />
      )
    }
  />
);
export default GuardedRouteEmployee;
