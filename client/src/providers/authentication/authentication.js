import { createContext } from 'react';
import jwtDecode from 'jwt-decode';

const AuthContext = createContext({
  isLoggedIn: false,
  user: null,
  setAuthState: () => {},
});

export const getUser = () => {
  try {
    return jwtDecode(getToken());
  } catch {
    return null;
  }
};

export const getToken = () => localStorage.getItem('token') || '';

export default AuthContext;
