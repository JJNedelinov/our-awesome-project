import { Route, Redirect } from 'react-router-dom';

const GuardedRouteAdmin = ({
  component: Component,
  isLoggedIn,
  user,
  ...rest
}) => (
  <Route
    {...rest}
    render={(props) =>
      isLoggedIn && user.role === 'admin' ? (
        <Component {...props} />
      ) : (
        <Redirect to="/" />
      )
    }
  />
);
export default GuardedRouteAdmin;
