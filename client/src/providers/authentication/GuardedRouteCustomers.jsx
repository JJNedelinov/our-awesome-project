import { Route, Redirect } from 'react-router-dom';

const GuardedRouteCustomers = ({
  component: Component,
  isLoggedIn,
  user,
  ...rest
}) => (
  <Route
    {...rest}
    render={(props) =>
      isLoggedIn && user.role === 'customer' ? (
        <Component {...props} />
      ) : (
        <Redirect to="/" />
      )
    }
  />
);
export default GuardedRouteCustomers;
