const SignupSchema = Yup.object().shape({
  firstName: Yup.string()
    .min(2, "Too Short!")
    .max(50, "Too Long!")
    .required("Required"),
  lastName: Yup.string()
    .min(2, "Too Short!")
    .max(50, "Too Long!")
    .required("Required"),
  email: Yup.string().email("Invalid email").required("Required"),
});

const schema = yup.object().shape({
  name: yup
    .string()
    .max(45, "Must be 45 characters or less.")
    .required("Required"),
  price: yup
    .number()
    .integer("Should be an integer value.")
    .positive("Should be a positive value.")
    .required("Required"),
});

import { Formik } from "formik";
import * as yup from "yup";

const schema = yup.object().shape({
  email: yup.string().email("Invalid email").required("Required"),
  question: yup
    .string()
    .min(100, "Too Short! Your questions should have at least 100 characters.")
    .max(500, "Must be 500 characters or less.")
    .required("Required"),
});

const testValidation = async (fieldToTest, valueToTest) => {
  const errors = await schema
    .validate({
      fieldToTest: valueToTest,
    })
    .catch(function (err) {
      return err;
    });

  console.log("errors:", errors);
};
testValidation("email", "asdasd");
