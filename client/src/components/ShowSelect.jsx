const ShowSelect = (props) => {
  return <div className="show">
    <label htmlFor="show">Show</label>
    <select
      id="show"
      name="show"
      value={props.limit}
      onChange={(e) => props.updateLimit(e.target.value)}
    >
      <option value="12">12</option>
      <option value="24">24</option>
      <option value="48">48</option>
      <option value="96">96</option>
    </select>
    <span>
      {props.match.path.slice(1)[0].toUpperCase() + props.match.path.slice(2)}
    </span>
  </div>;
};

export default ShowSelect;
