import { NavLink } from 'react-router-dom';
import Home from './Home';
import Login from './Login';
import tipTopGarageLogo from '../assets/tipTopGarageLogo.png';

const LoginSelect = ({ updateLoginOption }) => {
  return (
    <div className="login-select-container">
      <div className="login-select-logo">
        <NavLink className="button-logo" to="/">
          <img alt="" src={tipTopGarageLogo} />
        </NavLink>
      </div>
      <div className="login-select-buttons">
        <button
          className="button-option"
          onClick={(e) => updateLoginOption('customer')}
          // to="/login"
          // render={(props) => <Login {...props} role="customer" />}
        >
          Login As Customer
        </button>
        <button
          className="button-option"
          onClick={(e) => updateLoginOption('employee')}
          // to="/login"
          // render={(props) => <Login {...props} role="employee" />}
        >
          Login As Employee
        </button>
      </div>
    </div>
  );
};

export default LoginSelect;
