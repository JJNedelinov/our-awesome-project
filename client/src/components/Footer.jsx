import { NavLink } from 'react-router-dom';
import tipTopGarageLogo from '../assets/tipTopGarageLogo.png';

const Footer = (props) => {
  return (
    <div className="footer">
      <div className="footer-content">
        <div className="footer-logo">
          <p>
            <NavLink exact to="/">
              <img src={tipTopGarageLogo} alt="logo"></img>
            </NavLink>
          </p>
          <hr />
          <p style={{marginLeft:'.5rem'}}>Copyright	&copy; 2021-2021 Drift Brothers, Inc. All rights reserved.</p>
        </div>
        {/* <div className="footer-navigation">
          <div>
            <NavLink exact to="/">
              Home
            </NavLink>
          </div>
          <div>
            <NavLink exact to="/services">
              Services
            </NavLink>
          </div>
          <div>
            <NavLink exact to="/about">
              About
            </NavLink>
          </div>
          <div>
            <NavLink exact to="/contacts">
              Contacts
            </NavLink>
          </div>
          <div>
            <NavLink exact to="/login">
              Login
            </NavLink>
          </div>
        </div>
         */}
      </div>
    </div>
  );
};

export default Footer;
