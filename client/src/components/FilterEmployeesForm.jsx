import { useState } from 'react';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';

const FilterEmployeesForm = (props) => {
  const [filters, updateFilters] = useState({});

  const handleFilter = (e) => {
    props.updateFilterOpts({ ...filters });
  };

  const clearFilters = (e) => {
    updateFilters({});
    props.updateFilterOpts({});
  };

  return (
    <>
      <Form className="filter-search-form">
        <Form.Group controlId="name">
          <Form.Label>Name</Form.Label>
          <Form.Control
            name="name"
            type="text"
            placeholder="Name"
            value={filters.name ?? ''}
            onChange={(e) =>
              updateFilters((prev) => {
                return { ...prev, name: e.target.value };
              })
            }
          />
        </Form.Group>
        <Form.Group controlId="email">
          <Form.Label>Email</Form.Label>
          <Form.Control
            name="email"
            type="text"
            placeholder="Email"
            value={filters.email ?? ''}
            onChange={(e) =>
              updateFilters((prev) => {
                return { ...prev, email: e.target.value };
              })
            }
          />
        </Form.Group>
        <Form.Group controlId="phone-number">
          <Form.Label>Phone Number</Form.Label>
          <Form.Control
            name="phone"
            type="text"
            placeholder="Phone Number"
            value={filters.phone ?? ''}
            onChange={(e) =>
              updateFilters((prev) => {
                return { ...prev, phone: e.target.value };
              })
            }
          />
        </Form.Group>
      </Form>
      <Button
        variant="primary"
        className="filter-submit"
        onClick={(e) => clearFilters(e)}
      >
        Clear
      </Button>
      <Button
        variant="primary"
        className="filter-submit"
        onClick={(e) => handleFilter(e)}
      >
        Submit
      </Button>
    </>
  );
};

export default FilterEmployeesForm;
