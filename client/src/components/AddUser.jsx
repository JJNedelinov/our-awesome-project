import Button from 'react-bootstrap/Button';

const AddUser = (props) => {
  return (
    <div className={`add-${props.match.path.slice(1)}`}>
      <Button
        onClick={() =>
          props.history.push(
            `/${
              props.match.path.includes('customers')
                ? 'add-customer'
                : 'add-employee'
            }`
          )
        }
      >
        Add New{' '}
        {props.match.path.slice(1)[0].toUpperCase() +
          props.match.path.slice(2, props.match.path.length - 1)}
      </Button>
    </div>
  );
};

export default AddUser;
