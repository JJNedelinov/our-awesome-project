import { useEffect, useState, useContext } from 'react';
import Button from 'react-bootstrap/Button';
import Pagination from 'react-bootstrap/Pagination';
import UserCard from './Cards/UserCard';
import { BASE_URL } from '../common/config';
import Loading from './Loading';
import FilterCustomersForm from './FilterCustomersForm';
import FilterEmployeesForm from './FilterEmployeesForm';
import ShowSelect from './ShowSelect';
import AddUser from './AddUser';
import OrderUsers from './OrderUsers';
import EmptyResult from './EmptyResult';
import AuthContext, {
  getToken,
} from '../providers/authentication/authentication';
import NewEmployeeModal from './AddNewUser/NewEmployeeModal';

const Users = (props) => {
  const [users, updateUsers] = useState([]);
  const [filterOpts, updateFilterOpts] = useState({});
  const [showFilterOpt, updateShowFilterOpt] = useState(false);
  const [offset, updateOffset] = useState(0);
  const [limit, updateLimit] = useState(12);
  const [orderByName, updateOrderByName] = useState('');
  const [orderByVisitDate, updateOrderByVisitDate] = useState('');
  const [loading, setLoading] = useState(true);

  const [showModal, updateShowModal] = useState(false);

  const [addedUser, updateAddedUser] = useState(0);
  const [editUser, updateEditUser] = useState(0);
  const [deleteUser, updateDeleteUser] = useState(0);

  const { user } = useContext(AuthContext);

  const handleOrderBy = (e) => {
    if (e.target.textContent.toLowerCase().includes('name')) {
      updateOrderByName((prev) =>
        !prev ? 'asc' : prev === 'asc' ? 'desc' : 'asc'
      );
      updateOrderByVisitDate('');
    } else {
      updateOrderByVisitDate((prev) =>
        !prev ? 'asc' : prev === 'asc' ? 'desc' : 'asc'
      );
      updateOrderByName('');
    }
  };

  const handleResetOrder = () => {
    updateOrderByName('');
    updateOrderByVisitDate('');
  };

  useEffect(() => {
    let mounted = true;

    console.log(filterOpts);

    const url = (() => `${BASE_URL}${props.match.path}?${
      Object.keys(filterOpts).length > 0
        ? `${Object.entries(filterOpts)
            .map(([key, value]) => `${key}=${value}`)
            .join('&')}&`
        : ''
    }offset=${offset}&limit=${limit}${
      orderByName ? `&order-by=first_name&order-style=${orderByName}` : ''
    }${
      orderByVisitDate
        ? `&order-by=visit_date&order-style=${orderByVisitDate}`
        : ''
    }
  `)();

    console.log(url);

    fetch(url, {
      headers: {
        Authorization: `Bearer ${getToken()}`,
        // 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEyLCJmdWxsTmFtZSI6IlpodWxpZW4gWmhpdmtvdiIsInJvbGUiOiJlbXBsb3llZSIsImlhdCI6MTYyMjQ0MDI3MywiZXhwIjoxNjIyNTI2NjczfQ.XJB8UQaKdeQzFexixc4GJv7z5U4QV5us-nbVff83_v0',
      },
    })
      .then((res) => res.json())
      .then(({ data, error }) => {
        if (mounted) {
          if (error.length) throw new Error(error.join('\n'));

          setLoading(false);

          if (user.role === 'admin') {
            updateUsers(data);
          } else {
            const filteredEmployees = data.filter((user) => !user.deleted);
            updateUsers(filteredEmployees);
          }
        }
        return data;
      })
      .catch(console.error);

    return () => (mounted = false);
  }, [
    props,
    filterOpts,
    offset,
    limit,
    orderByName,
    orderByVisitDate,
    addedUser,
    editUser,
    deleteUser,
    user,
  ]);

  return (
    <>
      {props.match.path.includes('employees') ? (
        <NewEmployeeModal
          {...props}
          updateShowModal={updateShowModal}
          updateAddedUser={updateAddedUser}
          show={showModal}
          onHide={() => updateShowModal(false)}
        />
      ) : null}
      <div className="users">
        <div className="filter-search-bar">
          <div className="filter-search-header">
            <h4>Filter {props.match.path.slice(1)} by:</h4>
            <img
              onClick={() => updateShowFilterOpt((prev) => !prev)}
              src="https://img.icons8.com/ultraviolet/86/000000/double-down.png"
              alt="dropdown"
            />
          </div>
          {showFilterOpt ? (
            <>
              {props.match.path.includes('customers') ? (
                <FilterCustomersForm
                  {...props}
                  updateFilterOpts={updateFilterOpts}
                />
              ) : (
                <FilterEmployeesForm
                  {...props}
                  updateFilterOpts={updateFilterOpts}
                />
              )}
            </>
          ) : (
            ''
          )}
          <OrderUsers
            {...props}
            handleOrderBy={handleOrderBy}
            handleResetOrder={handleResetOrder}
            orderByName={orderByName}
            orderByVisitDate={orderByVisitDate}
          />
          <div
            className="order-add"
            style={{ marginBottom: user.role === 'employee' ? '0.5em' : '0' }}
          >
            {props.match.path.includes('customers') ? (
              <>
                <AddUser {...props} />{' '}
              </>
            ) : user.role === 'admin' ? (
              <Button
                style={{ display: 'block' }}
                onClick={() => updateShowModal(true)}
              >
                Add New Employee
              </Button>
            ) : null}
            <ShowSelect {...props} limit={limit} updateLimit={updateLimit} />
          </div>
        </div>
        <div className={loading ? 'content-loading' : 'content'}>
          {loading ? (
            <Loading />
          ) : users.length > 0 ? (
            users.map((user) => {
              return (
                <UserCard
                  {...props}
                  user={user}
                  key={user.id}
                  updateEditUser={updateEditUser}
                  updateDeleteUser={updateDeleteUser}
                />
              );
            })
          ) : (
            <EmptyResult />
          )}
        </div>
        <div
          className={
            loading || users.length === 0
              ? 'pagination-container-loading'
              : users.length <= 4
              ? 'pagination-container-less-items'
              : 'pagination-container'
          }
        >
          <div className="pag-buttons">
            <Button
              disabled={offset === 0}
              onClick={() => updateOffset((prev) => prev - limit)}
            >
              Previous Page
            </Button>
            <Button
              disabled={users.length < limit}
              onClick={() => updateOffset((prev) => prev + limit)}
            >
              Next Page
            </Button>
          </div>
        </div>
      </div>
    </>
  );
};

export default Users;
