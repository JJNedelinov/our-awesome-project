import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import { BASE_URL } from '../../common/config';
import { useContext, useState } from 'react';
import AuthContext, {
  getToken,
} from '../../providers/authentication/authentication';
import { toast } from 'react-toastify';

function EditUserModal(props) {
  const { user } = useContext(AuthContext);

  const [employeeProps, updateEmployeeProps] = useState(
    props.user
      ? {
          first_name: props.user.first_name,
          last_name: props.user.last_name,
          email: props.user.email,
          phone: props.user.phone,
        }
      : {}
  );

  const [changePassword, updateChangePassword] = useState(false);

  const [fillDetails, updateFillDetails] = useState(true);

  const creationSuccess = (message) =>
    toast.success(message, {
      autoClose: 3000,
    });

  const creationFail = (message) => {
    toast.error(message, {
      autoClose: 3000,
    });
  };

  const handleFillDetails = () => {
    updateEmployeeProps(
      props.user
        ? {
            first_name: props.user.first_name,
            last_name: props.user.last_name,
            email: props.user.email,
            phone: props.user.phone,
          }
        : {}
    );

    updateFillDetails(true);
  };

  const handleClearDetails = () => {
    updateEmployeeProps({});
    updateFillDetails(false);
  };

  const handleCancel = () => {
    props.setShow(false);
    updateChangePassword(false);
  };

  const handleAdd = () => {
    if (changePassword) {
      if (
        'current_password' in employeeProps &&
        'new_password' in employeeProps &&
        'repeat_password' in employeeProps
      ) {
        if (employeeProps.new_password !== employeeProps.repeat_password) {
          creationFail(`The new password do not match the repeated password!`);
          return;
        }
      } else {
        creationFail('All password fields are required!');
        return;
      }
    }

    const url = `${BASE_URL}${
      props.match.path.includes('account')
        ? `/customers/${props.user.id}`
        : props.match.path.includes('id')
        ? props.match.url
        : props.match.path.includes('customers')
        ? `/customers/${props.user.id}`
        : `/employees/${props.user.id}`
    }`;

    fetch(url, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${getToken()}`,
      },
      body: JSON.stringify(employeeProps),
    })
      .then((res) => res.json())
      .then((res) => {
        const { error } = res;

        if (error.length) throw new Error(error.join('\n'));

        props.setShow(false);

        props.updateEditUser((prev) => prev + 1);
        updateChangePassword(false);
        // updateEmployeeProps({});
        creationSuccess(
          `Succesfully edited ${
            props.match.path.includes('account')
              ? 'your Account'
              : props.match.path.includes('customers')
              ? 'the Customer'
              : 'the Employee'
          } with email: ${props.user.email}!`
        );
      })
      .catch((error) => {
        // updateEmployeeProps({});
        creationFail(error.message);
      });
  };

  const handleSelections = (e, field) => {
    e.target.value = e.target.value.trim();

    if (e.target.value === '') {
      const employeePropsCopy = JSON.parse(JSON.stringify(employeeProps));

      delete employeePropsCopy[field];

      updateEmployeeProps(employeePropsCopy);
    } else {
      updateEmployeeProps((prev) => ({ ...prev, [field]: e.target.value }));
    }
  };

  return (
    <>
      <Modal
        {...props}
        size="m"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header className="custom-title" closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Edit{' '}
            {props.match.path.includes('my-account')
              ? 'My Account'
              : props.match.path.includes('customers')
              ? 'Customer'
              : 'Employee'}
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div
            className="no-model"
            // style={{
            //   display:
            //     selectedBrandId && selectedBrandId !== 'no-brand'
            //       ? 'block'
            //       : 'none',
            // }}
          >
            {/* <p>Add a Model For Selected Brand</p> */}
            <div
              className="add-model"
              // style={{ display: 'block' }}
            >
              <div className="no-inputs">
                {+user.sub === +props.user.id ? (
                  <h5 style={{ marginBottom: '.75em' }}>
                    You are editing <b>your</b> account details.
                  </h5>
                ) : (
                  <h5 style={{ marginBottom: '.75em' }}>
                    You are editing{' '}
                    <b>
                      {props.user.first_name[0]}. {props.user.last_name}
                    </b>
                    's details.
                  </h5>
                )}
                <label htmlFor="first-name">First Name:</label>
                <input
                  id="first-name"
                  style={{ marginBottom: '1em' }}
                  type="text"
                  placeholder="First Name"
                  className="form-control"
                  value={employeeProps.first_name ?? ''}
                  onChange={(e) => handleSelections(e, 'first_name')}
                />
                <label htmlFor="last-name">Last Name:</label>
                <input
                  id="last-name"
                  style={{ marginBottom: '1em' }}
                  type="text"
                  placeholder="Last Name"
                  className="form-control"
                  value={employeeProps.last_name ?? ''}
                  onChange={(e) => handleSelections(e, 'last_name')}
                />
                <label htmlFor="email">Email:</label>
                <input
                  id="email"
                  style={{ marginBottom: '1em' }}
                  type="email"
                  placeholder="Email"
                  className="form-control"
                  value={employeeProps.email ?? ''}
                  onChange={(e) => handleSelections(e, 'email')}
                />
                <label htmlFor="phone">Phone Number:</label>
                <input
                  id="phone"
                  style={{ marginBottom: '1em' }}
                  type="text"
                  placeholder="Phone Number"
                  className="form-control"
                  value={employeeProps.phone ?? ''}
                  onChange={(e) => handleSelections(e, 'phone')}
                />
                <div
                  className={`no-buttons ${
                    +user.sub === +props.user.id ? 'first-row-btns' : ''
                  }`}
                >
                  <Button
                    className="add-button"
                    onClick={() =>
                      fillDetails ? handleClearDetails() : handleFillDetails()
                    }
                  >
                    {fillDetails ? 'Clear' : 'Fill'} Account Details
                  </Button>
                  {+user.sub === +props.user.id ? (
                    <Button
                      className="add-button"
                      onClick={() => updateChangePassword((prev) => !prev)}
                    >
                      Change Password?
                    </Button>
                  ) : null}
                </div>
                {+user.sub === +props.user.id && changePassword ? (
                  <>
                    <hr style={{ marginBottom: '.75em' }} />
                    <h5>Change password?</h5>
                    <label htmlFor="current-password">Current Password:</label>
                    <input
                      id="current-password"
                      style={{ marginBottom: '.5em' }}
                      type="password"
                      placeholder="Current Password"
                      className="form-control"
                      // value={employeeProps.phone ?? ''}
                      onChange={(e) => handleSelections(e, 'current_password')}
                    />
                    <label htmlFor="new-password">New Password:</label>
                    <input
                      id="new-password"
                      style={{ marginBottom: '.5em' }}
                      type="password"
                      placeholder="New Password"
                      className="form-control"
                      // value={employeeProps.phone ?? ''}
                      onChange={(e) => handleSelections(e, 'new_password')}
                    />
                    <label htmlFor="repeat-password">
                      Repeat New Password:
                    </label>
                    <input
                      id="repeat-password"
                      style={{ marginBottom: '.5em' }}
                      type="password"
                      placeholder="Repeat New Password"
                      className="form-control"
                      // value={employeeProps.phone ?? ''}
                      onChange={(e) => handleSelections(e, 'repeat_password')}
                    />
                  </>
                ) : null}
              </div>
            </div>
            <div className="no-buttons">
              <Button
                className="add-button"
                style={
                  {
                    // display: addModel ? 'inline' : 'none',
                  }
                }
                onClick={handleAdd}
              >
                Edit
              </Button>
              <Button
                className="cancel-button"
                style={{
                  // display: addModel ? 'inline' : 'none',
                  marginLeft: '0.5em',
                }}
                onClick={handleCancel}
              >
                Cancel
              </Button>
            </div>
          </div>
        </Modal.Body>
      </Modal>
    </>
  );
}

export default EditUserModal;
