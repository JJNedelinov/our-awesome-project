import constants from '../../common/constants';
import Button from 'react-bootstrap/Button';
import { useState, useEffect, useContext } from 'react';
import { BASE_URL } from '../../common/config';
import AuthContext, {
  getToken,
} from '../../providers/authentication/authentication';
import Loading from '../Loading';
import EmptyResult from '../EmptyResult';

const UserCar = (props) => {
  const { user } = useContext(AuthContext);

  const [cars, updateCars] = useState([]);
  const [firstCar, updateFirstCar] = useState();
  const [lastCar, updateLastCar] = useState();
  const [currentCar, updateCurrentCar] = useState(0);

  const [loading, updateLoading] = useState(true);

  useEffect(() => {
    let mounted = true;

    let endpoint = null;
    let id = null;

    if (props.match.path.includes('my-account')) {
      endpoint = `${user.role === 'customer' ? 'customers' : 'employees'}`;
      id = user.sub;
    } else {
      endpoint = props.match.path.includes('customers')
        ? 'customers'
        : 'employees';
      id = props.match.params.id;
    }

    // ! MOCK ID - CHANGE IT
    fetch(`${BASE_URL}/cars?${endpoint}_id=${id}`, {
      headers: {
        Authorization: `Bearer ${getToken()}`,
        // 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEyLCJmdWxsTmFtZSI6IlpodWxpZW4gWmhpdmtvdiIsInJvbGUiOiJhZG1pbiIsImlhdCI6MTYyMjY5ODk0MiwiZXhwIjoxNjIyNzg1MzQyfQ.5332qecIeiZv430OlvtSGCttNj2CUfGIrk54Tn-fypc',
      },
    })
      .then((response) => response.json())
      .then((res) => {
        if (mounted) {
          const { data, error } = res;

          if (error.length) throw new Error(error.join('\n'));

          updateLoading(false);
          updateCars(data);
          updateFirstCar(0);
          updateLastCar(Math.max(0, data.length - 1));
          updateCurrentCar(data.length && 0);
        }
      })
      .catch(console.log);

    return () => (mounted = false);
  }, [props, user]);

  // return loading ? (
  //   <Loading />
  // ) : cars.length ? (
  return (
    <div className="details car-details">
      <div className="car-details-header">
        <h3>
          Car Details{' '}
          <span
            style={{
              // display: cars.length ? 'inline' : 'none',
              verticalAlign: 'middle',
              fontSize: '.8em',
            }}
          >
            {cars.length ? `(${currentCar + 1}/${lastCar + 1})` : '(No Cars)'}
          </span>
        </h3>
        <div
          className="edit-delete-icons"
          style={{
            display: props.match.path.includes('my-account') ? 'none' : 'block',
          }}
        >
          <button
            onClick={() => props.history.push(`${props.match.url}/add-car`)}
          >
            <img
              style={{ maxWidth: '34px' }}
              src="https://img.icons8.com/plasticine/100/000000/plus-2-math.png"
              alt="add"
            />
          </button>
          <button
            onClick={() =>
              props.history.push(`${props.match.url}/edit-car/${currentCar}`)
            }
          >
            <img
              src="https://img.icons8.com/dusk/512/000000/edit--v1.png"
              alt="edit"
            />
          </button>
          {/* <button>
            <img
              style={{ maxWidth: '34px' }}
              src="https://img.icons8.com/plasticine/206/000000/filled-trash.png"
              alt="delete"
            />
          </button> */}
        </div>
      </div>
      <div className="car-content">
        {loading ? (
          <Loading />
        ) : cars.length ? (
          <>
            <div className="view-car-img">
              <img
                src={
                  cars.length &&
                  (/(http)?s?:?(\/\/[^"']*\.(?:png|jpg|jpeg|gif|png|svg))/.test(
                    cars[currentCar].car_image
                  )
                    ? cars[currentCar].car_image
                    : 'https://region4.uaw.org/sites/default/files/bio/10546i3dac5a5993c8bc8c_4.jpg')
                    // : 'https://www.regentfurniture.co.za/wp-content/uploads/2016/07/Insert-Image-Here.png')
                }
                alt="example car"
              />
            </div>
            {/* <div className="car-info"> */}
            <ul className="car-info">
              {/* {cars.map((car) => ( */}
              {cars.length
                ? constants.carProps.map((prop, i) => {
                    return (
                      <li key={i}>
                        <p>{prop[0].toUpperCase() + prop.slice(1)}:</p>
                        <h5>
                          {prop === 'year'
                            ? new Date(cars[currentCar][prop]).getFullYear()
                            : cars[currentCar][prop]}
                        </h5>
                      </li>
                    );
                  })
                : null}
            </ul>
          </>
        ) : (
          <EmptyResult {...props} />
        )}
      </div>
      <div className="car-details-footer">
        <div className="show-next">
          {/* if service order index = 0 - enabled - if last - disable and prev button enabled */}
          <Button
            // style={{
            //   display:
            //     currentCar === firstServiceOrder
            //       ? 'none'
            //       : 'block',
            // }}
            disabled={currentCar === firstCar}
            onClick={() => updateCurrentCar((prev) => --prev)}
          >
            {/* Previous Service Order */}
            Previous
          </Button>
          <Button
            // style={{
            //   display:
            //     currentCar === lastServiceOrder ? 'none' : 'block',
            // }}
            disabled={currentCar === lastCar}
            onClick={() => updateCurrentCar((prev) => ++prev)}
          >
            {/* Next Service Order */}
            Next
          </Button>
        </div>
      </div>
    </div>
  );
};

export default UserCar;
