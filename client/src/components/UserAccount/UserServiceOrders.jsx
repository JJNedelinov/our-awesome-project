import { useContext } from 'react';
import { useEffect } from 'react';
import { useState } from 'react';
import Button from 'react-bootstrap/Button';
import { BASE_URL } from '../../common/config';
import AuthContext, {
  getToken,
} from '../../providers/authentication/authentication';
import EmptyResult from '../EmptyResult';
import ServiceOrder from '../ServiceOrders/ServiceOrder';

const UserServiceOrders = (props) => {
  const { user } = useContext(AuthContext);

  const [serviceOrders, setServiceOrders] = useState([]);
  const [firstServiceOrder, updateFirstServiceOrder] = useState();
  const [lastServiceOrder, updateLastServiceOrder] = useState();
  const [currentServiceOrder, updateCurrentServiceOrder] = useState(0);

  const handleEditCreateBtns = () => {
    props.history.push('/edit-create-service-order');
  };

  const updateStatus = (serviceOrderId, newStatus) => {
    const myHeaders = new Headers();
    myHeaders.append('content-type', 'application/json');
    myHeaders.append('Authorization', `Bearer ${getToken()}`);

    var raw = JSON.stringify({
      status: `${newStatus}`,
    });

    var requestOptions = {
      method: 'PUT',
      headers: myHeaders,
      body: raw,
      redirect: 'follow',
    };

    fetch(
      `http://localhost:8888/service-orders/${serviceOrderId}`,
      requestOptions
    )
      .then((response) => response.text())
      .then((result) => console.log(result))
      .catch((error) => console.log('error', error));
  };

  useEffect(() => {
    let mounted = true;

    let endpoint = null;
    let id = null;

    if (props.match.path.includes('my-account')) {
      endpoint = `${user.role === 'customer' ? 'customers' : 'employees'}`;
      id = user.sub;
    } else {
      endpoint = props.match.path.includes('customers')
        ? 'customers'
        : 'employees';
      id = props.match.params.id;
    }

    fetch(`${BASE_URL}/service-orders?${endpoint}_id=${id}`, {
      headers: {
        Authorization: `Bearer ${getToken()}`,
      },
    })
      .then((response) => response.json())
      .then((res) => {
        if (mounted) {
          const { data, error } = res;

          if (error.length) throw new Error(error.join('\n'));

          setServiceOrders(data);
          updateFirstServiceOrder(0);
          updateLastServiceOrder(Math.max(0, data.length - 1));
          updateCurrentServiceOrder(data.length && 0);
        }
      })
      .catch(console.log);

    return () => (mounted = false);
  }, [user, props]);

  return (
    <div className="details service-order-details">
      <div className="service-order-details-header">
        <h3>
          Service Orders{' '}
          <span
            className="service-orders-count"
            style={{
              verticalAlign: 'middle',
              fontSize: '.8em',
            }}
          >
            {serviceOrders.length
              ? `(${currentServiceOrder + 1}/${lastServiceOrder + 1})`
              : '(No Service Orders)'}
          </span>
        </h3>
        <div
          className="edit-delete-icons"
          style={{
            display: props.match.path.includes('my-account') ? 'none' : 'block',
          }}
        >
          <button onClick={handleEditCreateBtns}>
            <img
              style={{ maxWidth: '34px' }}
              src="https://img.icons8.com/plasticine/100/000000/plus-2-math.png"
              alt="create"
            />
          </button>
          <button onClick={handleEditCreateBtns}>
            <img
              src="https://img.icons8.com/dusk/512/000000/edit--v1.png"
              alt="edit"
            />
          </button>
        </div>
      </div>
      <div className="service-order-content">
        {serviceOrders.length ? (
          <ServiceOrder
            serviceOrder={serviceOrders[currentServiceOrder]}
            updateStatus={updateStatus}
          />
        ) : (
          <EmptyResult {...props} />
        )}
      </div>
      <div className="service-order-details-footer">
        <div className="show-next">
          <Button
            disabled={
              serviceOrders.length
                ? currentServiceOrder === firstServiceOrder
                : true
            }
            onClick={() => updateCurrentServiceOrder((prev) => --prev)}
          >
            Previous
          </Button>
          <Button
            disabled={
              serviceOrders.length
                ? currentServiceOrder === lastServiceOrder
                : true
            }
            onClick={() => updateCurrentServiceOrder((prev) => ++prev)}
          >
            Next
          </Button>
        </div>
      </div>
    </div>
  );
};

export default UserServiceOrders;
