import { useState, useEffect, useContext, useCallback } from 'react';
import { BASE_URL } from '../../common/config';
import constants from '../../common/constants';
import AuthContext, {
  getToken,
} from '../../providers/authentication/authentication';
import { toast } from 'react-toastify';
import Loading from '../Loading';
import EditUserModal from '../EditUser/EditUserModal';

const UserInfo = (props) => {
  const { user } = useContext(AuthContext);

  const [userCreds, updateUserCreds] = useState([]);
  const [deleteUser, updateDeleteUser] = useState(0);
  const [editUser, updateEditUser] = useState(0);

  const [loading, updateLoading] = useState(true);
  const [show, setShow] = useState(false);

  const deleteRenewSuccess = (message) =>
    toast.success(message, {
      autoClose: 3000,
    });

  const deleteRenewFail = (message) => {
    toast.error(message, {
      autoClose: 3000,
    });
  };

  const usersMap = useCallback(
    (prop, i) => {
      let propTransform = prop.split('_');
      return (
        <li key={i}>
          {prop.includes('name') ? (
            <p>
              {propTransform[0][0].toUpperCase() + propTransform[0].slice(1)}{' '}
              {propTransform[1][0].toUpperCase() + propTransform[1].slice(1)}
            </p>
          ) : (
            <p>{prop[0].toUpperCase() + prop.slice(1)}</p>
          )}
          <h5>{userCreds[0][prop]}</h5>
        </li>
      );
    },
    [userCreds]
  );

  const handleDelete = () => {
    fetch(`${BASE_URL}${props.match.url}`, {
      headers: {
        Authorization: `Bearer ${getToken()}`,
      },
      method: 'DELETE',
    })
      .then((res) => res.json())
      .then((res) => {
        const { error } = res;

        if (error.length) throw new Error(error.join('\n'));

        deleteRenewSuccess(
          `Successfully deleted Customer with email - ${userCreds[0].email}`
        );
        updateDeleteUser((prev) => prev + 1);
      })
      .catch((error) => deleteRenewFail(error.message));
  };

  const handleRenewal = () => {
    fetch(`${BASE_URL}${props.match.url}/renewal`, {
      headers: {
        Authorization: `Bearer ${getToken()}`,
      },
      method: 'PUT',
    })
      .then((res) => res.json())
      .then((res) => {
        const { error } = res;

        if (error.length) throw new Error(error.join('\n'));

        deleteRenewSuccess(
          `Successfully renewed Customer with email - ${userCreds[0].email}`
        );
        updateDeleteUser((prev) => prev + 1);
      })
      .catch((error) => deleteRenewFail(error.message));
  };

  useEffect(() => {
    let mounted = true;

    let endpoint = null;
    let id = null;

    if (props.match.path.includes('my-account')) {
      endpoint = `${user.role === 'customer' ? 'customers' : 'employees'}`;
      id = user.sub;
    } else {
      endpoint = props.match.path.includes('customers')
        ? 'customers'
        : 'employees';
      id = props.match.params.id;
    }

    // ! MOCK ID - CHANGE IT
    fetch(`${BASE_URL}/${endpoint}/${id}`, {
      headers: {
        Authorization: `Bearer ${getToken()}`,
      },
    })
      .then((response) => response.json())
      .then((res) => {
        if (mounted) {
          const { data, error } = res;

          if (error.length) throw new Error(error.join('\n'));

          updateUserCreds(data);
          updateLoading(false);
        }
      })
      .catch(console.log);

    return () => (mounted = false);
  }, [
    props.match.params.id,
    props.match.path,
    user.sub,
    user.role,
    deleteUser,
    editUser,
  ]);

  return (
    <>
      {userCreds.length && (
        <EditUserModal
          {...props}
          user={userCreds[0]}
          setShow={setShow}
          updateEditUser={updateEditUser}
          show={show}
          onHide={() => setShow(false)}
        />
      )}
      <div className="details customer-details">
        <div className="user-details-header">
          <h3>
            {props.match.path.includes('my-account')
              ? 'Account'
              : props.match.path.includes('customers')
              ? 'Customer'
              : 'Employee'}{' '}
            Details
          </h3>
          {props.match.path.includes('my-account') ? (
            <div className="edit-delete-icons">
              <button onClick={() => setShow(true)}>
                <img
                  src="https://img.icons8.com/dusk/512/000000/edit--v1.png"
                  alt="edit"
                />
              </button>
            </div>
          ) : (
            <div className="edit-delete-icons">
              <button onClick={() => setShow(true)}>
                <img
                  src="https://img.icons8.com/dusk/512/000000/edit--v1.png"
                  alt="edit"
                />
              </button>
              {userCreds.length && userCreds[0].deleted ? (
                <button onClick={handleRenewal}>
                  <img
                    style={{ maxWidth: '34px' }}
                    src="https://img.icons8.com/plasticine/100/000000/return.png"
                    alt="renew"
                  />
                </button>
              ) : (
                <button onClick={handleDelete}>
                  <img
                    style={{ maxWidth: '34px' }}
                    src="https://img.icons8.com/plasticine/206/000000/filled-trash.png"
                    alt="delete"
                  />
                </button>
              )}
            </div>
          )}
        </div>
        {loading ? (
          <Loading />
        ) : (
          <ul className="user-info">
            {userCreds.length && constants.userProps.map(usersMap)}
            {userCreds.length && (
              <>
                <li>
                  <p>Registration Date:</p>
                  <h5>
                    {new Date(userCreds[0].created_at).toLocaleDateString(
                      'en-US'
                    )}
                  </h5>
                </li>
                {user.role === 'admin' || user.role === 'employee' ? (
                  <li>
                    <p>Is Deleted?</p>
                    <h5>{userCreds[0].deleted ? 'Yes' : 'No'}</h5>
                  </li>
                ) : null}
              </>
            )}
          </ul>
        )}
      </div>
    </>
  );
};

export default UserInfo;
