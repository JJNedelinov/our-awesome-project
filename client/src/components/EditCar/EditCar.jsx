import { useState, useEffect } from 'react';
// import { Formik } from 'formik';
// import * as yup from 'yup';
import { BASE_URL } from '../../common/config';
import AddBrandModal from '../AddBrandModal';
import AddModelModal from '../AddModelModal';
import { toast } from 'react-toastify';
import { getToken } from '../../providers/authentication/authentication';
import Button from 'react-bootstrap/Button';
import constants from '../../common/constants';
import EditCarForm from './EditCarForm';

const EditCar = (props) => {
  const [userOnPage, updateUserOnPage] = useState({});

  const [incomingCarObj, updateIncomingCarObj] = useState(
    constants.initialCarObjState
  );
  const [carObj, updateCarObj] = useState(constants.initialCarObjState);

  const [brands, updateBrands] = useState([]);
  const [brand, updateBrand] = useState('');
  const [addBrand, updateAddBrand] = useState(false);
  const [selectedBrandId, updateSelectedBrandId] = useState('');
  const [models, updateModels] = useState([]);
  const [selectedModelId, updateSelectedModelId] = useState('');
  const [addModel, updateAddModel] = useState(false);
  const [model, updateModel] = useState('');
  const [modelYear, updateModelYear] = useState('');
  const [newBrand, updateNewBrand] = useState({});
  const [newModel, updateNewModel] = useState({});
  const [modelSelectField, setModelSelectField] = useState(true);

  const editSuccess = (message) =>
    toast.success(message, {
      autoClose: 3000,
    });

  const editFail = (message) => {
    toast.error(message, {
      autoClose: 3000,
    });
  };

  const handleEditCar = (e) => {
    const url = `${BASE_URL}/cars/${incomingCarObj.car_id}`;

    fetch(url, {
      method: 'PUT',
      headers: {
        Authorization: `Bearer ${getToken()}`,

        'Content-Type': 'application/json',
      },
      body: JSON.stringify(carObj),
    })
      .then((res) => res.json())
      .then((res) => {
        const { error } = res;

        if (error.length) throw new Error(error.join('\n'));

        editSuccess(
          `Successfully edit the car details for ${
            userOnPage && userOnPage.email
          }`
        );

        // updateCarObj(incomingCarObj);
        // updateSelectedBrandId('');
        // updateSelectedModelId('');
        // setModelSelectField(true);

        return res;
      })
      .catch((error) => editFail(error.message));
  };

  const handleSelections = (e, field) => {
    e.target.value = e.target.value.trim();

    switch (field) {
      case 'brand':
        updateSelectedBrandId(e.target.value);

        if (e.target.value && e.target.value !== 'no-brand') {
          const brand = brands.find((brand) => brand.id === +e.target.value);
          updateCarObj((prev) => ({ ...prev, brand: brand.name }));
          setModelSelectField(false);
        } else {
          updateCarObj((prev) => ({ ...prev, brand: '' }));
          updateSelectedModelId('');
          setModelSelectField(true);
        }
        break;
      case 'model':
        updateSelectedModelId(e.target.value);

        if (e.target.value && e.target.value !== 'no-model') {
          const model = models.find((model) => model.id === +e.target.value);
          updateCarObj((prev) => ({
            ...prev,
            model: model.name,
            year: model.year,
          }));
        } else {
          updateCarObj((prev) => ({ ...prev, model: '' }));
        }
        break;

      case 'coupe':
      case 'engine':
      case 'transmission':
      case 'horse_power':
      case 'registration_plate':
      case 'vin':
      case 'car_image':
        if (e.target.value) {
          updateCarObj((prev) => ({
            ...prev,
            [field]: e.target.value,
          }));
        } else updateCarObj((prev) => ({ ...prev, [field]: '' }));
        break;
      default:
        break;
    }
  };

  useEffect(() => {
    let mounted = true;

    fetch(`${BASE_URL}/customers/${props.match.params.id}`, {
      headers: {
        Authorization: `Bearer ${getToken()}`,
      },
    })
      .then((res) => res.json())
      .then((res) => {
        if (mounted) {
          const { data, error } = res;

          if (error.length) throw new Error(error.join('\n'));

          updateUserOnPage(data[0]);
        }
      })
      .catch((error) => editFail(`${error.message}`));

    return () => (mounted = false);
  }, [props]);

  useEffect(() => {
    let mounted = true;

    fetch(`${BASE_URL}/cars?customers_id=${props.match.params.id}`, {
      headers: {
        Authorization: `Bearer ${getToken()}`,
      },
    })
      .then((res) => res.json())
      .then((res) => {
        if (mounted) {
          const { data, error } = res;

          if (error.length) throw new Error(error.join('\n'));
          updateIncomingCarObj(data[props.match.params.carId]);
        }
      })
      .catch((error) => editFail(`${error.message}`));

    return () => (mounted = false);
  }, [props]);

  useEffect(() => {
    let mounted = true;

    fetch(`${BASE_URL}/brands`)
      .then((res) => res.json())
      .then((res) => {
        const { data } = res;

        if (mounted) {
          if (!Object.keys(newBrand).length) {
            const incomingBrandId = data.find(
              (brand) => brand.name === incomingCarObj.brand
            ).id;
            updateSelectedBrandId(incomingBrandId);
          }

          updateBrands(data);
        }

        return res;
      })
      .catch(console.error);

    return () => (mounted = false);
  }, [newBrand, incomingCarObj]);

  useEffect(() => {
    let mounted = true;

    fetch(`${BASE_URL}/models?brands_id=${selectedBrandId}`)
      .then((res) => res.json())
      .then((res) => {
        const { data } = res;

        if (mounted) {
          if (!Object.keys(newModel).length) {
            const incomingModelId = data.find(
              (model) => model.name === incomingCarObj.model
            ).id;
            setModelSelectField(false);
            updateSelectedModelId(incomingModelId);
          }

          updateModels(data);
        }

        return res;
      })
      .catch(console.error);

    return () => (mounted = false);
  }, [selectedBrandId, newModel, incomingCarObj]);

  useEffect(() => {
    let mounted = true;

    if (mounted) {
      updateCarObj({
        brand: incomingCarObj.brand,
        model: incomingCarObj.model,
        coupe: incomingCarObj.coupe,
        engine: incomingCarObj.engine,
        transmission: incomingCarObj.transmission,
        horse_power: incomingCarObj.horse_power,
        registration_plate: incomingCarObj.registration_plate,
        vin: incomingCarObj.vin,
        car_image: incomingCarObj.car_image,
      });
    }

    return () => (mounted = false);
  }, [incomingCarObj]);

  return (
    <>
      <div className="add-new">
        <div className="details customer-details">
          <div className="overlay" style={{ display: 'block' }}></div>
          <div className="details-heading">
            <h3>Customer Details</h3>
          </div>
          <div className="customer-info">
            <h5>
              First Name: <span>{userOnPage.first_name}</span>
            </h5>
            <h5>
              Last Name: <span>{userOnPage.last_name}</span>
            </h5>
            <h5>
              Email: <span>{userOnPage.email}</span>
            </h5>
            <h5>
              Phone: <span>{userOnPage.phone}</span>
            </h5>
            <h5>
              Registration Date:{' '}
              <span>
                {new Date(userOnPage.created_at).toLocaleDateString('en-US')}
              </span>
            </h5>
          </div>
        </div>
        <div className="add-page-buttons"></div>
        <div className="details car-details">
          <div className="details-heading">
            <h3>Car Details</h3>
          </div>
          <div className="car-content">
            <div className="view-car-img">
              <img
                src={
                  carObj && carObj.hasOwnProperty('car_image')
                    ? carObj.car_image
                    : 'https://www.maxim.com/.image/t_share/MTM3NTcwOTYwNjM0MTYwNTU3/grey-gt500cr_002jpg.jpg'
                }
                alt="example car"
              />
            </div>
            <AddBrandModal
              {...props}
              brand={brand}
              updateNewBrand={updateNewBrand}
              selectedBrandId={selectedBrandId}
              updateSelectedBrandId={updateSelectedBrandId}
              updateCarObj={updateCarObj}
              addBrand={addBrand}
              updateBrand={updateBrand}
              updateAddBrand={updateAddBrand}
              setModelSelectField={setModelSelectField}
              show={addBrand}
              onHide={() => updateAddBrand(false)}
            />
            <AddModelModal
              {...props}
              model={model}
              updateModel={updateModel}
              modelYear={modelYear}
              updateModelYear={updateModelYear}
              carObj={carObj}
              updateCarObj={updateCarObj}
              selectedBrandId={selectedBrandId}
              updateSelectedModelId={updateSelectedModelId}
              addModel={addModel}
              updateAddModel={updateAddModel}
              updateNewModel={updateNewModel}
              show={addModel}
              onHide={() => updateAddModel(false)}
            />

            <EditCarForm
              {...props}
              carObj={carObj}
              brands={brands}
              addBrand={addBrand}
              models={models}
              addModel={addModel}
              updateAddModel={updateAddModel}
              updateAddBrand={updateAddBrand}
              modelSelectField={modelSelectField}
              selectedModelId={selectedModelId}
              selectedBrandId={selectedBrandId}
              handleSelections={handleSelections}
            />
          </div>
        </div>
        <div className="add-page-buttons">
          <div>
            <Button
              variant="primary"
              className="filter-submit"
              onClick={(e) => handleEditCar(e)}
            >
              Edit Car
            </Button>
          </div>
        </div>
      </div>
    </>
  );
};

export default EditCar;
