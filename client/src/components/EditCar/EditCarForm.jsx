import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import * as yup from 'yup';

const EditCarForm = (props) => {
  const schema = yup.object().shape({
    // horsepower, vin, reg plate
    registrationPlate: yup
      .string()
      .min(5, 'Registration plate should be between 5 and 6 characters.')
      .max(6, 'Registration plate should be between 5 and 6 characters.')
      .matches(
        /^[A-Z]{1,3}-[A-Z]{1,2}-[0-9]{1,4}$/, // JJ to validate
        'Registration plate format is not valid.'
      )
      .required('Registration plate is required.'),
    vinNumber: yup
      .string()
      .min(17, 'VIN number should be 17 characters.')
      .max(17, 'VIN number should be 17 characters.')
      .matches(
        /^[A-HJ-NPR-Za-hj-npr-z\d]{8}[\dX][A-HJ-NPR-Za-hj-npr-z\d]{2}\d{6}$/, // JJ to validate
        'VIN number format is not valid.'
      ),
    horsePower: yup
      .number('Horse power should be a number.')
      .integer('Horse power should be an integer value.')
      .positive('Horse power should be a positive number.'),
  });

  return (
    <Form className="add-user">
      <Form.Group controlId="brand">
        <Form.Label>Brand</Form.Label>
        <select
          id="brand"
          className="form-control"
          value={
            (props.carObj && props.selectedBrandId !== 'no-brand') ||
            props.selectedBrandId
              ? props.selectedBrandId
              : ''
          }
          onChange={(e) => props.handleSelections(e, 'brand')}
        >
          <option value="">Select a Brand...</option>
          <option value="no-brand">Don't See The Brand...</option>
          {props.brands
            .sort((a, b) => a.name.localeCompare(b.name))
            .map((brand) => (
              <option key={brand.id} value={brand.id}>
                {brand.name}
              </option>
            ))}
        </select>
        <div
          className="no-buttons"
          style={{
            display: props.selectedBrandId === 'no-brand' ? 'block' : 'none',
          }}
        >
          <Button
            className="add-button"
            style={{
              display: !props.addBrand ? 'inline' : 'none',
              marginBottom: '-1.6em',
            }}
            onClick={() => props.updateAddBrand(true)}
          >
            Add a Brand
          </Button>
        </div>
      </Form.Group>
      <Form.Group controlId="model">
        <Form.Label>Model</Form.Label>
        <select
          id="model"
          className="form-control"
          disabled={props.modelSelectField}
          value={
            props.selectedModelId !== 'no-model' || props.selectedModelId
              ? props.selectedModelId
              : ''
          }
          onChange={(e) => props.handleSelections(e, 'model')}
        >
          <option value="">Select a Model...</option>
          <option value="no-model">Don't See The Model...</option>
          {props.models
            .sort((a, b) => a.name.localeCompare(b.name))
            .map((model) => (
              <option key={model.id} value={model.id}>
                {model.name} - {new Date(model.year).getFullYear()}
              </option>
            ))}
        </select>
        <div
          className="no-buttons"
          style={{
            display:
              (props.selectedBrandId !== 'no-model' &&
                props.selectedBrandId &&
                props.selectedModelId) === 'no-model'
                ? 'block'
                : 'none',
          }}
        >
          <Button
            className="add-button"
            style={{
              display: !props.addModel ? 'inline' : 'none',
              marginBottom: '-1.6em',
            }}
            onClick={() => props.updateAddModel(true)}
          >
            Add a Model
          </Button>
        </div>
      </Form.Group>
      <Form.Group controlId="coupe">
        <Form.Label>Coupe</Form.Label>
        <select
          id="coupe"
          className="form-control"
          value={props.carObj && props.carObj.coupe}
          onChange={(e) => props.handleSelections(e, 'coupe')}
        >
          <option value="">Select a Coupe Type...</option>
          <option value="Convertible">Convertible</option>
          <option value="Coupe">Coupe</option>
          <option value="Minivan">Minivan</option>
          <option value="Van">Van</option>
          <option value="Pickup Truck">Pickup Truck</option>
          <option value="SUV">SUV</option>
          <option value="Sedan">Sedan</option>
          <option value="Wagon">Wagon</option>
          <option value="Hatchback">Hatchback</option>
        </select>
      </Form.Group>
      <Form.Group controlId="engine">
        <Form.Label>Engine</Form.Label>
        <select
          id="engine"
          className="form-control"
          value={props.carObj.engine}
          onChange={(e) => props.handleSelections(e, 'engine')}
        >
          <option value="">Select an Engine Type...</option>
          <option value="gasoline">Gasoline</option>
          <option value="diesel">Diesel</option>
          <option value="hybrid">Hybrid</option>
          <option value="electirc">Electric</option>
          <option value="hydrogen">Hydrogen</option>
        </select>
      </Form.Group>
      <Form.Group controlId="transmission">
        <Form.Label>Transmission</Form.Label>
        <select
          id="transmission"
          className="form-control"
          value={props.carObj.transmission}
          onChange={(e) => props.handleSelections(e, 'transmission')}
        >
          <option value="">Select a Transmission...</option>
          <option value="manual">Manual</option>
          <option value="automatic">Automatic</option>
          <option value="continuously variable">Continuously Variable</option>
          <option value="semi-automatic and dual-clutch">
            Semi-Automatic and Dual-Clutch
          </option>
        </select>
      </Form.Group>
      <Form.Group controlId="horse_power">
        <Form.Label>Horse Power</Form.Label>
        <Form.Control
          type="number"
          placeholder="Horse Power"
          value={props.carObj.horse_power}
          onChange={(e) => props.handleSelections(e, 'horse_power')}
        />
      </Form.Group>
      <Form.Group controlId="registration">
        <Form.Label>Registration (Plate) Number</Form.Label>
        <Form.Control
          type="text"
          placeholder="Registration Number"
          value={props.carObj.registration_plate}
          onChange={(e) => props.handleSelections(e, 'registration_plate')}
        />
      </Form.Group>
      <Form.Group controlId="vin">
        <Form.Label>VIN Number</Form.Label>
        <Form.Control
          type="text"
          placeholder="VIN Number"
          value={props.carObj.vin}
          onChange={(e) => props.handleSelections(e, 'vin')}
        />
      </Form.Group>
      <Form.Group controlId="car_image">
        <Form.Label>Car Image (Media URL/Link)</Form.Label>
        <Form.Control
          type="text"
          placeholder="Car Image (link)"
          value={props.carObj.car_image}
          onChange={(e) => props.handleSelections(e, 'car_image')}
        />
      </Form.Group>
    </Form>
  );
};

export default EditCarForm;
