import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import { useState } from "react";
import Col from "react-bootstrap/esm/Col";
import crypto from "crypto";
import { Link } from "react-router-dom";
import * as yup from "yup";

const Register = (props) => {
  const [email, updateEmail] = useState("");
  const [username, updateUsername] = useState("");
  const [password, updatePassword] = useState("");
  const [rePassword, updateRePassword] = useState("");

  const schema = yup.object().shape({
    email: yup.string().email("Invalid email.").required("Email is required."),
    username: yup
      .string()
      .min(5, "Too Short! Your username should have at least 5 characters.")
      .max(15, "Username must be 15 characters or less.")
      .required("Username is required."),
    password: yup
      .string()
      .min(8, "Too Short! Your password should have at least 8 characters.")
      .max(15, "Password must be 15 characters or less.")
      .matches(/[a-zA-Z]/, "Password can only contain Latin letters.") // JJ -> put here the desired regex
      .required("Password is required."),
    rePassword: yup
      .string()
      .oneOf([yup.ref("password"), null], "Passwords must match."),
  });

  const submitLoginForm = (e) => {
    e.preventDefault();

    if (password !== rePassword) {
      console.log("The passwords don't match");
      return;
    }

    const user = {
      id: crypto.randomBytes(16).toString("hex"),
      email,
      username,
      password,
    };

    props.updateUsers((prev) => [...prev, user]);
  };

  return (
    <Col lg={6}>
      <Form onSubmit={(e) => submitLoginForm(e)}>
        <Form.Group controlId="email">
          <Form.Label>Email address</Form.Label>
          <Form.Control
            name="email"
            type="email"
            placeholder="Enter email"
            onChange={(e) => updateEmail(e.target.value)}
          />
        </Form.Group>
        <Form.Group controlId="username">
          <Form.Label>Username</Form.Label>
          <Form.Control
            name="username"
            type="text"
            placeholder="Enter username"
            onChange={(e) => updateUsername(e.target.value)}
          />
        </Form.Group>

        <Form.Group controlId="password">
          <Form.Label>Password</Form.Label>
          <Form.Control
            name="password"
            type="password"
            placeholder="Password"
            onChange={(e) => updatePassword(e.target.value)}
          />
        </Form.Group>
        <Form.Group controlId="re-password">
          <Form.Label>Repeat Password</Form.Label>
          <Form.Control
            name="rePassword"
            type="password"
            placeholder="Repeat Password"
            onChange={(e) => updateRePassword(e.target.value)}
          />
        </Form.Group>
        <Form.Group controlId="have-an-account">
          <Link to="/login">Already have an account?</Link>
        </Form.Group>
        <Button variant="primary" type="submit">
          Register
        </Button>
      </Form>
    </Col>
  );
};

export default Register;
