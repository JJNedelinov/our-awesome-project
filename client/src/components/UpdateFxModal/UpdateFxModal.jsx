import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import ListGroup from 'react-bootstrap/ListGroup';
import { BASE_URL } from '../../common/config';
import { getToken } from '../../providers/authentication/authentication';
import { useContext, useState } from 'react';
import { FxContext } from '../../providers/fx/FxContext';

function UpdateFxModal(props) {
  const { fxData, setFxData } = useContext(FxContext);

  const [selectedCurrency, updateSelectedCurrency] = useState('');

  const refreshFx = () => {
    const requestOptions = {
      method: 'GET',
      redirect: 'follow',
      headers: {
        Authorization: `Bearer ${getToken()}`,
      },
    };

    fetch(`${BASE_URL}/fx`, requestOptions)
      .then((response) => response.json())
      .then((result) => {
        console.log(result);
        setFxData({
          ...result.data[0],
          defaultFX: false,
          currentCurrency: 'BGN',
          bgnToBGN: 1,
        });
      })
      // .then((result) => console.log(result.data[0]))
      .catch((error) => console.log('error', error));
  };

  const handleChange = () => {
    if (selectedCurrency === fxData.currentCurrency) {
      props.notChangedFx(
        `The FX Rate was not changed. The currency is still in ${fxData.currentCurrency}.`
      );
    } else {
      setFxData({ ...fxData, currentCurrency: selectedCurrency });
      props.updatedFx(
        `Successfully changed the FX Rate to ${selectedCurrency}!`
      );
    }
    updateSelectedCurrency('');
    props.setShow(false);
  };

  const handleCancelBrand = () => {
    updateSelectedCurrency('');
    props.setShow(false);
  };

  return (
    <Modal
      {...props}
      size="m"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header className="custom-title fx" closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Want a Different FX Rate?
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div className="change-fx">
          <p>Why Not Change It?</p>
          <div className="fx-options">
            <div
              onChange={(ev) => {
                // setFxData({ ...fxData, currentCurrency: ev.target.id });
                updateSelectedCurrency(ev.target.id);
                // console.log(ev.target.id)
              }}
            >
              <Form.Check
                inline
                label="BGN"
                name="group1"
                type="radio"
                id="BGN"
                // checked={fxData.currentCurrency === 'BGN'}
              />
              <Form.Check
                inline
                label="EUR"
                name="group1"
                type="radio"
                id="EUR"
                // checked={fxData.currentCurrency === 'EUR'}
              />
              <Form.Check
                inline
                label="USD"
                name="group1"
                type="radio"
                id="USD"
                // checked={fxData.currentCurrency === 'USD'}
              />
            </div>
            <div className="get-live-fx">
              <button onClick={() => refreshFx()}>Get FX</button>
            </div>
          </div>

          <ListGroup>
            {Object.entries(fxData)
              .sort((a, b) => {
                const [aKey] = a;
                const [bKey] = b;
                return aKey.localeCompare(bKey);
              })
              .map(([key, value]) => {
                if (value && key !== 'defaultFX') {
                  switch (key) {
                    case 'bgnToUSD':
                      key = 'BGN - USD';
                      break;
                    case 'bgnToEUR':
                      key = 'BGN - EUR';
                      break;
                    case 'bgnToBGN':
                      key = 'BGN - BGN';
                      break;
                    case 'currentCurrency':
                      key = 'Current Currency';
                      break;
                    case 'timeStamp':
                      key = 'Updated FX Rate At:';
                      break;
                    default:
                      break;
                  }
                  return (
                    <ListGroup.Item>
                      <span>{key}</span> {': '}
                      <span>{value.toString()}</span>
                    </ListGroup.Item>
                  );
                }
                return value;
              })}
          </ListGroup>

          <div className="no-buttons">
            <Button
              className="add-button"
              disabled={selectedCurrency ? false : true}
              onClick={handleChange}
            >
              Change
            </Button>
            <Button
              className="cancel-button"
              style={{
                marginLeft: '0.5em',
              }}
              onClick={handleCancelBrand}
            >
              Cancel
            </Button>
          </div>
        </div>
      </Modal.Body>
    </Modal>
  );
}

export default UpdateFxModal;
