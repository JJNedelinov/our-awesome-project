import { useContext } from "react";
import { FxContext } from "../providers/fx/FxContext";

const ShowPrice = ({ amount, targetCurrency }) => {
  const { fxData } = useContext(FxContext);

  return (
    <>
      <span>
        {(amount * fxData[`bgnTo${targetCurrency}`]).toLocaleString(
          targetCurrency === "BGN"
            ? "bg-BG"
            : targetCurrency === "EUR"
            ? "de-DE"
            : "en-US",
          {
            style: "currency",
            currency: `${targetCurrency}`,
          }
        )}
      </span>
    </>
  );
};

export default ShowPrice;
