import { useState, useEffect } from 'react';
import { BASE_URL } from '../../common/config';
import AddBrandModal from '../AddBrandModal';
import AddModelModal from '../AddModelModal';
import { toast } from 'react-toastify';
import NewUserForm from './NewUserForm';
import NewUserPageButtons from './NewUserPageButtons';
import NewUserHeading from './NewUserHeading';
import NewCarForm from '../AddNewCar/NewCarForm';
import NewCarPageButtons from '../AddNewCar/NewCarPageButtons';
import { getToken } from '../../providers/authentication/authentication';
import constants from '../../common/constants';

const AddNewCustomer = (props) => {
  const [createdUser, setCreatedUser] = useState({});
  const [userObj, updateUserObj] = useState(constants.initialUserObjState);
  const [addUser, updateAddUser] = useState(false);
  const [carObj, updateCarObj] = useState(constants.initialCarObjState);
  const [addCar, updateAddCar] = useState(false);
  const [brands, updateBrands] = useState([]);
  const [brand, updateBrand] = useState('');
  const [addBrand, updateAddBrand] = useState(false);
  const [selectedBrandId, updateSelectedBrandId] = useState('');
  const [models, updateModels] = useState([]);
  const [selectedModelId, updateSelectedModelId] = useState('');
  const [addModel, updateAddModel] = useState(false);
  const [model, updateModel] = useState('');
  const [modelYear, updateModelYear] = useState('');
  const [newBrand, updateNewBrand] = useState({});
  const [newModel, updateNewModel] = useState({});
  const [modelSelectField, setModelSelectField] = useState(true);

  const creationSuccess = (message) =>
    toast.success(message, {
      autoClose: 3000,
    });

  const creationFail = (message) => {
    toast.error(message, {
      autoClose: 3000,
    });
  };

  const handleAddAnother = () => {
    updateUserObj(constants.initialUserObjState);
    updateAddCar(false);
    updateAddUser(false);
  };

  const handleAddUser = (e) => {
    const url = `${BASE_URL}/${props.match.path.slice(5) + 's'}`;

    fetch(url, {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${getToken()}`,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(userObj),
    })
      .then((res) => res.json())
      .then((res) => {
        const { data, error } = res;

        if (error.length) throw new Error(error.join('\n'));

        setCreatedUser(data[0]);
        creationSuccess(
          `Successfully created a new ${props.match.path.slice(5)}`
        );
        updateAddUser(true);

        return res;
      })
      .catch((error) => creationFail(error.message));
  };

  const handleAddCar = (e) => {
    const url = `${BASE_URL}/cars?customers_id=${
      createdUser && createdUser.id
    }`;

    fetch(url, {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${getToken()}`,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(carObj),
    })
      .then((res) => res.json())
      .then((res) => {
        const { error } = res;

        if (error.length) throw new Error(error.join('\n'));

        creationSuccess(
          `Successfully created a new car for ${
            createdUser && `${createdUser.first_name} ${createdUser.last_name}`
          }`
        );
        updateAddCar(true);
        updateCarObj(constants.initialCarObjState);
        updateSelectedBrandId('');
        updateSelectedModelId('');
        setModelSelectField(true);

        return res;
      })
      .catch((error) => creationFail(error.message));
  };

  const handleSelections = (e, field) => {
    e.target.value = e.target.value.trim();

    switch (field) {
      case 'brand':
        updateSelectedBrandId(e.target.value);

        if (e.target.value && e.target.value !== 'no-brand') {
          const brand = brands.find((brand) => brand.id === +e.target.value);
          updateCarObj((prev) => ({ ...prev, brand: brand.name }));
          setModelSelectField(false);
        } else {
          updateCarObj((prev) => ({ ...prev, brand: '' }));
          updateSelectedModelId('');
          setModelSelectField(true);
        }
        break;
      case 'model':
        updateSelectedModelId(e.target.value);

        if (e.target.value && e.target.value !== 'no-model') {
          const model = models.find((model) => model.id === +e.target.value);
          updateCarObj((prev) => ({
            ...prev,
            model: model.name,
            year: model.year,
          }));
        } else {
          updateCarObj((prev) => ({ ...prev, model: '' }));
        }
        break;

      case 'coupe':
      case 'engine':
      case 'transmission':
      case 'horse_power':
      case 'registration_plate':
      case 'vin':
      case 'car_image':
        if (e.target.value) {
          updateCarObj((prev) => ({
            ...prev,
            [field]: e.target.value,
          }));
        } else updateCarObj((prev) => ({ ...prev, [field]: '' }));
        break;
      case 'first_name':
      case 'last_name':
      case 'email':
      case 'phone':
        if (e.target.value) {
          updateUserObj((prev) => ({
            ...prev,
            [field]: e.target.value,
          }));
        } else updateUserObj((prev) => ({ ...prev, [field]: '' }));
        break;
      default:
        break;
    }
  };

  useEffect(() => {
    let mounted = true;

    fetch(`${BASE_URL}/brands`)
      .then((res) => res.json())
      .then((res) => {
        const { data } = res;

        if (mounted) {
          updateBrands(data);
        }

        return res;
      })
      .catch(console.error);

    return () => (mounted = false);
  }, [newBrand]);

  useEffect(() => {
    let mounted = true;

    fetch(`${BASE_URL}/models?brands_id=${selectedBrandId}`)
      .then((res) => res.json())
      .then((res) => {
        const { data } = res;

        if (mounted) {
          updateModels(data);
        }

        return res;
      })
      .catch(console.error);

    return () => (mounted = false);
  }, [selectedBrandId, newModel]);

  return (
    <>
      <div className="add-new">
        <div className="details customer-details">
          <div
            className="overlay"
            style={{ display: addUser ? 'none' : 'block' }}
          ></div>
          <NewUserHeading
            {...props}
            addUser={addUser}
            handleAddAnother={handleAddAnother}
          />
          <NewUserForm
            {...props}
            userObj={userObj}
            addUser={addUser}
            updateAddUser={updateAddUser}
            setCreatedUser={setCreatedUser}
            handleAddAnother={handleAddAnother}
            handleSelections={handleSelections}
            updateAddCar={updateAddCar}
            creationSuccess={creationSuccess}
            creationFail={creationFail}
          />
        </div>
        <div className="add-page-buttons">
          <NewUserPageButtons
            {...props}
            addUser={addUser}
            handleAddUser={handleAddUser}
            updateAddCar={updateAddCar}
            handleAddAnother={handleAddAnother}
          />
        </div>
        <div className="details car-details">
          <div
            className="overlay"
            style={{ display: addCar ? 'none' : 'block' }}
          ></div>
          <div className="details-heading">
            <h3>Car Details</h3>
          </div>
          <div className="car-content">
            <div className="view-car-img">
              <img
                src={
                  carObj.car_image
                    ? carObj.car_image
                    : 'https://www.maxim.com/.image/t_share/MTM3NTcwOTYwNjM0MTYwNTU3/grey-gt500cr_002jpg.jpg'
                }
                alt="example car"
              />
            </div>
            <AddBrandModal
              {...props}
              brand={brand}
              updateNewBrand={updateNewBrand}
              selectedBrandId={selectedBrandId}
              updateSelectedBrandId={updateSelectedBrandId}
              updateCarObj={updateCarObj}
              addBrand={addBrand}
              updateBrand={updateBrand}
              updateAddBrand={updateAddBrand}
              setModelSelectField={setModelSelectField}
              creationSuccess={creationSuccess}
              creationFail={creationFail}
              show={addBrand}
              onHide={() => updateAddBrand(false)}
            />
            <AddModelModal
              {...props}
              model={model}
              updateModel={updateModel}
              modelYear={modelYear}
              updateModelYear={updateModelYear}
              carObj={carObj}
              updateCarObj={updateCarObj}
              selectedBrandId={selectedBrandId}
              updateSelectedModelId={updateSelectedModelId}
              addModel={addModel}
              updateAddModel={updateAddModel}
              updateNewModel={updateNewModel}
              creationSuccess={creationSuccess}
              creationFail={creationFail}
              show={addModel}
              onHide={() => updateAddModel(false)}
            />

            <NewCarForm
              {...props}
              brands={brands}
              addBrand={addBrand}
              models={models}
              addModel={addModel}
              carObj={carObj}
              updateAddModel={updateAddModel}
              updateAddBrand={updateAddBrand}
              modelSelectField={modelSelectField}
              selectedModelId={selectedModelId}
              selectedBrandId={selectedBrandId}
              handleSelections={handleSelections}
            />
          </div>
        </div>
        <div className="add-page-buttons">
          <NewCarPageButtons
            {...props}
            addCar={addCar}
            handleAddCar={handleAddCar}
          />
        </div>
      </div>
    </>
  );
};

export default AddNewCustomer;
