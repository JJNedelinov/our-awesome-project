import Form from 'react-bootstrap/Form';
import * as yup from 'yup';

const NewUserForm = (props) => {
  const schema = yup.object().shape({
    firstname: yup
      .string()
      .min(2, 'Too Short! First name should have at least 2 characters.')
      .max(15, 'First name  must be 15 characters or less.')
      .required('First name is required.'),
    lastname: yup
      .string()
      .min(2, 'Too Short! Last name should have at least 2 characters.')
      .max(15, 'Last name  must be 15 characters or less.')
      .required('LOast name is required.'),
    email: yup.string().email('Invalid email.').required('Email is required.'),
    phonenumber: yup
      .string()
      .matches(
        /^(?:\+\d{1,3}|0\d{1,3}|00\d{1,2})?(?:\s?\(\d+\))?(?:[-\/\s.]|\d)+$/,
        'Phone number is not valid.'
      ) // JJ -> put here the desired regex
      .required('Phone number is required.'),
  });
  return (
    <>
      <Form className="filter-search-form">
        <Form.Group controlId="fname">
          <Form.Label>First Name</Form.Label>
          <Form.Control
            type="text"
            placeholder="First Name"
            value={props.userObj && props.userObj.first_name}
            disabled={props.addUser}
            onChange={(e) => props.handleSelections(e, 'first_name')}
          />
        </Form.Group>
        <Form.Group controlId="lname">
          <Form.Label>Last Name</Form.Label>
          <Form.Control
            type="text"
            placeholder="Last Name"
            value={props.userObj && props.userObj.last_name}
            disabled={props.addUser}
            onChange={(e) => props.handleSelections(e, 'last_name')}
          />
        </Form.Group>
        <Form.Group controlId="email">
          <Form.Label>Email</Form.Label>
          <Form.Control
            type="text"
            placeholder="Email"
            value={props.userObj && props.userObj.email}
            disabled={props.addUser}
            onChange={(e) => props.handleSelections(e, 'email')}
          />
        </Form.Group>
        <Form.Group controlId="phone-number">
          <Form.Label>Phone Number</Form.Label>
          <Form.Control
            type="text"
            placeholder="Phone Number"
            value={props.userObj && props.userObj.phone}
            disabled={props.addUser}
            onChange={(e) => props.handleSelections(e, 'phone')}
          />
        </Form.Group>
      </Form>
    </>
  );
};

export default NewUserForm;
