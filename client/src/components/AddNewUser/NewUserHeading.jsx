const NewUserHeading = (props) => {
  return (
    <div className="details-heading">
      <h3>
        {props.match.path
          .slice(5)[0]
          .toUpperCase()
          .concat(props.match.path.slice(6))}{' '}
        Details
      </h3>
    </div>
  );
};

export default NewUserHeading;
