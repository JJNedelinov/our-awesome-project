import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import { BASE_URL } from '../../common/config';
import { useState } from 'react';
import { getToken } from '../../providers/authentication/authentication';
import { toast } from 'react-toastify';

function NewEmployeeModal(props) {
  const [employeeProps, updateEmployeeProps] = useState({});

  const creationSuccess = (message) =>
    toast.success(message, {
      autoClose: 3000,
    });

  const creationFail = (message) => {
    toast.error(message, {
      autoClose: 3000,
    });
  };

  const handleCancel = () => {
    props.updateShowModal(false);
    // updateModel('');
    // updateModelYear('');
    // updateAddModel(false);
  };

  const handleAdd = () => {
    fetch(`${BASE_URL}/employees`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${getToken()}`,
      },
      body: JSON.stringify(employeeProps),
    })
      .then((res) => res.json())
      .then((res) => {
        const { error } = res;

        if (error.length) throw new Error(error.join('\n'));

        props.updateShowModal(false);
        props.updateAddedUser(prev => prev + 1);
        updateEmployeeProps({});
        creationSuccess(
          `Succesfully created a new Employee with email: ${employeeProps.email}!`
        );
      })
      .catch((error) => creationFail(error.message));
  };

  const handleSelections = (e, field) => {
    e.target.value = e.target.value.trim();

    updateEmployeeProps((prev) => ({ ...prev, [field]: e.target.value }));
  };

  return (
    <>
      <Modal
        {...props}
        size="m"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header className="custom-title" closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Add New Employee?
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div
            className="no-model"
            // style={{
            //   display:
            //     selectedBrandId && selectedBrandId !== 'no-brand'
            //       ? 'block'
            //       : 'none',
            // }}
          >
            {/* <p>Add a Model For Selected Brand</p> */}
            <div
              className="add-model"
              // style={{ display: 'block' }}
            >
              <div className="no-inputs">
                <label htmlFor="first-name">First Name:</label>
                <input
                  id="first-name"
                  style={{ marginBottom: '1em' }}
                  type="text"
                  placeholder="First Name"
                  className="form-control"
                  // value={employeeProps.first_name ?? ''}
                  onChange={(e) => handleSelections(e, 'first_name')}
                />
                <label htmlFor="last-name">Last Name:</label>
                <input
                  id="last-name"
                  style={{ marginBottom: '1em' }}
                  type="text"
                  placeholder="Last Name"
                  className="form-control"
                  onChange={(e) => handleSelections(e, 'last_name')}
                />
                <label htmlFor="email">Email:</label>
                <input
                  id="email"
                  style={{ marginBottom: '1em' }}
                  type="email"
                  placeholder="Email"
                  className="form-control"
                  onChange={(e) => handleSelections(e, 'email')}
                />
                <label htmlFor="phone">Phone Number</label>
                <input
                  id="phone"
                  style={{ marginBottom: '.5em' }}
                  type="text"
                  placeholder="Phone Number"
                  className="form-control"
                  onChange={(e) => handleSelections(e, 'phone')}
                />
              </div>
            </div>
            <div className="no-buttons">
              <Button
                className="add-button"
                style={
                  {
                    // display: addModel ? 'inline' : 'none',
                  }
                }
                onClick={handleAdd}
              >
                Add
              </Button>
              <Button
                className="cancel-button"
                style={{
                  // display: addModel ? 'inline' : 'none',
                  marginLeft: '0.5em',
                }}
                onClick={handleCancel}
              >
                Cancel
              </Button>
            </div>
          </div>
        </Modal.Body>
      </Modal>
    </>
  );
}

export default NewEmployeeModal;
