import Button from 'react-bootstrap/Button';

const NewUserPageButtons = (props) => {
  return (
    <>
      <div>
        {!props.addUser ? (
          <Button
            variant="primary"
            className="filter-submit"
            // disabled={props.addUser}
            onClick={(e) => props.handleAddUser(e)}
          >
            Add{' '}
            {props.match.path
              .slice(5)[0]
              .toUpperCase()
              .concat(props.match.path.slice(6))}
          </Button>
        ) : (
          <Button
            className="filter-submit"
            // style={{ display: props.addUser ? 'block' : 'none' }}
            onClick={props.handleAddAnother}
          >
            Add Another{' '}
            {props.match.path
              .slice(5)[0]
              .toUpperCase()
              .concat(props.match.path.slice(6))}
            ?
          </Button>
        )}
      </div>
      <div>
        <Button
          variant="primary"
          className="filter-submit"
          disabled={!props.addUser}
          onClick={(e) => props.updateAddCar(true)}
        >
          Will You Add a Car?
          {/* Add Car For {Object.keys(createdUser).length ?? createdUser.first_name}? */}
          {/* Add a Car For Vladimir? */}
        </Button>
      </div>
    </>
  );
};

export default NewUserPageButtons;
