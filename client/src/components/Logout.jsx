import { useEffect, useContext } from 'react';
import { toast } from 'react-toastify';
import AuthContext from '../providers/authentication/authentication';

const Logout = (props) => {
  const { setAuthState } = useContext(AuthContext);

  useEffect(() => {
    localStorage.setItem('token', null);
    setAuthState({
      isLoggedIn: false,
      user: null,
    });
    toast.success('Successfully logged out of your account!', {
      autoClose: 3000,
    });
  }, [setAuthState]);

  return <h1>You've logged out!</h1>;
};

export default Logout;
