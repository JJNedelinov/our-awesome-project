import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Col from 'react-bootstrap/esm/Col';
import { Link, NavLink } from 'react-router-dom';
import { toast } from 'react-toastify';
import tipTopGarageLogo from '../assets/tipTopGarageLogo.png';
import * as yup from 'yup';

const LoginForm = ({
  submitLoginForm,
  updateEmail,
  updatePassword,
  loginOption,
  updateLoginOption,
}) => {
  const notify = (role) =>
    toast.info(`Login as ${role[0].toUpperCase() + role.slice(1)}!`, {
      autoClose: 3000,
    });

  const loginOptionHandler = (e, role) => {
    notify(role);
    updateLoginOption(role);
  };

  const schema = yup.object().shape({
    email: yup.string().email('Invalid email.').required('Email is required.'),
    password: yup.string().required('Password is required.'),
  });

  return (
    <>
      <Col className="login-form-container" lg={4}>
        <div className="login-form-logo">
          <NavLink className="button-logo" to="/">
            <img alt="" src={tipTopGarageLogo} />
          </NavLink>
        </div>
        <Form className="login-form" onSubmit={(e) => submitLoginForm(e)}>
          <Form.Group controlId="email">
            <Form.Label>Email address</Form.Label>
            <Form.Label className="input-icon">
              <img
                src="https://img.icons8.com/material-outlined/100/000000/gender-neutral-user.png"
                alt="email-icon"
              />
            </Form.Label>
            <Form.Control
              type="email"
              placeholder="Enter email"
              onChange={(e) => updateEmail(e.target.value)}
            />
          </Form.Group>
          <Form.Group controlId="password">
            <Form.Label>Password</Form.Label>
            <Form.Label className="input-icon">
              <img
                src="https://img.icons8.com/wired/100/000000/forgot-password.png"
                alt="password-icon"
              />
            </Form.Label>
            <Form.Control
              type="password"
              placeholder="Password"
              onChange={(e) => updatePassword(e.target.value)}
            />
          </Form.Group>
          <Form.Group className="have-an-account">
            {loginOption === 'employee' ? (
              <Link
                to="/login"
                onClick={(e) => loginOptionHandler(e, 'customer')}
              >
                Made a Mistake? <span>Login as Customer</span>
              </Link>
            ) : (
              <Link
                to="/login"
                onClick={(e) => loginOptionHandler(e, 'employee')}
              >
                Made a Mistake? <span>Login as Employee</span>
              </Link>
            )}
          </Form.Group>
          <Button variant="primary" type="submit">
            Login
          </Button>
        </Form>
      </Col>
    </>
  );
};

export default LoginForm;
