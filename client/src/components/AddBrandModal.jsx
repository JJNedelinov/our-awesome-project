import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import { BASE_URL } from '../common/config';
import { getToken } from '../providers/authentication/authentication';

function AddBrandModal(props) {
  const handleCancelBrand = () => {
    props.updateBrand('');
    props.updateAddBrand(false);
  };

  const handleAddBrand = () => {
    fetch(`${BASE_URL}/brands`, {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${getToken()}`,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ name: props.brand }),
    })
      .then((res) => res.json())
      .then((res) => {
        const { data } = res;
        props.updateCarObj((prev) => ({ ...prev, brand: data[0].name }));
        props.updateNewBrand(data[0]);
        props.updateSelectedBrandId(data[0].id);
        props.updateBrand('');
        props.updateAddBrand(false);
        props.setModelSelectField(false);
        props.creationSuccess('Succesfully added a new brand to our Database!');
      })
      .catch(console.error);
  };

  return (
    <Modal
      {...props}
      size="m"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header className="custom-title" closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Don't See The Needed Brand?
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div
          className="no-brand"
          style={{
            display: props.selectedBrandId === 'no-brand' ? 'block' : 'none',
          }}
        >
          {/* <p>Don't See The Brand You Need?</p> */}
          <p>Why Not Add It?</p>
          <div
            className="add-brand"
            style={{ display: props.addBrand ? 'block' : 'none' }}
          >
            <input
              type="text"
              placeholder="Add a New Brand"
              className="form-control"
              value={props.brand ?? ''}
              onChange={(e) => props.updateBrand(e.target.value)}
            />
          </div>
          <div className="no-buttons">
            <Button
              className="add-button"
              style={{
                display: props.addBrand ? 'inline' : 'none',
              }}
              onClick={handleAddBrand}
            >
              Add
            </Button>
            <Button
              className="cancel-button"
              style={{
                display: props.addBrand ? 'inline' : 'none',
                marginLeft: '0.5em',
              }}
              onClick={handleCancelBrand}
            >
              Cancel
            </Button>
          </div>
        </div>
      </Modal.Body>
    </Modal>
  );
}

export default AddBrandModal;
