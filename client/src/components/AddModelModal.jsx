import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import { BASE_URL } from '../common/config';
import { getToken } from '../providers/authentication/authentication';

function AddModelModal(props) {
  const handleCancelModel = () => {
    props.updateModel('');
    props.updateModelYear('');
    props.updateAddModel(false);
  };

  const handleAddModel = () => {
    console.log(props.model);
    console.log(props.selectedBrandId);

    fetch(`${BASE_URL}/models?brands_id=${props.selectedBrandId}`, {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${getToken()}`,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ name: props.model, year: props.modelYear }),
    })
      .then((res) => res.json())
      .then((res) => {
        const { data } = res;
        props.updateCarObj((prev) => ({
          ...prev,
          model: data[0].name,
          year: data[0].year,
        }));
        props.updateNewModel(data[0]);
        props.updateSelectedModelId(data[0].id);
        props.updateModel('');
        props.updateModelYear('');
        props.updateAddModel(false);
        props.creationSuccess(
          'Succesfully added a new model for the selected brand to our Database!'
        );

        return res;
      })
      .catch(console.error);
  };

  return (
    <Modal
      {...props}
      size="m"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header className="custom-title" closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Don't See The Needed Model?
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div
          className="no-model"
          style={{
            display:
              props.selectedBrandId && props.selectedBrandId !== 'no-brand'
                ? 'block'
                : 'none',
          }}
        >
          {/* <p>Add a Model For Selected Brand</p> */}
          <div
            className="add-model"
            style={{ display: props.addModel ? 'block' : 'none' }}
          >
            <div className="no-inputs">
              <label htmlFor="brand-model">
                Model For The Selected Brand -{' '}
                <b>{props.carObj && props.carObj.brand}</b>
              </label>
              <input
                id="brand-model"
                style={{ marginBottom: '1em' }}
                type="text"
                placeholder="Model"
                className="form-control"
                onChange={(e) => props.updateModel(e.target.value)}
              />
              <label htmlFor="year">Year of Manufacture</label>
              <input
                id="year"
                type="date"
                placeholder="Year"
                className="form-control"
                onChange={(e) => props.updateModelYear(e.target.value)}
              />
            </div>
          </div>
          <div className="no-buttons">
            <Button
              className="add-button"
              style={{
                display: !props.addModel ? 'inline' : 'none',
              }}
              onClick={() => props.updateAddModel(true)}
            >
              Add a Model
            </Button>
            <Button
              className="add-button"
              style={{
                display: props.addModel ? 'inline' : 'none',
              }}
              onClick={handleAddModel}
            >
              Add
            </Button>
            <Button
              className="cancel-button"
              style={{
                display: props.addModel ? 'inline' : 'none',
                marginLeft: '0.5em',
              }}
              onClick={handleCancelModel}
            >
              Cancel
            </Button>
          </div>
        </div>
      </Modal.Body>
    </Modal>
  );
}

export default AddModelModal;
