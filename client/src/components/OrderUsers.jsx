const OrderUsers = (props) => {
  return (
    <div className="order">
      <p>Order by:</p>
      <div className="order-container">
        <p onClick={(e) => props.handleOrderBy(e)}>
          Name{' '}
          {props.orderByName === 'asc' ? (
            <span>&#8595;</span>
          ) : (
            <span>&#8593;</span>
          )}
        </p>
        {props.match.path.includes('customers') ? (
          <p onClick={(e) => props.handleOrderBy(e)}>
            Visit Date{' '}
            {props.orderByVisitDate === 'asc' ? (
              <span>&#8595;</span>
            ) : (
              <span>&#8593;</span>
            )}
          </p>
        ) : (
          ''
        )}
        <p onClick={(e) => props.handleResetOrder(e)}>Default Order</p>
      </div>
    </div>
  );
};

export default OrderUsers;
