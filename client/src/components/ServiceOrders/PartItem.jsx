import { useState } from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import ListGroup from "react-bootstrap/ListGroup";
import { Formik } from "formik";
import * as yup from "yup";

const PartItem = ({ partItemData, addPartToOrder }) => {
  // console.log(partItemData);

  const [partItemLocalData, setPartItemLocalData] = useState(partItemData);
  const [isSelected, setIsSelected] = useState(false);

  const schema = yup.object().shape({
    totalUnits: yup
      .number()
      .integer("Should be an integer value.")
      .positive("Should be a positive value."),
  });

  return (
    <Formik
      validationSchema={schema}
      initialValues={{
        totalUnits: partItemLocalData.total_units,
      }}
    >
      {({ values, touched, errors, handleBlur }) => (
        <Card key={partItemLocalData.id}>
          <Card.Header>
            <h5>{partItemLocalData.name}</h5>
          </Card.Header>

          <ListGroup variant="flush">
            <ListGroup.Item>Car: {partItemLocalData.car}</ListGroup.Item>
            <ListGroup.Item>
              Manufacturer: {partItemLocalData.brand}
            </ListGroup.Item>
            <ListGroup.Item>Code: {partItemLocalData.product}</ListGroup.Item>
            <ListGroup.Item>
              Price per item: {partItemLocalData.unit_price}
            </ListGroup.Item>
            <ListGroup.Item>Unit type: {partItemLocalData.unit}</ListGroup.Item>
            <ListGroup.Item>
              {isSelected ? (
                <Form.Group>
                  <Form.Label>Qty:</Form.Label>
                  <Form.Control
                    onBlur={handleBlur}
                    type="number"
                    name="totalUnits"
                    value={values.totalUnits}
                    onChange={(ev) => {
                      setPartItemLocalData({
                        ...partItemLocalData,
                        total_units: ev.target.value,
                      });
                      values.totalUnits = ev.target.value;
                    }}
                    isInvalid={touched.totalUnits && !!errors.totalUnits}
                  />
                  <Form.Control.Feedback type="invalid">
                    {errors.totalUnits}
                  </Form.Control.Feedback>
                </Form.Group>
              ) : (
                <>Qty: {partItemLocalData.total_units}</>
              )}{" "}
            </ListGroup.Item>
            <ListGroup.Item>
              Total price:{" "}
              {partItemLocalData.total_units
                ? partItemLocalData.total_units * partItemLocalData.unit_price
                : 0}
            </ListGroup.Item>
            <ListGroup.Item>
              <Form.Check
                inline
                label="Edit"
                onChange={() => setIsSelected(!isSelected)}
              />
              <Button
                size="sm"
                variant="link"
                onClick={() => addPartToOrder(partItemLocalData)}
              >
                Add
              </Button>
            </ListGroup.Item>
          </ListGroup>
        </Card>
      )}
    </Formik>
  );
};

export default PartItem;
