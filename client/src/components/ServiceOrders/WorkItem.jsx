import { useState } from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import ListGroup from "react-bootstrap/ListGroup";
import { Formik } from "formik";
import * as yup from "yup";

const WorkItem = ({ workItemData, addWorktoOrder }) => {
  const [workItemLocalData, setWorkItemLocalData] = useState(workItemData);
  const [isSelected, setIsSelected] = useState(false);

  const schema = yup.object().shape({
    manHours: yup
      .number()
      .integer("Should be an integer value.")
      .positive("Should be a positive value."),
  });

  return (
    <Formik
      validationSchema={schema}
      initialValues={{
        manHours: workItemLocalData.min_man_hours,
      }}
    >
      {({ values, touched, errors, handleBlur }) => (
        <Card key={workItemLocalData.id}>
          <Card.Header>
            <h5>{workItemLocalData.name}</h5>
          </Card.Header>

          <ListGroup variant="flush">
            <ListGroup.Item>
              Price per man-hour: {workItemLocalData.price}
            </ListGroup.Item>
            <ListGroup.Item>
              Price category: {workItemLocalData.priceCategory}
            </ListGroup.Item>
            <ListGroup.Item>
              {isSelected ? (
                <Form.Group>
                  <Form.Label>Man-hours:</Form.Label>
                  <Form.Control
                    onBlur={handleBlur}
                    type="number"
                    name="manHours"
                    value={values.manHours}
                    onChange={(ev) => {
                      setWorkItemLocalData({
                        ...workItemLocalData,
                        min_man_hours: ev.target.value,
                      });
                      values.manHours = ev.target.value;
                    }}
                    isInvalid={touched.manHours && !!errors.manHours}
                  />
                  <Form.Control.Feedback type="invalid">
                    {errors.manHours}
                  </Form.Control.Feedback>
                </Form.Group>
              ) : (
                <>Min man-hours: {workItemLocalData.min_man_hours}</>
              )}{" "}
            </ListGroup.Item>
            <ListGroup.Item>
              Total price:{" "}
              {workItemLocalData.min_man_hours * workItemLocalData.price}
            </ListGroup.Item>
            <ListGroup.Item>
              <Form.Check
                inline
                label="Edit"
                onChange={() => setIsSelected(!isSelected)}
              />
              <Button
                size="sm"
                variant="link"
                onClick={() => addWorktoOrder(workItemLocalData)}
              >
                Add
              </Button>
            </ListGroup.Item>
          </ListGroup>
        </Card>
      )}
    </Formik>
  );
};

export default WorkItem;
