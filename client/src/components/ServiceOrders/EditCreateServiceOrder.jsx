import React, { useState } from 'react';
import Form from 'react-bootstrap/Form';
import FormControl from 'react-bootstrap/FormControl';
import { Formik } from 'formik';
import * as yup from 'yup';
import Button from 'react-bootstrap/Button';
import ListGroup from 'react-bootstrap/ListGroup';
import { toast } from 'react-toastify';
import { BASE_URL } from '../../common/config';

import WorkItem from './WorkItem';
import PartItem from './PartItem';
import { getToken } from '../../providers/authentication/authentication';

const EditCreateServiceOrder = ({ location, history }) => {
  const [workItemsCatalogue, setWorkItemsCatalogue] = useState([]);
  const [searchWorks, setSearchWorks] = useState('');
  const [searchCustomer, setSearchCustomer] = useState('');
  const [description, setDescription] = useState('');
  const [rates, setRates] = useState([]);
  const [carBrands, setCarBrands] = useState([]);
  const [partItemsCatalogue, setPartItemsCatalogue] = useState([]);
  const [searchParts, setSearchParts] = useState('');
  const [worksHistory, setWorksHistory] = useState([]);
  const [partsHistory, setPartsHistory] = useState([]);
  const [customers, setCustomers] = useState([]);
  const [selectedCustomer, setSelectedCustomer] = useState({});
  const [selectedCar, setSelectedCar] = useState({});
  const [cars, setCars] = useState([]);

  const success = (message) => {
    console.log('toast success func');
    toast.success(message, {
      autoClose: 1500,
    });
  };

  const fail = (message) => {
    console.log('toast fail func');
    toast.error(message, {
      autoClose: 1100,
    });
  };

  const schema = yup.object().shape({
    description: yup.string().required(),
  });

  const handlePartsSearch = (searchTerm) => {
    const myHeaders = new Headers();
    myHeaders.append('Authorization', `Bearer ${getToken()}`);
    const requestOptions = {
      method: 'GET',
      headers: myHeaders,
    };
    fetch(`${BASE_URL}/brands`, requestOptions)
      .then((response) => response.json())
      .then((data) => setCarBrands(data.data))
      .then(() => {
        const myHeaders = new Headers();
        myHeaders.append('Authorization', `Bearer ${getToken()}`);
        const requestOptions = {
          method: 'GET',
          headers: myHeaders,
        };
        fetch(
          `${BASE_URL}/catalogue/parts?search=${searchTerm}`,
          requestOptions
        )
          .then((response) => response.json())
          .then((data) => setPartItemsCatalogue(data.data))
          .catch((error) => console.log('error', error));
      })
      .catch((error) => console.log('error', error));
  };

  const handleWorksSearch = (searchTerm) => {
    const myHeaders = new Headers();
    myHeaders.append('Authorization', `Bearer ${getToken()}`);
    const requestOptions = {
      method: 'GET',
      headers: myHeaders,
    };
    fetch(`${BASE_URL}/catalogue/rates`, requestOptions)
      .then((response) => response.json())
      .then((data) => setRates(data.data))
      .then(() => {
        const myHeaders = new Headers();
        myHeaders.append('Authorization', `Bearer ${getToken()}`);
        const requestOptions = {
          method: 'GET',
          headers: myHeaders,
        };
        fetch(
          `${BASE_URL}/catalogue/works?search=${searchTerm}`,
          requestOptions
        )
          .then((response) => response.json())
          .then((data) => setWorkItemsCatalogue(data.data))
          .catch((error) => console.log('error', error));
      })
      .catch((error) => console.log('error', error));
  };

  const handleCustomerSearch = (searchTerm) => {
    const myHeaders = new Headers();
    myHeaders.append('Authorization', `Bearer ${getToken()}`);
    const requestOptions = {
      method: 'GET',
      headers: myHeaders,
    };

    fetch(`${BASE_URL}/customers?name=${searchTerm}`, requestOptions)
      .then((response) => response.json())
      .then((result) => setCustomers(result.data))

      .catch((error) => console.log('error', error));
  };
  // !car
  //   ? setCustomer(
  //       result.data.find(
  //         (customer) =>
  //           customer.first_name
  //             .toLowerCase()
  //             .includes((searchTerm ? searchTerm : " ").toLowerCase()) ||
  //           customer.last_name
  //             .toLowerCase()
  //             .includes((searchTerm ? searchTerm : " ").toLowerCase())
  //       )
  //     )
  // : setCustomer(
  //     result.data.find(
  //       (customer) =>
  //         (customer.first_name
  //           .toLowerCase()
  //           .includes((searchTerm ? searchTerm : " ").toLowerCase()) ||
  //           customer.last_name
  //             .toLowerCase()
  //             .includes(
  //               (searchTerm ? searchTerm : " ").toLowerCase()
  //             )) &&
  //         car.customers_id === customer.id
  //     )
  // );

  const handleCarSearch = (customerId) => {
    const myHeaders = new Headers();
    myHeaders.append('Authorization', `Bearer ${getToken()}`);
    const requestOptions = {
      method: 'GET',
      headers: myHeaders,
    };

    fetch(`${BASE_URL}/cars?customers_id=${customerId}`, requestOptions)
      .then((response) => response.json())
      .then((result) => setCars(result.data))

      .catch((error) => console.log('error', error));
  };

  const addWorktoOrder = (workItem) => {
    setWorksHistory([...worksHistory, workItem]);
    success('Work item added to service order!');
  };

  const addPartToOrder = (partItem) => {
    setPartsHistory([...partsHistory, partItem]);
    success('Part item added to service order!');
  };

  const checkServiceOrderData = (
    customers_id,
    cars_id,
    description,
    worksHistory,
    partsHistory
  ) => {
    switch (true) {
      case !customers_id:
        return 'Customer not selected.';
      case !cars_id:
        return 'Car not selected.';
      case !description:
        return 'Please enter description.';
      case !worksHistory.length && !partsHistory.length:
        return 'Please select at least one work item or one part item.';
      default:
        return 'passed';
    }
  };

  const submitNewServiceOrder = (
    customers_id,
    cars_id,
    description,
    status,
    visit_date,
    worksHistory,
    partsHistory
  ) => {
    const myHeaders = new Headers();
    myHeaders.append('content-type', 'application/json');
    myHeaders.append('Authorization', `Bearer ${getToken()}`);

    const raw = JSON.stringify({
      customers_id,
      cars_id,
      description,
      status,
      visit_date,
    });

    const requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: raw,
    };

    fetch(`${BASE_URL}/service-orders`, requestOptions)
      .then((response) => response.json())
      .then((result) => {
        Promise.all(
          worksHistory.map((workItem) => {
            const myHeaders = new Headers();
            myHeaders.append('content-type', 'application/json');
            myHeaders.append('Authorization', `Bearer ${getToken()}`);
            const raw = JSON.stringify({
              name: workItem.name,
              man_hours: workItem.min_man_hours,
              service_orders_id: +result.data[0].id,
            });
            const requestOptions = {
              method: 'POST',
              headers: myHeaders,
              body: raw,
            };
            fetch(`${BASE_URL}/works-history`, requestOptions)
              .then((response) => response.json())
              .then((result) => {
                const myHeaders = new Headers();
                myHeaders.append('content-type', 'application/json');
                myHeaders.append('Authorization', `Bearer ${getToken()}`);

                const raw = JSON.stringify({
                  name: workItem.priceCategory,
                  price: workItem.price,
                  works_history_id: result.data[0].id,
                });
                const requestOptions = {
                  method: 'POST',
                  headers: myHeaders,
                  body: raw,
                };
                fetch(`${BASE_URL}/rates-history`, requestOptions).catch(
                  (error) => console.log('error', error)
                );
              })
              .catch((error) => console.log('error', error));
          })
        ).catch((error) => console.log('error', error));

        Promise.all(
          partsHistory.map((partItem) => {
            const myHeaders = new Headers();
            myHeaders.append('content-type', 'application/json');
            myHeaders.append('Authorization', `Bearer ${getToken()}`);
            const raw = JSON.stringify({
              name: partItem.name,
              unit: partItem.unit,
              unit_price: partItem.unit_price,
              brand: partItem.brand,
              product: partItem.product,
              total_units: partItem.total_units,
              service_orders_id: +result.data[0].id,
            });
            const requestOptions = {
              method: 'POST',
              headers: myHeaders,
              body: raw,
            };
            fetch(`${BASE_URL}/parts-history`, requestOptions)
              .then((response) => response.json())
              .catch((error) => console.log('error', error));
          })
        );
      })
      .then(() => success('Service order submitted!'))
      .catch((error) => console.log('error', error));
  };

  return (
    <div
      className="edit-create-service-order"
      style={{
        minHeight:
          customers.length === 0
            ? cars.length === 0
              ? workItemsCatalogue.length === 0
                ? partItemsCatalogue.length === 0
                  ? '74%'
                  : 'auto'
                : 'auto'
              : 'auto'
            : 'auto',
      }}
    >
      <h3>
        <span>Create Service Order</span>{' '}
        <Button
          variant="link"
          
          onClick={(ev) => {
            ev.preventDefault();
            const message = checkServiceOrderData(
              selectedCustomer.id,
              selectedCar.car_id,
              description,
              worksHistory,
              partsHistory
            );
            message === 'passed'
              ? submitNewServiceOrder(
                  selectedCustomer.id,
                  selectedCar.car_id,
                  description,
                  'New',
                  new Date().toISOString().substr(0, 10),
                  worksHistory,
                  partsHistory
                )
              : fail(message);
          }}
        >
          Submit
        </Button>{' '}
        <Button
          variant="link"
          
          onClick={() => {
            history.goBack();
          }}
        >
          Cancel
        </Button>
        <Button
          
          variant="link"
          onClick={() => {
            setCustomers([]);
            setSelectedCustomer({});
            setCars([]);
            setPartItemsCatalogue([]);
            setPartsHistory([]);
            setWorkItemsCatalogue([]);
            setWorksHistory([]);
          }}
        >
          Clear
        </Button>
      </h3>
      <ListGroup className="create-seo">
        <ListGroup.Item>
          <div>
            <h5 id="customer">
              <span>Select customer:</span>

              <Form inline className="search-field">
                <FormControl
                  
                  type="text"
                  placeholder="Search by name"
                  onChange={(ev) => setSearchCustomer(ev.target.value)}
                />
                <Button
                  
                  variant="outline-success"
                  onClick={() => handleCustomerSearch(searchCustomer)}
                >
                  Search
                </Button>
              </Form>
            </h5>
            <ListGroup style={{marginTop: '.5em'}}
              onChange={(ev) => {
                setSelectedCustomer(
                  customers.find((cus) => cus.id === +ev.target.id)
                );
              }}
            >
              {customers.map((customer) => (
                <ListGroup.Item key={customer.id}>
                  <span>
                    {' '}
                    <Form.Check
                      inline
                      name="group1"
                      type="radio"
                      id={customer.id}
                    />
                    {customer.first_name} {customer.last_name} {customer.email}{' '}
                  </span>
                </ListGroup.Item>
              ))}
            </ListGroup>
          </div>
        </ListGroup.Item>
        <ListGroup.Item>
          <div>
            <h5 id="car">
              Select car:{' '}
              <Button
                size="sm"
                variant="outline-success"
                onClick={() => {
                  handleCarSearch(selectedCustomer.id);
                }}
              >
                Show cars:
              </Button>
            </h5>

            <ListGroup
              onChange={(ev) => {
                setSelectedCar(
                  cars.find((car) => car.car_id === +ev.target.id)
                );
              }}
            >
              {cars.map((car) => (
                <ListGroup.Item key={car.car_id}>
                  <span>
                    {' '}
                    <Form.Check
                      inline
                      name="group2"
                      type="radio"
                      id={+car.car_id}
                    />
                    {car.brand} {car.model} {car.registration_plate}{' '}
                  </span>
                </ListGroup.Item>
              ))}
            </ListGroup>
          </div>
        </ListGroup.Item>
        <ListGroup.Item>
          <h5 id="description">Description:</h5>
          <Formik
            validationSchema={schema}
            initialValues={{
              description: '',
            }}
          >
            {({ handleBlur, errors, touched, values }) => (
              <Form noValidate>
                <Form.Group>
                  <Form.Control
                    as="textarea"
                    type="text"
                    name="description"
                    value={values.description}
                    onChange={(ev) => {
                      setDescription(ev.target.value);
                      values.description = ev.target.value;
                    }}
                    isInvalid={touched.description && !!errors.description}
                    onBlur={handleBlur}
                  />
                  <Form.Control.Feedback type="invalid">
                    {errors.description}
                  </Form.Control.Feedback>
                </Form.Group>
              </Form>
            )}
          </Formik>
        </ListGroup.Item>
        <ListGroup.Item>
          <h5 id="parts">Parts:</h5>
          <Form inline className="search-field">
            <FormControl
              
              type="text"
              placeholder="Search Parts"
              onChange={(ev) => setSearchParts(ev.target.value)}
            />
            <Button
              
              variant="outline-success"
              onClick={() => handlePartsSearch(searchParts)}
            >
              Search
            </Button>
          </Form>
          <div className="content parts-seo">
            {partItemsCatalogue.map((partItem) => (
              <PartItem
                key={partItem.id}
                partItemData={{
                  ...partItem,
                  car:
                    partItem.car_specific === 0
                      ? 'all'
                      : carBrands.filter(
                          (car) => car.id === partItem.models_id
                        )[0].name,
                }}
                addPartToOrder={addPartToOrder}
              />
            ))}
          </div>
        </ListGroup.Item>
        <ListGroup.Item>
          <h5 id="works">Works:</h5>
          <Form inline className="search-field">
            <FormControl
              
              type="text"
              placeholder="Search Works"
              onChange={(ev) => setSearchWorks(ev.target.value)}
            />

            <Button
              
              variant="outline-success"
              onClick={() => handleWorksSearch(searchWorks)}
            >
              Search
            </Button>
          </Form>
          <div className="content works-seo">
            {workItemsCatalogue.map((workItem) => (
              <WorkItem
                key={workItem.id}
                workItemData={{
                  ...workItem,
                  price: rates.filter(
                    (rate) => workItem.rates_id === rate.id
                  )[0].price,
                  priceCategory: rates.filter(
                    (rate) => workItem.rates_id === rate.id
                  )[0].name,
                }}
                addWorktoOrder={addWorktoOrder}
              />
            ))}
          </div>
        </ListGroup.Item>
      </ListGroup>
    </div>
  );
};

export default EditCreateServiceOrder;
