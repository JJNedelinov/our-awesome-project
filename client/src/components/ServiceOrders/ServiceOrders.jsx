import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import NavDropdown from 'react-bootstrap/NavDropdown';
import Form from 'react-bootstrap/Form';
import FormControl from 'react-bootstrap/FormControl';
import Button from 'react-bootstrap/Button';
import { useEffect, useState } from 'react';
import { BASE_URL } from '../../common/config';
import {
  getToken,
  getUser,
} from '../../providers/authentication/authentication';
import ServiceOrder from './ServiceOrder';

const ServiceOrders = ({ history }) => {
  const [serviceOrders, setServiceOrders] = useState([]);
  const [serviceOrdersSearch, setServiceOrdersSearch] = useState('');
  const [carId, setCarId] = useState('');
  const [dateFilter, setDateFilter] = useState({ fromDate: '', toDate: '' });
  const [customerCars, setCustomerCars] = useState([]);
  const isAdmin =
    getUser().role === 'admin' || getUser().role === 'employee' ? true : false;
  const customerId = getUser().sub;

  const getCustomerCars = (servOrders) => {
    return servOrders.reduce(
      (carsArray, servOrder) => {
        if (carsArray.some((car) => car.id === servOrder.car.id)) {
          return carsArray;
        }
        return (carsArray = [...carsArray, servOrder.car]);
      },
      [servOrders[0] && servOrders[0].car]
    );
  };
  useEffect(() => {
    const myHeaders = new Headers();
    myHeaders.append('Authorization', `Bearer ${getToken()}`);

    const requestOptions = {
      method: 'GET',
      headers: myHeaders,
    };

    fetch(
      `${BASE_URL}/service-orders${
        !isAdmin ? `?isExact=true&customers_id=${customerId}` : ''
      }`,
      requestOptions
    )
      .then((response) => response.json())
      .then((data) => {
        setServiceOrders(data.data);
        return data;
      })
      .then((data) => setCustomerCars(getCustomerCars(data.data)))
      .catch((error) => console.log('error', error));
  }, [customerId, isAdmin]);

  const handleServiceOrdersSearch = () => {
    const myHeaders = new Headers();
    myHeaders.append('Authorization', `Bearer ${getToken()}`);
    const requestOptions = {
      method: 'GET',
      headers: myHeaders,
    };

    fetch(
      `${BASE_URL}/service-orders?search=${serviceOrdersSearch}`,
      requestOptions
    )
      .then((response) => response.json())
      .then((data) => {
        console.log(customerId)
        const filtered = data.data.filter(
          (order) => +order.serviceOrder.customers_id === +customerId
        );
        setServiceOrders(filtered);
      })
      .catch((error) => console.log('error', error));
  };

  const updateStatus = (serviceOrderId, newStatus) => {
    const myHeaders = new Headers();
    myHeaders.append('content-type', 'application/json');
    myHeaders.append('authorization', `Bearer ${getToken()}`);
    const raw = JSON.stringify({
      status: `${newStatus}`,
    });
    const requestOptions = {
      method: 'PUT',
      headers: myHeaders,
      body: raw,
    };

    fetch(`${BASE_URL}/service-orders/${serviceOrderId}`, requestOptions).catch(
      (error) => console.log('error', error)
    );
  };

  const handleFilter = () => {
    const myHeaders = new Headers();
    myHeaders.append('Authorization', `Bearer ${getToken()}`);
    const requestOptions = {
      method: 'GET',
      headers: myHeaders,
    };

    fetch(
      `${BASE_URL}/service-orders?visitDateMin=${
        dateFilter.fromDate || ''
      }&visitDateMax=${dateFilter.toDate || ''}&cars_id=${carId}
      `,
      requestOptions
    )
      .then((response) => response.json())
      .then((data) => setServiceOrders(data.data))
      .catch((error) => console.log('error', error));
  };

  return (
    <>
      <div id="service-orders">
        <Navbar expand="lg" className="service-bar">
          <Navbar.Brand style={{ paddingBottom: 0 }}>
            <h3>Service Orders</h3>
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            {isAdmin && (
              <Button
                className="new-order-btn"
                // size="sm"
                // variant="link"
                onClick={() =>
                  history.push({
                    pathname: '/edit-create-service-order',
                  })
                }
              >
                New Order
              </Button>
            )}
            <Nav className="mr-auto">
              {!isAdmin && (
                <NavDropdown title="Car" id="basic-nav-dropdown">
                  {customerCars.map((car) => (
                    <NavDropdown.Item
                      onClick={() => setCarId(car && car.id)}
                      key={car && car.id}
                    >
                      <span>{car && car.brand}</span>{' '}
                      <span>{car && car.model}</span>
                    </NavDropdown.Item>
                  ))}
                </NavDropdown>
              )}
              {!isAdmin && (
                <>
                  <NavDropdown title="Date" id="basic-nav-dropdown">
                    <Form inline>
                      <FormControl
                        type="date"
                        value={dateFilter.fromDate}
                        onChange={(ev) =>
                          setDateFilter({
                            ...dateFilter,
                            fromDate: ev.target.value,
                          })
                        }
                      />
                      <FormControl
                        type="date"
                        value={dateFilter.toDate}
                        onChange={(ev) =>
                          setDateFilter({
                            ...dateFilter,
                            toDate: ev.target.value,
                          })
                        }
                      />
                    </Form>
                  </NavDropdown>
                  <Button
                    size="m"
                    variant="link"
                    onClick={() => handleFilter()}
                  >
                    Filter
                  </Button>
                  <Button
                    size="m"
                    variant="link"
                    onClick={() => {
                      handleServiceOrdersSearch();
                      setCarId('');
                      setDateFilter({ fromDate: '', toDate: '' });
                    }}
                  >
                    Reset
                  </Button>
                </>
              )}
            </Nav>

            <NavDropdown.Divider />

            <Form inline>
              <FormControl className="last-minute"
                // size="sm"
                type="text"
                placeholder="Search..."
                onChange={(ev) => setServiceOrdersSearch(ev.target.value)}
              />
              <Button
                // size="m"
                // variant="link"
                onClick={handleServiceOrdersSearch}
              >
                Search
              </Button>
            </Form>
          </Navbar.Collapse>
        </Navbar>
        {serviceOrders.map((serviceOrder) => (
          <ServiceOrder
            serviceOrder={serviceOrder}
            updateStatus={updateStatus}
            key={serviceOrder.serviceOrder.id}
          />
        ))}
      </div>
    </>
  );
};

export default ServiceOrders;
