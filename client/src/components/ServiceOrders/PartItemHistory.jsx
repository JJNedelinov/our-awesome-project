import ListGroup from "react-bootstrap/ListGroup";
import Card from "react-bootstrap/Card";
import ShowPrice from "../ShowPrice";
import { FxContext } from "../../providers/fx/FxContext";
import { useContext } from "react";

const PartItemHistory = ({ partData }) => {
  const { fxData } = useContext(FxContext);
  return (
    <Card className="custom-card">
      <Card.Header className="custom-card-header">{partData.name}</Card.Header>
      <ListGroup variant="flush">
        <ListGroup.Item>Manufacturer: {partData.brand}</ListGroup.Item>
        <ListGroup.Item>Part Number: {partData.product}</ListGroup.Item>
        <ListGroup.Item>
          Unit Price:{" "}
          <ShowPrice
            amount={partData.unit_price}
            targetCurrency={fxData.currentCurrency}
          />
        </ListGroup.Item>
        <ListGroup.Item>
          Total {partData.unit}: {partData.total_units}
        </ListGroup.Item>
        <ListGroup.Item>
          Total Price:{" "}
          <ShowPrice
            amount={partData.unit_price * partData.total_units}
            targetCurrency={fxData.currentCurrency}
          />
        </ListGroup.Item>
      </ListGroup>
    </Card>
  );
};

export default PartItemHistory;
