/* eslint-disable default-case */
import ProgressBar from "react-bootstrap/ProgressBar";
import constants from "../../common/constants";

const ProgressBarLocal = ({ status }) => {
  const now = (status) => {
    switch (status) {
      case "New":
        return constants.SERVICE_ORDER_STATUSES.New;
      case "Scheduled":
        return constants.SERVICE_ORDER_STATUSES.Scheduled;
      case "In Progress":
        return constants.SERVICE_ORDER_STATUSES["In Progress"];
      case "Quality Check":
        return constants.SERVICE_ORDER_STATUSES["Quality Check"];
      case "RFA":
        return constants.SERVICE_ORDER_STATUSES.RFA;
      case "Accepted":
        return constants.SERVICE_ORDER_STATUSES.Accepted;
      case "Paid":
        return constants.SERVICE_ORDER_STATUSES.Paid;
    }
  };
  return <ProgressBar animated now={now(status)} label={`${status}`} />;
};
export default ProgressBarLocal;
