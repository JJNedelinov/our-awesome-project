import ListGroup from "react-bootstrap/ListGroup";
import Card from "react-bootstrap/Card";
import ShowPrice from "./../ShowPrice";
import { FxContext } from "../../providers/fx/FxContext";
import { useContext } from "react";

const WorkItemHistory = ({ workData, rates }) => {
  const { fxData } = useContext(FxContext);
  return (
    <Card className="custom-card">
      <Card.Header className="custom-card-header">{workData.name}</Card.Header>
      <ListGroup variant="flush">
        <ListGroup.Item>
          Unit Price:{" "}
          <ShowPrice
            amount={
              rates.find((rate) => rate.works_history_id === workData.id).price
            }
            targetCurrency={fxData.currentCurrency}
          />
        </ListGroup.Item>
        <ListGroup.Item>Man-hours: {workData.man_hours}</ListGroup.Item>
        <ListGroup.Item>
          Total Price:{" "}
          <ShowPrice
            amount={
              rates.find((rate) => rate.works_history_id === workData.id)
                .price * workData.man_hours
            }
            targetCurrency={fxData.currentCurrency}
          />
        </ListGroup.Item>
      </ListGroup>
    </Card>
  );
};

export default WorkItemHistory;
