import Jumbotron from 'react-bootstrap/Jumbotron';
import ListGroup from 'react-bootstrap/ListGroup';
import DropdownButton from 'react-bootstrap/DropdownButton';
import Dropdown from 'react-bootstrap/Dropdown';
import ButtonGroup from 'react-bootstrap/ButtonGroup';
import PartItemHistory from './PartItemHistory';
import { useContext, useState } from 'react';
import WorkItemHistory from './WorkItemHistory';
import ProgressBarLocal from './ProgressBarLocal';
import constants from '../../common/constants';
import { FxContext } from '../../providers/fx/FxContext';
import ShowPrice from '../ShowPrice';
import { getUser } from '../../providers/authentication/authentication';

const ServiceOrder = ({ serviceOrder, updateStatus }) => {
  const isAdmin = getUser().role === 'admin' ? true : false;

  const [stat, setStat] = useState(serviceOrder.serviceOrder.status);
  const { fxData } = useContext(FxContext);

  const calcTotalWorks = ({ works, rates } = serviceOrder) => {
    const totalWorks = works.length
      ? works.reduce(
          (total, work) =>
            (total +=
              work.man_hours *
              rates.find((rate) => +rate.works_history_id === +work.id).price),
          0
        )
      : 0;

    return totalWorks;
  };

  const calcTotalParts = ({ parts } = serviceOrder) => {
    const totalParts = parts.length
      ? parts.reduce(
          (total, part) => (total += part.unit_price * part.total_units),
          0
        )
      : 0;

    return totalParts;
  };

  return (
    <Jumbotron
      className="service-order"
      key={serviceOrder.serviceOrder.id}
      fluid
    >
      <h4>
        <span className="service-order-header">
          {serviceOrder.customer.first_name} {serviceOrder.customer.last_name}
        </span>
        <span className="service-order-header">
          {serviceOrder.car.brand} {serviceOrder.car.model}
        </span>
        <span className="service-order-header">
          {serviceOrder.car.registration_plate}
        </span>
        {isAdmin && (
          <ButtonGroup aria-label="Basic example">
            <DropdownButton
              as={ButtonGroup}
              variant="link"
              title="Change Status"
            >
              {Object.keys(constants.SERVICE_ORDER_STATUSES).map(
                (statusName) => (
                  <Dropdown.Item
                    onClick={() => {
                      updateStatus(serviceOrder.serviceOrder.id, statusName);
                      setStat(statusName);
                    }}
                  >
                    {statusName}
                  </Dropdown.Item>
                )
              )}
            </DropdownButton>
          </ButtonGroup>
        )}
      </h4>

      <ListGroup>
        <ListGroup.Item>
          <ProgressBarLocal status={stat} />
        </ListGroup.Item>
        <ListGroup.Item>
          <h5>
            <span className="service-order-header">
              <span>Total price: </span>
              <ShowPrice
                amount={
                  calcTotalWorks(serviceOrder) + calcTotalParts(serviceOrder)
                }
                targetCurrency={fxData.currentCurrency}
              />
            </span>
            <span className="service-order-header">
              Visit Date:{' '}
              {serviceOrder.serviceOrder.visit_date.substring(0, 10)}
            </span>
          </h5>
        </ListGroup.Item>
        <ListGroup.Item>
          <h5>Description:</h5>
          <span>{serviceOrder.serviceOrder.description}</span>
        </ListGroup.Item>
        <ListGroup.Item>
          {' '}
          <h5 id="works">
            Parts:{' '}
            <ShowPrice
              amount={calcTotalParts(serviceOrder)}
              targetCurrency={fxData.currentCurrency}
            />
          </h5>
          <div className="service-orders-items">
            {serviceOrder.parts.map((partData) => (
              <PartItemHistory key={partData.id} partData={partData} />
            ))}
          </div>
        </ListGroup.Item>
        <ListGroup.Item>
          <h5>
            Works:{' '}
            <ShowPrice
              amount={calcTotalWorks(serviceOrder)}
              targetCurrency={fxData.currentCurrency}
            />
          </h5>
          <div className="service-orders-items">
            {serviceOrder.works.map((workData) => (
              <WorkItemHistory
                key={workData.id}
                workData={workData}
                rates={serviceOrder.rates}
              />
            ))}
          </div>
        </ListGroup.Item>
      </ListGroup>
    </Jumbotron>
  );
};

export default ServiceOrder;
