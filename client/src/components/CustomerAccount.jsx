import UserServiceOrders from './UserAccount/UserServiceOrders';
import UserCar from './UserAccount/UserCar';
import UserInfo from './UserAccount/UserInfo';

const CustomerAccount = (props) => {
  return (
    <>
      <div className="page-container account-page">
        <div className="user-account">
          <UserInfo {...props} />
        </div>

        <div className="car-container">
          <UserCar {...props} />
        </div>

        <div className="service-order-container">
          <UserServiceOrders {...props} />
        </div>
      </div>
    </>
  );
};

export default CustomerAccount;

/* 



*/
