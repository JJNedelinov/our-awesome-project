import { useState } from 'react';
import { useEffect } from 'react';
import LoginSelect from './LoginSelect';
import LoginForm from './LoginForm';
import { BASE_URL } from '../common/config';
import AuthContext, {
  getUser,
} from '../providers/authentication/authentication';
import { useContext } from 'react';
import { toast } from 'react-toastify';

const Login = (props) => {
  const [email, updateEmail] = useState('');
  const [password, updatePassword] = useState('');
  const [loginOption, updateLoginOption] = useState('');

  const { isLoggedIn, setAuthState } = useContext(AuthContext);

  const submitLoginForm = (e) => {
    e.preventDefault();

    fetch(`${BASE_URL}/${`${loginOption}s/login`}`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ email, password }),
    })
      .then((res) => res.json())
      .then((res) => {
        const { data, error } = res;

        if (error.length) throw new Error(error.join('\n'));

        const token = data[0].token;

        toast.success('You have successfully logged in to your account!', {
          autoClose: 3000,
        });
        localStorage.setItem('token', token);
        setAuthState({
          isLoggedIn: !!getUser(token),
          user: getUser(token),
        });
      })
      .catch((error) => {
        toast.error(error.message, { autoClose: 3000 });
      });
  };

  useEffect(() => {
    let mounted = true;

    if (mounted && isLoggedIn) {
      props.history.push('/');
    }

    return () => (mounted = false);
  }, [isLoggedIn, props.history]);

  useEffect(() => {
    let mounted = true;

    if (mounted && loginOption) {
      props.history.push(`/login/as-${loginOption}`);
    } else if (mounted && !loginOption) {
      props.history.push('/login');
    }

    return () => {
      mounted = false;
      // updateLoginOption('');
    };
  }, [loginOption, props.history]);

  return loginOption ? (
    <LoginForm
      {...props}
      submitLoginForm={submitLoginForm}
      updateEmail={updateEmail}
      updatePassword={updatePassword}
      loginOption={loginOption}
      updateLoginOption={updateLoginOption}
    />
  ) : (
    <LoginSelect {...props} updateLoginOption={updateLoginOption} />
  );
};

export default Login;
