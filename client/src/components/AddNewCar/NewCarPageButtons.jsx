import Button from 'react-bootstrap/Button';

const NewCarPageButtons = (props) => {
  return (
    <div>
      <Button
        variant="primary"
        className="filter-submit"
        disabled={!props.addCar}
        onClick={(e) => props.handleAddCar(e)}
      >
        Add Car
      </Button>
    </div>
  );
};

export default NewCarPageButtons;
