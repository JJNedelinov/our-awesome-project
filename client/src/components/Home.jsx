import { useContext, useState } from 'react';
import Col from 'react-bootstrap/esm/Col';
import { NavLink } from 'react-router-dom';
import { toast } from 'react-toastify';
import * as yup from 'yup';
import { BASE_URL } from '../common/config';
import AuthContext from '../providers/authentication/authentication';

const Home = (props) => {
  const { isLoggedIn } = useContext(AuthContext);

  const [email, updateEmail] = useState('');
  const [question, updateQuestion] = useState('');

  const sendEmail = () => {
    fetch(`${BASE_URL}/send-us-email`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ email, question }),
    })
      .then((res) => res.json())
      .then((res) => {
        const { data, error } = res;

        if (error.length) throw new Error(error.join('\n'));

        updateEmail('');
        updateQuestion('');

        toast.success(data[0], { autoClose: 3000 });
      })
      .catch((e) => toast.error(e.message, { autoClose: 3000 }));
  };

  // console.log(props);
  const schema = yup.object().shape({
    email: yup.string().email('Invalid email.').required('Email is required.'),
    question: yup
      .string()
      .min(
        100,
        'Too Short! Your questions should have at least 100 characters.'
      )
      .max(500, 'Your question must be 500 characters or less.')
      .required('Required'),
  });

  return (
    <>
      <Col className="main-page-header">
        <div className="main-page-content">
          <h1>
            Welcome to <span>Tip-Top Garage</span>
          </h1>
          <p>
            The place where the trust is high{' '}
            <span>and results always Tip-Top</span>
          </p>
          <div className="main-page-header-btn">
            {isLoggedIn ? null : (
              <NavLink className="btn-login" to="/login">
                Login
              </NavLink>
            )}
          </div>
        </div>
      </Col>
      <div id="services">
        <div className="main-page-services-header">
          <h2>Services</h2>
          {/* <h5>We offer many services, but some of them are:</h5> */}
        </div>
        <div className="services-intro">
          {/* <div>
            <Button>View All</Button>
          </div> */}
          {/* <h4>
            Services. Okay <br /> BUT <b>WHO ARE WE</b>?
          </h4> */}
          <p>
            TipTop Garage offers repair, maintenance and tunning services on all
            common makes and models. We also specialize in high-performance
            road-legal cars.
          </p>
          <p>
            We are a one-stop-shop for your family sedan or your tuned GT-R.
            This means that if you maintain your vehicle with us, we will
            diligently assess all components of your vehicle and perform all
            required maintenance at the correct intervals, as per manufacturer
            specifications so that your vehicle is fully prepared for the road.
            Your chances of having to deal with the inconvenience of getting
            your vehicle towed in to perform a repair are minimized.
          </p>
          <p>
            Our technicians have decades of years of experience in inspecting,
            diagnosing and repairing all safety-critical car systems: brakes,
            engine, transmission, steering, suspension, tyres.
          </p>
          <p>
            We also excel in troubleshooting and solving all types of electrical
            and software malfunctions. We work with certified systems/software
            and our diagnostic consultants are trained according to the
            manufacturer's recommendations.
          </p>
        </div>
      </div>
      <div id="about-us">
        {/* <div> */}
        <h2>Who Are We?</h2>
        <div className="about-us-part-one">
          <div className="about-us-paragraphs">
            <p>
              It all started on one weirdly hot spring day when the two friends{' '}
              <b>JJ </b>
              and <b>MadJo</b>, were working on decreasing the weight of their
              race car - <b>Nissan Skyline GT-R R34</b>.
            </p>
            <p>
              It was two months before one of the most significant car events in
              Japan - <b>Tokyo Drift</b>.
            </p>
            <p>
              Since they did not have any extra funds to buy a garage, they were
              doing all of the repairings in JJ's garage in his hometown{' '}
              <b>Montana, Bulgaria</b>.
            </p>
            <p>
              They were planning to buy a garage with the money from the race
              once they win it.
            </p>
            <p>
              The weirdest thing... and when we say '<b>THE WEIRDEST THING</b>'
              that happened during that day was the unexpected visit of Ken
              Block.
            </p>
          </div>
          <div className="about-us-img-container">
            <img
              src="http://static1.squarespace.com/static/568ac8fb4bf118b4ed67932a/56ba172a60b5e9e762f73a91/56ba18885559863eb9a62593/1524493055253/?format=1500w"
              alt="the-nissan"
            />
          </div>
        </div>
        <hr />
        {/* <br /> */}
        <div className="about-us-part-two">
          <div className="about-us-paragraphs">
            <p>
              <b>KEN BLOCK - THE KING OF DRIFT. </b>
            </p>
            <p>
              <b>THE GUY WHO THEY SAID HAS DIED IN A CAR CRASH IN 2018!!!</b>
            </p>
            <p>
              He said that he had seen a few of their viral <b>clips</b> where
              they were
              <b> escaping the police by drifting on the streets of Dubai</b>.
            </p>
            <p>
              There were many <b>bounties</b> on their heads in that country,
              and you can guess why.
            </p>
            <p>These guys were bad.</p>
            <p>
              <b>REAL BAD!</b>
            </p>
            <p>
              That is how they started earning respect on the streets and not
              only there.
            </p>
          </div>
          <div className="about-us-img-container">
            {/* https://static0.hotcarsimages.com/wordpress/wp-content/uploads/2021/04/Hoonicorn-via-Hoonigan.jpg */}
            <img
              src="https://static0.hotcarsimages.com/wordpress/wp-content/uploads/2021/04/Hoonicorn-via-Hoonigan.jpg"
              alt="the-nissan"
            />
          </div>
        </div>
        <hr />
        {/* <br /> */}
        <div className="about-us-part-one">
          <div className="about-us-paragraphs">
            <p>
              Both of them were{' '}
              <b>software engineers during the day and drifters at night</b>.
              They had access to every gala dinner and every private meeting
              because of the software solutions they provided the <b>world</b>{' '}
              with, such as
              <b> skin regeneration, consciousness transfer</b>, and many more.
            </p>
            <p>
              But for a guy like Ken Block to visit them in their tiny garage
              was a<b> DREAM COME TRUE</b>.
            </p>
            <p>
              <b>HE INSPIRED THEM!</b>
            </p>
            <p>
              Since that day, they <b>won the Tokyo Drift car event</b>,{' '}
              <b>opened </b>
              their first garage in the <b>heart of Sofia, Bulgaria</b> -{' '}
              <b>Tip-Top Garage</b>, grew so big that nowadays they are on{' '}
              <b>3 continets</b> doing what they love, and doing it for more
              than <b>200 000</b> customers.
            </p>
            <p>
              Shortly, that is how <b>Tip-Top Garage</b> was created. We are
              hoping that if you have any issues with your vehicle/s or just
              want to say hi, visit us in our main garage where JJ and MadJo
              still work in.
            </p>
          </div>
          <div className="about-us-img-container">
            <img
              src="https://i.pinimg.com/originals/7e/84/ef/7e84efb67b7831d47a9432e8e4745810.jpg"
              alt="our-shop"
            />
          </div>
        </div>
      </div>

      {/* <div id="brands-we-serve">
        <h2>Brands We Serve</h2>
      </div> */}
      <div id="contacts">
        <h2>Contacts</h2>
        <div className="contacts-content">
          <div className="contacts-map">
            <iframe
              title="google map"
              src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2934.5351448264223!2d23.377095115754667!3d42.65001382473286!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40aa86940322e46b%3A0x9dcf2a89d173b821!2sTelerik%20Academy!5e0!3m2!1sen!2sbg!4v1622107461640!5m2!1sen!2sbg"
              alt="map"
            ></iframe>
          </div>
          <div className="contacts-info">
            <div className="contacts-info-content">
              <div>
                <h5>Email:</h5>
                <p>
                  <b>tip.top.garage007@gmail.com</b>
                </p>
                <h5>Phone:</h5>
                <p>
                  <b>+359 878 745 722</b>
                </p>
              </div>
              <div>
                <h5>Working Hours:</h5>
                <p>
                  <b>Monday - Friday</b> - From <b>7 AM to 7 PM</b>
                </p>
                <p>
                  <b>Saturday</b> - From <b>10 AM to 5 PM</b>
                </p>
                <p>
                  <b>Sunday</b> -<b> We are hosting pool parties</b>
                </p>
              </div>
            </div>
            <div className="contacts-email">
              <input
                type="email"
                name="email"
                placeholder="Your Email..."
                value={email && email}
                onChange={(e) => updateEmail(e.target.value)}
              ></input>
              {/* <button className="btn btn-primary">Submit</button> */}
            </div>
            <div className="contacts-question">
              <textarea
                name="question"
                placeholder="Your Question or Just Say Hi..."
                value={question && question}
                onChange={(e) => updateQuestion(e.target.value)}
              ></textarea>
            </div>
            <div className="contacts-submit">
              <button
                className="btn btn-primary"
                disabled={!email && !question}
                onClick={sendEmail}
              >
                Submit
              </button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Home;
