const Loading = () => {
  return <div className="loading">
    <img src="https://cdn.dribbble.com/users/778626/screenshots/4368760/tire.gif" alt="loading wheel"></img>
  </div>;
};

export default Loading;
