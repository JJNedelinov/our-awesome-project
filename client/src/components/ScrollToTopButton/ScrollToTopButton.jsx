import { useState } from 'react';

const ScrollToTopButton = () => {
  const [visible, setVisible] = useState(false);
  const [phoneView, setPhoneView] = useState(false);

  const toggleVisible = () => {
    const scrolled = document.documentElement.scrollTop;
    if (scrolled > 400 && document.querySelector('body').offsetWidth <= 425) {
      setVisible(true);
      setPhoneView(true);
    } else if (
      scrolled <= 400 &&
      document.querySelector('body').offsetWidth <= 425
    ) {
      setVisible(false);
      setPhoneView(true);
    } else {
      setPhoneView(false);
    }
  };

  // const resizer = () => {
  //   if (document.querySelector('body').offsetWidth <= 425) {
  //     setPhoneView(true);
  //   } else {
  //     setPhoneView(false);
  //   }
  // };

  const scrollToTop = () => {
    window.scrollTo({
      top: 0,
      behavior: 'smooth',
    });
  };

  window.addEventListener('scroll', toggleVisible);
  // window.addEventListener('resize', resizer);

  return (
    <button
      className="scroll-up-button"
      style={{ display: phoneView && visible ? 'inline' : 'none' }}
      onClick={scrollToTop}
    >
      {/* https://img.icons8.com/color/70/000000/send-letter--v1.png */}
      {/* https://img.icons8.com/fluent/70/000000/send-letter.png */}
      <img
        src="https://img.icons8.com/fluent/80/000000/send-letter.png"
        alt="scroll-up-button"
      />
      {/* <span
        style={{
          display: 'block',
          backgroundColor: 'white',
          borderRadius: '.2em',
          padding: '.3em .5em',
          color:'black',
          boxShadow: '0 0 7px 1px black'
        }}
      >
        GO TO TOP
      </span> */}
    </button>
  );
};

export default ScrollToTopButton;
