import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import { NavLink } from 'react-router-dom';

const EmployeeNavigation = (props) => {
  return (
    <Navbar.Collapse id="basic-navbar-nav">
      <Nav className="mr-auto">
        <div className="c-separator"></div>
        <NavLink active exact to="/">
          Home
        </NavLink>
        <hr />
        <NavLink active exact to="/customers">
          Customers
        </NavLink>
        <hr />
        <NavLink active exact to="/service-orders">
          Service Orders
        </NavLink>
        <hr />
        <NavLink active exact to="/catalogue">
          Catalogue
        </NavLink>
        <hr />
        <NavLink to="/update-fx" onClick={(e) => props.handleUpdateFx(e)}>
          Update FX
        </NavLink>
        <hr />
        <NavLink active exact to="/logout" className="last-before-my-acc">
          Logout
        </NavLink>
      </Nav>
    </Navbar.Collapse>
  );
};

export default EmployeeNavigation;
