import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import { NavLink } from 'react-router-dom';

const GuestNavigation = () => {
  return (
    <Navbar.Collapse id="basic-navbar-nav">
      <Nav className="mr-auto">
        <div className="c-separator"></div>
        <NavLink exact to="/">Home</NavLink>
        <hr />
        <a href="#services">Services</a>
        <hr />
        <a href="#about-us">About</a>
        <hr />
        <a href="#contacts">Contacts</a>
        <hr />
        <NavLink to="/login" className="last-before-my-acc">
          Login
        </NavLink>
      </Nav>
    </Navbar.Collapse>
  );
};

export default GuestNavigation;
