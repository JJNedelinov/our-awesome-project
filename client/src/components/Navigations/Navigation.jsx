import Navbar from 'react-bootstrap/Navbar';
import { Link } from 'react-router-dom';
import tipTopGarageLogo from '../../assets/tipTopGarageLogo.png';
import { useContext, useState } from 'react';
import AuthContext from '../../providers/authentication/authentication';
import GuestNavigation from './GuestNavigation';
import CustomerNavigation from './CustomerNavigation';
import EmployeeNavigation from './EmployeeNavigation';
import AdminNavigation from './AdminNavigation';
import UpdateFxModal from '../UpdateFxModal/UpdateFxModal';
import { toast } from 'react-toastify';

const Navigation = () => {
  const { user } = useContext(AuthContext);
  const [show, setShow] = useState(false);

  // switch(user.role) {
  //   case 'customer': return ;
  //   case 'employee': return <CustomerNavigation />
  //   case 'admin': return <CustomerNavigation />
  //   default: return <GuestNavigation />
  // };

  const handleUpdateFx = (e) => {
    e.preventDefault();
    setShow(true);
  };

  const notChangedFx = (message) => {
    toast.info(message, { autoClose: 3000 });
  };

  const updatedFx = (message) => {
    toast.success(message, { autoClose: 3000 });
  };

  return (
    <>
      <UpdateFxModal
        show={show}
        setShow={setShow}
        updatedFx={updatedFx}
        notChangedFx={notChangedFx}
        onHide={() => setShow(false)}
      />
      <div className="custom-nav-container">
        <Navbar expand="lg" className="custom-nav">
          <Navbar.Brand style={{ marginRight: '1.5em' }}>
            <Link to="/">
              <img
                alt=""
                src={tipTopGarageLogo}
                width="180"
                height="70"
                className="d-inline-block align-top"
              />{' '}
            </Link>
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          {user && user.role === 'customer' ? (
            <CustomerNavigation handleUpdateFx={handleUpdateFx} />
          ) : user && user.role === 'employee' ? (
            <EmployeeNavigation handleUpdateFx={handleUpdateFx} />
          ) : user && user.role === 'admin' ? (
            <AdminNavigation handleUpdateFx={handleUpdateFx} />
          ) : (
            <GuestNavigation />
          )}
        </Navbar>
      </div>
    </>
  );
};

export default Navigation;
