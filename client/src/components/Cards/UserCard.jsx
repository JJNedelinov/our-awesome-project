import ListGroup from 'react-bootstrap/ListGroup';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import { useState } from 'react';
import EditUserModal from '../EditUser/EditUserModal';
import { BASE_URL } from '../../common/config';
import { toast } from 'react-toastify';
import AuthContext, {
  getToken,
} from '../../providers/authentication/authentication';
import { useContext } from 'react';

const UserCard = (props) => {
  const { user } = useContext(AuthContext);

  const initialName = `${props.user.first_name[0]}. ${props.user.last_name}`;
  const [show, setShow] = useState(false);

  const deleteRenewSuccess = (message) =>
    toast.success(message, {
      autoClose: 3000,
    });

  const deleteRenewFail = (message) => {
    toast.error(message, {
      autoClose: 3000,
    });
  };

  const handleDelete = () => {
    fetch(`${BASE_URL}${props.match.path}/${props.user.id}`, {
      headers: {
        Authorization: `Bearer ${getToken()}`,
      },
      method: 'DELETE',
    })
      .then((res) => res.json())
      .then((res) => {
        const { error } = res;

        if (error.length) throw new Error(error.join('\n'));

        deleteRenewSuccess(
          `Successfully deleted ${props.match.path
            .slice(1)[0]
            .toUpperCase()}${props.match.path.slice(
            2,
            props.match.path.length - 1
          )} with email - ${props.user.email}`
        );
        props.updateDeleteUser((prev) => prev + 1);
      })
      .catch((error) => deleteRenewFail(error.message));
  };

  const handleRenewal = () => {
    fetch(`${BASE_URL}${props.match.path}/${props.user.id}/renewal`, {
      headers: {
        Authorization: `Bearer ${getToken()}`,
      },
      method: 'PUT',
    })
      .then((res) => res.json())
      .then((res) => {
        const { error } = res;

        if (error.length) throw new Error(error.join('\n'));

        deleteRenewSuccess(
          `Successfully renewed ${props.match.path
            .slice(1)[0]
            .toUpperCase()}${props.match.path.slice(
            2,
            props.match.path.length - 1
          )} with email - ${props.user.email}`
        );
        props.updateDeleteUser((prev) => prev + 1);
      })
      .catch((error) => deleteRenewFail(error.message));
  };

  return (
    <>
      <Card className="custom-card">
        <EditUserModal
          {...props}
          user={props.user}
          setShow={setShow}
          updateEditUser={props.updateEditUser}
          show={show}
          onHide={() => setShow(false)}
        />
        <Card.Header className="custom-card-header">
          <span>
            {initialName} {+user.sub === +props.user.id ? '(You)' : ''}
          </span>

          <div
            style={{ display: user.role === 'admin' ? 'block' : 'none' }}
            className="edit-delete-icons"
          >
            <button onClick={() => setShow(true)}>
              <img
                src="https://img.icons8.com/dusk/512/000000/edit--v1.png"
                alt="edit"
              />
            </button>
            {props.user.deleted ? (
              <button onClick={handleRenewal}>
                <img
                  style={{ maxWidth: '34px' }}
                  src="https://img.icons8.com/plasticine/100/000000/return.png"
                  alt="renew"
                />
              </button>
            ) : (
              <button onClick={handleDelete}>
                <img
                  style={{ maxWidth: '34px' }}
                  src="https://img.icons8.com/plasticine/206/000000/filled-trash.png"
                  alt="delete"
                />
              </button>
            )}
          </div>
        </Card.Header>
        <ListGroup variant="flush">
          <ListGroup.Item>
            <b>Name:</b>{' '}
            <span>{`${props.user.first_name} ${props.user.last_name}`}</span>
          </ListGroup.Item>
          <ListGroup.Item>
            <b>Email:</b> <span>{props.user.email}</span>
          </ListGroup.Item>
          <ListGroup.Item>
            <b>Phone:</b> <span>{props.user.phone}</span>
          </ListGroup.Item>
        </ListGroup>
        <Card.Footer className="custom-card-footer">
          {props.match.path.includes('customers') ? (
            <Button
              onClick={() =>
                props.history.push(`${props.match.path}/${props.user.id}`)
              }
            >
              {props.match.path.slice(1)[0].toUpperCase() +
                props.match.path.slice(2, props.match.path.length - 1)}{' '}
              Details
            </Button>
          ) : null}
        </Card.Footer>
      </Card>
    </>
  );
};

export default UserCard;
