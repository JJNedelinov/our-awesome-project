import noresults from '../assets/noresults.png'

const EmptyResult = (props) => {
  return (
    <div className="empty-result">
      {/* <img src="https://i.pinimg.com/originals/1f/aa/3c/1faa3cda455c865f037d63a223577ab5.png" alt="loading wheel"></img> */}
      <img src={noresults} alt="loading wheel"></img>
    </div>
    // https://i.pinimg.com/originals/1f/aa/3c/1faa3cda455c865f037d63a223577ab5.png
  );
};

export default EmptyResult;