import { useState } from 'react';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';

const FilterCustomersForm = (props) => {
  const [filters, updateFilters] = useState({});

  const handleFilter = (e) => {
    props.updateFilterOpts({ ...filters });
  };

  const clearFilters = (e) => {
    updateFilters({});
    props.updateFilterOpts({});
  };

  return (
    <>
      <Form className="filter-search-form">
        <Form.Group controlId="name">
          <Form.Label>Name</Form.Label>
          <Form.Control
            name="name"
            type="text"
            placeholder="Name"
            value={filters.name ?? ''}
            onChange={(e) =>
              updateFilters((prev) => {
                return { ...prev, name: e.target.value };
              })
            }
          />
        </Form.Group>
        <Form.Group controlId="email">
          <Form.Label>Email</Form.Label>
          <Form.Control
            name="email"
            type="text"
            placeholder="Email"
            value={filters.email ?? ''}
            onChange={(e) =>
              updateFilters((prev) => {
                return { ...prev, email: e.target.value };
              })
            }
          />
        </Form.Group>
        <Form.Group controlId="phone-number">
          <Form.Label>Phone Number</Form.Label>
          <Form.Control
            name="phone"
            type="text"
            placeholder="Phone Number"
            value={filters.phone ?? ''}
            onChange={(e) =>
              updateFilters((prev) => {
                return { ...prev, phone: e.target.value };
              })
            }
          />
        </Form.Group>
        <Form.Group controlId="vehicle-brand">
          <Form.Label>Vehicle Brand</Form.Label>
          <Form.Control
            name="vehicle-brand"
            type="text"
            placeholder="Vehicle Brand"
            value={filters.brand ?? ''}
            onChange={(e) =>
              updateFilters((prev) => {
                return { ...prev, brand: e.target.value };
              })
            }
          />
        </Form.Group>
        <Form.Group controlId="vehicle-model">
          <Form.Label>Vehicle Model</Form.Label>
          <Form.Control
            name="vehicle-model"
            type="text"
            placeholder="Vehicle Model"
            value={filters.model ?? ''}
            onChange={(e) =>
              updateFilters((prev) => {
                return { ...prev, model: e.target.value };
              })
            }
          />
        </Form.Group>
        <Form.Group controlId="visit-date-from">
          <Form.Label>Visit Date (from)</Form.Label>
          <Form.Control
            name="from"
            type="date"
            value={filters.from ?? ''}
            onChange={(e) =>
              updateFilters((prev) => {
                return { ...prev, from: e.target.value };
              })
            }
          />
        </Form.Group>
        <Form.Group controlId="visit-date=to">
          <Form.Label>Visit Date (to)</Form.Label>
          <Form.Control
            name="to"
            type="date"
            value={filters.to ?? ''}
            onChange={(e) =>
              updateFilters((prev) => {
                return { ...prev, to: e.target.value };
              })
            }
          />
        </Form.Group>
      </Form>
      <Button
        variant="primary"
        className="filter-submit"
        onClick={(e) => clearFilters(e)}
      >
        Clear
      </Button>
      <Button
        variant="primary"
        className="filter-submit"
        onClick={(e) => handleFilter(e)}
      >
        Submit
      </Button>
    </>
  );
};

export default FilterCustomersForm;
