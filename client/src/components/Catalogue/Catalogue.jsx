import ListGroup from 'react-bootstrap/ListGroup';
import Form from 'react-bootstrap/Form';
import FormControl from 'react-bootstrap/FormControl';
import Button from 'react-bootstrap/Button';
import { useEffect, useState } from 'react';
import ListGroupItem from 'react-bootstrap/esm/ListGroupItem';
import RateItem from './RateItem';
import WorkItem from './WorkItem';
import PartItem from './PartItem';
import NavCatalogue from './NavCatalogue';
import { BASE_URL } from '../../common/config';
import { getToken } from '../../providers/authentication/authentication';

const Catalogue = ({ history }) => {
  const [parts, setParts] = useState([]);
  const [works, setWorks] = useState([]);
  const [rates, setRates] = useState([]);
  const [searchParts, setSearchParts] = useState('');
  const [searchWorks, setSearchWorks] = useState('');
  const [carBrands, setCarBrands] = useState([]);

  useEffect(() => {
    const myHeaders = new Headers();
    myHeaders.append('Authorization', `Bearer ${getToken()}`);

    const requestOptions = {
      method: 'GET',
      headers: myHeaders,
    };
    fetch(`${BASE_URL}/catalogue/rates`, requestOptions)
      .then((response) => response.json())
      .then((data) => setRates(data.data))
      .catch((error) => console.log('error', error));

    fetch(`${BASE_URL}/brands`, requestOptions)
      .then((response) => response.json())
      .then((data) => setCarBrands(data.data))
      .catch((error) => console.log('error', error));
  }, []);

  const handlePartsSearch = () => {
    const myHeaders = new Headers();
    myHeaders.append('Authorization', `Bearer ${getToken()}`);

    const requestOptions = {
      method: 'GET',
      headers: myHeaders,
    };
    fetch(`${BASE_URL}/catalogue/parts?search=${searchParts}`, requestOptions)
      .then((response) => response.json())
      .then((data) => setParts(data.data))
      .catch((error) => console.log('error', error));
  };

  const handleWorksSearch = () => {
    const myHeaders = new Headers();
    myHeaders.append('Authorization', `Bearer ${getToken()}`);

    const requestOptions = {
      method: 'GET',
      headers: myHeaders,
    };
    fetch(`${BASE_URL}/catalogue/works?search=${searchWorks}`, requestOptions)
      .then((response) => response.json())
      .then((data) => setWorks(data.data))
      .catch((error) => console.log('error', error));
  };

  const deleteWorkItem = (id) => {
    const myHeaders = new Headers();
    myHeaders.append('content-type', 'application/json');
    myHeaders.append('Authorization', `Bearer ${getToken()}`);

    const raw = JSON.stringify({
      deleted: 1,
    });

    const requestOptions = {
      method: 'PUT',
      headers: myHeaders,
      body: raw,
    };

    fetch(`${BASE_URL}/catalogue/works/${id}`, requestOptions).catch((error) =>
      console.log('error', error)
    );

    setWorks([...works.filter((work) => +work.id !== +id)]);
  };

  const deleteRateItem = (id) => {
    const myHeaders = new Headers();
    myHeaders.append('content-type', 'application/json');
    myHeaders.append('Authorization', `Bearer ${getToken()}`);

    const raw = JSON.stringify({
      deleted: 1,
    });

    var requestOptions = {
      method: 'PUT',
      headers: myHeaders,
      body: raw,
    };

    fetch(`${BASE_URL}/catalogue/rates/${id}`, requestOptions)
      .then((response) => response.json())
      .catch((error) => console.log('error', error));
    setRates([...rates.filter((rate) => +rate.id !== +id)]);
  };

  return (
    <>
      <NavCatalogue />
      <ListGroup
        className="catalogue-elements"
        style={{
          minHeight:
            parts.length === 0 ? (works.length === 0 ? '66%' : 'auto') : 'auto',
        }}
      >
        <ListGroupItem id="rates">
          <h4>Rates:</h4>
          <Button
            // variant="outline-danger"
            // size="sm"
            onClick={() =>
              history.push({
                pathname: '/edit-create-rate',
              })
            }
          >
            Add New Rate
          </Button>
          <div className="rates">
            {rates.map((rateItem) => {
              return (
                <RateItem rateItem={rateItem} deleteRateItem={deleteRateItem} />
              );
            })}
          </div>
        </ListGroupItem>
        <ListGroupItem id="parts">
          <h4>Parts:</h4>
          <Form inline className="search-field">
            <FormControl
              // size="sm"
              type="text"
              placeholder="Search All"
              onChange={(ev) => setSearchParts(ev.target.value)}
            />
            <Button
              // size="sm"
              variant="outline-success"
              onClick={handlePartsSearch}
            >
              Search
            </Button>
          </Form>
          <div className="parts">
            {parts.map((partItem) => {
              return <PartItem partItem={partItem} carBrands={carBrands} />;
            })}
          </div>
        </ListGroupItem>
        <ListGroupItem id="works">
          <h4>Works:</h4>
          <Button
            // variant="outline-danger"
            style={{ marginBottom: '1em' }}
            // size="sm"
            onClick={() =>
              history.push({
                pathname: '/edit-create-work',
                state: {
                  rates,
                },
              })
            }
          >
            Аdd New Work
          </Button>
          <Form inline className="search-field">
            <FormControl
              // size="sm"
              type="text"
              placeholder="Search All"
              onChange={(ev) => setSearchWorks(ev.target.value)}
            />

            <Button
              // size="sm"
              variant="outline-success"
              onClick={handleWorksSearch}
            >
              Search
            </Button>
          </Form>
          <div className="works">
            {works.map((workItem) => {
              return (
                <WorkItem
                  workItem={workItem}
                  deleteWorkItem={deleteWorkItem}
                  rates={rates}
                />
              );
            })}
          </div>
        </ListGroupItem>
      </ListGroup>
    </>
  );
};

export default Catalogue;
