import ListGroup from "react-bootstrap/ListGroup";
import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import ButtonGroup from "react-bootstrap/ButtonGroup";
import { withRouter } from "react-router";

const PartItem = ({ partItem, carBrands }) => {
  console.log();
  return (
    <Card>
      <Card.Header>
        <h6>{partItem.name}</h6>
      </Card.Header>
      <ListGroup variant="flush">
        <ListGroup.Item>Manufacturer: {partItem.brand}</ListGroup.Item>
        <ListGroup.Item>Part Number: {partItem.product}</ListGroup.Item>
        <ListGroup.Item>Unit: {partItem.unit}</ListGroup.Item>
        <ListGroup.Item>Unit Price: {partItem.unit_price}</ListGroup.Item>
        <ListGroup.Item>
          Suitable for:{" "}
          {partItem.car_specific
            ? carBrands.find((carBrand) => +partItem.models_id === +carBrand.id)
                .name
            : "All vehicles"}
        </ListGroup.Item>
      </ListGroup>
    </Card>
  );
};

export default withRouter(PartItem);
