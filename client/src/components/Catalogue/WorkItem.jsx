import ListGroup from 'react-bootstrap/ListGroup';
import Card from 'react-bootstrap/Card';
import { withRouter } from 'react-router';

const WorkItem = ({ workItem, history, deleteWorkItem, rates }) => {
  const { price: ratePrice, name: rateCategory } = rates.find(
    (rate) => +rate.id === +workItem.rates_id
  );

  return (
    <Card>
      <Card.Header className="custom-card-header">
        <h6>{workItem.name}</h6>
        <div className="edit-delete-icons">
          <button
            onClick={() =>
              history.push({
                pathname: '/edit-create-work',
                state: {
                  workItem,
                  rateCategory,
                  rates,
                },
              })
            }
          >
            <img
              src="https://img.icons8.com/dusk/512/000000/edit--v1.png"
              alt="edit"
            />
          </button>
          <button>
            <img
              style={{ maxWidth: '34px' }}
              src="https://img.icons8.com/plasticine/206/000000/filled-trash.png"
              alt="delete"
              onClick={() => deleteWorkItem(workItem.id)}
            />
          </button>
        </div>
      </Card.Header>
      <ListGroup variant="flush">
        <ListGroup.Item>Minimum hours: {workItem.min_man_hours}</ListGroup.Item>
        <ListGroup.Item>Rate: {ratePrice}</ListGroup.Item>
        <ListGroup.Item>Rate Category: {rateCategory}</ListGroup.Item>
      </ListGroup>
    </Card>
  );
};

export default withRouter(WorkItem);
