import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import NavDropdown from 'react-bootstrap/NavDropdown';

const NavCatalogue = () => {
  return (
    <Navbar expand="lg" className="catalogue-bar">
      <Navbar.Brand>
        <h3>Catalogue</h3>
      </Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="mr-auto">
          <Nav.Link href="/catalogue/#rates">Rates</Nav.Link>
          <Nav.Link href="/catalogue/#parts">Parts</Nav.Link>
          <Nav.Link href="/catalogue/#works">Works</Nav.Link>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
};
export default NavCatalogue;
