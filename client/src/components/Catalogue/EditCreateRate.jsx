import Form from 'react-bootstrap/Form';
import { Formik } from 'formik';
import * as yup from 'yup';
import Button from 'react-bootstrap/esm/Button';
import Col from 'react-bootstrap/esm/Col';
import { getToken } from '../../providers/authentication/authentication';
import { BASE_URL } from '../../common/config';

const EditCreateRate = ({ location, history }) => {
  const schema = yup.object().shape({
    name: yup
      .string()
      .max(45, 'Must be 45 characters or less.')
      .required('Required'),
    price: yup
      .number()
      .integer('Should be an integer value.')
      .positive('Should be a positive value.')
      .required('Required'),
  });

  const uploadData = (values, id) => {
    const myHeaders = new Headers();
    myHeaders.append('content-type', 'application/json');
    myHeaders.append('Authorization', `Bearer ${getToken()}`);
    const raw = JSON.stringify(values);
    const requestOptions = {
      method: id ? 'PUT' : 'POST',
      headers: myHeaders,
      body: raw,
    };

    fetch(
      id ? `${BASE_URL}/catalogue/rates/${id}` : `${BASE_URL}/catalogue/rates`,
      requestOptions
    ).catch((error) => console.log('error', error));
  };

  return (
    <Formik
      validationSchema={schema}
      onSubmit={(values) =>
        uploadData(values, location.rateItem ? location.rateItem.id : null)
      }
      initialValues={{
        name: location.rateItem ? location.rateItem.name : '',
        price: location.rateItem ? location.rateItem.price : 0,
      }}
    >
      {({ handleSubmit, handleChange, values, touched, errors }) => (
        <>
          <Form noValidate onSubmit={handleSubmit} className="edit-create-rate">
            <Form.Group as={Col} md="4" controlId="validationFormik01">
              <h3>{location.rateItem ? 'Edit' : 'Create'} Rate</h3>
              <Form.Label>Name:</Form.Label>
              <Form.Control
                type="text"
                name="name"
                value={values.name}
                onChange={handleChange}
                isInvalid={touched.name && !!errors.name}
              />
              <Form.Control.Feedback type="invalid">
                {errors.name}
              </Form.Control.Feedback>
            </Form.Group>
            <Form.Group as={Col} md="4" controlId="validationFormik02">
              <Form.Label>Price</Form.Label>
              <Form.Control
                type="number"
                name="price"
                value={values.price}
                onChange={handleChange}
                isInvalid={touched.price && !!errors.price}
              />
              <Form.Control.Feedback type="invalid">
                {errors.price}
              </Form.Control.Feedback>
              <br></br>
              <Button type="submit" onSubmit={handleSubmit}>
                Submit
              </Button>{' '}
              <Button type="button" onClick={() => history.goBack()}>
                Back
              </Button>
            </Form.Group>
          </Form>
        </>
      )}
    </Formik>
  );
};

export default EditCreateRate;
