// import React, { useEffect, useState } from 'react';
import Form from 'react-bootstrap/Form';
import { Formik } from 'formik';
import * as yup from 'yup';
import Button from 'react-bootstrap/esm/Button';
import Col from 'react-bootstrap/esm/Col';
import ListGroup from 'react-bootstrap/ListGroup';
import { useState } from 'react';
import { BASE_URL } from '../../common/config';
import { getToken } from '../../providers/authentication/authentication';

const EditCreateWork = ({ location, history }) => {
  const rates = location.state && location.state.rates && location.state.rates;
  const workItem =
    location.state && location.state.workItem && location.state.workItem;
  const [rateCategory, setRateCategory] = useState(
    location.state.rateCategory || ''
  );
  const schema = yup.object().shape({
    name: yup
      .string()
      .max(255, 'Must be 255 characters or less.')
      .required('Required'),
    min_man_hours: yup
      .number()
      .integer('Should be an integer value.')
      .positive('Should be a positive value.')
      .required('Required'),
  });

  const uploadData = (values, id) => {
    const myHeaders = new Headers();
    myHeaders.append('content-type', 'application/json');
    myHeaders.append('Authorization', `Bearer ${getToken()}`);
    const raw = JSON.stringify(values);
    const requestOptions = {
      method: id ? 'PUT' : 'POST',
      headers: myHeaders,
      body: raw,
    };

    fetch(
      id ? `${BASE_URL}/catalogue/works/${id}` : `${BASE_URL}/catalogue/works`,
      requestOptions
    ).catch((error) => console.log('error', error));
  };

  return (
    <Formik
      validationSchema={schema}
      onSubmit={(values) =>
        uploadData(
          {
            ...values,
            rates_id: rates.find((rate) => rate.name === rateCategory).id,
          },
          workItem ? workItem.id : null
        )
      }
      initialValues={{
        name: workItem ? workItem.name : '',
        min_man_hours: workItem ? workItem.min_man_hours : 0,
      }}
    >
      {({ handleSubmit, handleChange, values, touched, errors }) => (
        <>
          <Form noValidate onSubmit={handleSubmit} className="edit-create-work">
            <Form.Group as={Col} md="4" controlId="validationFormik01">
              <h3>{workItem ? `Edit` : `Create`} Work Item</h3>
              <Form.Label>Name:</Form.Label>
              <Form.Control
                type="text"
                name="name"
                value={values.name}
                onChange={handleChange}
                isInvalid={touched.name && !!errors.name}
              />
              <Form.Control.Feedback type="invalid">
                {errors.name}
              </Form.Control.Feedback>
            </Form.Group>
            <Form.Group as={Col} md="4" controlId="validationFormik02">
              <Form.Label>Minimum Man-hours</Form.Label>
              <Form.Control
                type="number"
                name="min_man_hours"
                value={values.min_man_hours}
                onChange={handleChange}
                isInvalid={touched.min_man_hours && !!errors.min_man_hours}
              />
              <Form.Control.Feedback type="invalid">
                {errors.min_man_hours}
              </Form.Control.Feedback>
            </Form.Group>
            <Form.Group
              as={Col}
              md="4"
              controlId="validationFormik03"
              onClick={(ev) => setRateCategory(ev.target.id)}
            >
              <Form.Label>Rate Category</Form.Label>
              {rates.map((rate) => (
                <ListGroup.Item key={rate.id}>
                  <span>
                    {' '}
                    <Form.Check
                      inline
                      name="group1"
                      type="radio"
                      id={rate.name}
                      checked={rate.name === rateCategory}
                    />
                    Category: {rate.name} {' | '} Price per man-hour:{' '}
                    {rate.price}
                  </span>
                </ListGroup.Item>
              ))}
            </Form.Group>
            <br></br>
            <div>
              <Button type="submit" onSubmit={handleSubmit}>
                Submit
              </Button>{' '}
              <Button type="button" onClick={() => history.goBack()}>
                Cancel
              </Button>
            </div>
          </Form>
        </>
      )}
    </Formik>
  );
};

export default EditCreateWork;
