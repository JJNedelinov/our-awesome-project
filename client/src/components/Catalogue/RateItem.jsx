import ListGroup from 'react-bootstrap/ListGroup';
import Card from 'react-bootstrap/Card';
import { withRouter } from 'react-router';

const RateItem = ({ rateItem, history, deleteRateItem }) => {
  return (
    <Card>
      <Card.Header className="custom-card-header">
        <h6>
          {rateItem.name} {}
        </h6>
        {/* <ButtonGroup aria-label="Basic example" size="sm"> */}
        <div className="edit-delete-icons">
          <button
            onClick={() =>
              history.push({
                pathname: '/edit-create-rate',
                rateItem,
              })
            }
          >
            <img
              src="https://img.icons8.com/dusk/512/000000/edit--v1.png"
              alt="edit"
            />
          </button>
          <button>
            <img
              style={{ maxWidth: '34px' }}
              src="https://img.icons8.com/plasticine/206/000000/filled-trash.png"
              alt="delete"
              onClick={() => deleteRateItem(rateItem.id)}
            />
          </button>
        </div>
        {/* </ButtonGroup> */}
      </Card.Header>
      <ListGroup variant="flush">
        <ListGroup.Item>Price per hour: {rateItem.price}</ListGroup.Item>
      </ListGroup>
    </Card>
  );
};

export default withRouter(RateItem);
