import './App.css';
import 'react-toastify/dist/ReactToastify.css';
import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom';
import Home from './components/Home';
import Register from './components/Register';
import { useState } from 'react';
import Users from './components/Users';
import Login from './components/Login';
import ServiceOrders from './components/ServiceOrders/ServiceOrders';
import Catalogue from './components/Catalogue/Catalogue';
import AuthContext, {
  getUser,
} from './providers/authentication/authentication';
import EditCreateRate from './components/Catalogue/EditCreateRate';
import EditCreateWork from './components/Catalogue/EditCreateWork';
import EditCreateServiceOrder from './components/ServiceOrders/EditCreateServiceOrder';
import withNavigationFooter from './hoc/withNavigationFooter';
import AddNewCustomer from './components/AddNewUser/AddNewCustomer';
import CustomerAccount from './components/CustomerAccount';
import { FxContext } from './providers/fx/FxContext';
import GuardedRoute from './providers/authentication/GuardedRoute';
import GuardedRouteEmployee from './providers/authentication/GuarderRouteEmployee';
import Logout from './components/Logout';
import NewCar from './components/AddNewCar/NewCar';
import GuardedRouteCustomers from './providers/authentication/GuardedRouteCustomers';
import EditCar from './components/EditCar/EditCar';
import ScrollToTopButton from './components/ScrollToTopButton/ScrollToTopButton';
import { ToastContainer } from 'react-toastify';

function App() {
  const [users, updateUsers] = useState([]);

  const [authValue, setAuthState] = useState({
    user: getUser(),
    isLoggedIn: Boolean(getUser()),
  });

  const [fxValue, setFxValue] = useState({
    bgnToUSD: 0.6,
    bgnToEUR: 0.5,
    bgnToBGN: 1,
    timeStamp: '',
    defaultFX: true,
    currentCurrency: 'BGN',
  });

  return (
    <BrowserRouter>
      <FxContext.Provider value={{ fxData: fxValue, setFxData: setFxValue }}>
        <AuthContext.Provider value={{ ...authValue, setAuthState }}>
          <Switch>
            <Route
              path="/"
              exact
              render={(props) =>
                withNavigationFooter(Home)({ ...props, users })
              }
            />
            {/* <Route path="/test-form" component={TestForm} /> */}
            {/* <Route
              path="/update-fx"
              component={withNavigationFooter(FxUpdate)}
            /> */}
            <Route
              path="/service-orders"
              component={withNavigationFooter(ServiceOrders)}
            />
            <Route
              path="/catalogue"
              component={withNavigationFooter(Catalogue)}
            />
            <Route
              path="/edit-create-rate"
              component={withNavigationFooter(EditCreateRate)}
            />
            <Route
              path="/edit-create-work"
              component={withNavigationFooter(EditCreateWork)}
            />
            <Route
              path="/edit-create-service-order"
              component={withNavigationFooter(EditCreateServiceOrder)}
            />
            <Route
              path="/register"
              exact
              render={(props) => (
                <Register {...props} updateUsers={updateUsers} />
              )}
            />
            <Route
              path="/login/as-customer"
              exact
              render={(props) => <Login {...props} />}
            />
            <Route
              path="/login/as-employee"
              exact
              render={(props) => <Login {...props} />}
            />
            <GuardedRouteEmployee
              exact
              path="/customers"
              user={authValue.user}
              isLoggedIn={authValue.isLoggedIn}
              component={withNavigationFooter(Users)}
            />
            <GuardedRouteEmployee
              exact
              path="/employees"
              user={authValue.user}
              isLoggedIn={authValue.isLoggedIn}
              component={withNavigationFooter(Users)}
            />
            <Route
              exact
              path="/employees"
              render={(props) =>
                withNavigationFooter(Users)({ ...props, users })
              }
            />
            <Route
              exact
              path="/add-customer"
              render={(props) =>
                withNavigationFooter(AddNewCustomer)({ ...props, users })
              }
            />

            <Route
              path="/customers/:id"
              exact
              render={(props) =>
                withNavigationFooter(CustomerAccount)({ ...props })
              }
            />
            <Route
              path="/customers/:id/add-car"
              exact
              render={(props) => withNavigationFooter(NewCar)({ ...props })}
            />
            <Route
              path="/customers/:id/edit-car/:carId"
              exact
              render={(props) => withNavigationFooter(EditCar)({ ...props })}
            />
            <GuardedRouteCustomers
              exact
              path="/my-account"
              user={authValue.user}
              isLoggedIn={authValue.isLoggedIn}
              component={withNavigationFooter(CustomerAccount)}
            />
            <Route
              path="/login"
              exact
              render={(props) =>
                authValue.isLoggedIn ? (
                  <Redirect to="/" />
                ) : (
                  <Login {...props} />
                )
              }
            />
            <GuardedRoute
              path="/logout"
              exact
              isLoggedIn={authValue.isLoggedIn}
              component={withNavigationFooter(Logout)}
            />
            {/* <Route component={withNavigationFooter(<NoMatch />)} /> */}
          </Switch>
          <ScrollToTopButton />
          <ToastContainer />
        </AuthContext.Provider>
      </FxContext.Provider>
    </BrowserRouter>
  );
}

export default App;
