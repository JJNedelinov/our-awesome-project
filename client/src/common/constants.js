const SERVICE_ORDER_STATUSES = {
  New: 10,
  Scheduled: 20,
  'In Progress': 40,
  'Quality Check': 70,
  RFA: 85,
  Accepted: 95,
  Paid: 100,
};

const MadJoToken =
  'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsImZ1bGxOYW1lIjoiTWFkSm8gVHN2ZXRrb3YiLCJyb2xlIjoiYWRtaW4iLCJpYXQiOjE2MjI3ODc1NDAsImV4cCI6MjkxODc4NzU0MH0.sCOjKl_bmwZ6LsOpd8KR4cCDi_0UvUVGMfARTel_HgQ';

const carProps = [
  'brand',
  'model',
  'year',
  'coupe',
  'engine',
  'transmission',
  'horse_power',
  'registration_plate',
  'vin',
  // 'car_image',
];

const initialCarObjState = {
  brand: '',
  model: '',
  coupe: '',
  engine: '',
  transmission: '',
  horse_power: '',
  registration_plate: '',
  vin: '',
  car_image: '',
};

const initialUserObjState = {
  first_name: '',
  last_name: '',
  email: '',
  phone: '',
};

const userProps = ['first_name', 'last_name', 'email', 'phone'];

export default {
  SERVICE_ORDER_STATUSES,
  carProps,
  MadJoToken,
  initialCarObjState,
  initialUserObjState,
  userProps,
};
