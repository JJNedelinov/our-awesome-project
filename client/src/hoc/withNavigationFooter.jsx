import React from 'react';
import Navigation from '../components/Navigations/Navigation';
import Footer from '../components/Footer';

const withNavigationFooter = (Component) => (props) => {
  return (
    <>
      <Navigation />
      <Component {...props} />
      <Footer />
    </>
  );
};

export default withNavigationFooter;
