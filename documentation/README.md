# Tip-Top Garage Customer Management Platform

![](tipTopGarageLogo.png)

**Copyright © 2021 Drift Brothers, Inc. All rights reserved.**

**Delivered to you by JJ and MadJo**

## Contents

- [Tip-Top Garage Customer Management Platform](#tip-top-garage-customer-management-platform)
  - [Contents](#contents)
  - [Platform Overview](#platform-overview)
  - [Customers](#customers)
  - [Employees](#employees)
  - [Email Integration](#email-integration)
  - [Catalogue](#catalogue)
    - [Rates](#rates)
    - [Works (services)](#works-services)
    - [Parts](#parts)
  - [Service Orders (Visits)](#service-orders-visits)
  - [FX](#fx)
  - [Search](#search)
  - [History](#history)
  - [System Requirements](#system-requirements)
    - [Technologies](#technologies)
    - [Example hosting requirements](#example-hosting-requirements)
    - [Internet Browser compatibility](#internet-browser-compatibility)
  - [Future improvements](#future-improvements)
    - [Front-end](#front-end)
    - [Back-end](#back-end)
    - [Database](#database)
- [Attachments](#attachments)
    - [DB EER Diagram](#db-eer-diagram)
    - [Back-end Architecture](#back-end-architecture)
    - [Front-end Architecture](#front-end-architecture)

## Platform Overview

Tip-Top Garage Customer Management Platform is a modern web application for managing the daily operations of a car maintenance shop. It delivers digital excellence and mobility for the customers and the employees to enrich the customer service experience and the employee satisfaction.

![](home-page.png)

**The App has the following core features:**

1. Customer profile management, including customer data, vehicle data and service orders history.
2. Catalogue management, offering creating, modifying and deleting of services (works) and services rates.
3. Thorough Service Orders management (visit management).

**The Platform offers the following non-functional features:**

1. The App is fully responsive, including all complex forms and views. This allows the customers and employees to work with the Platform seamlessly from anywhere.
2. The Platform focuses on user experience, offering modals, toast messages and a very simple design.
3. The Platform is fully open-source, single-technology and can be maintained and hosted in a cost-effective fashion.

## Customers

Customers will login into the App using the public part of the Platform (mobile version screenshot):

![](login.png)![](login-details.png)

The Customer is able to fully modify hers/his account details and to view her/his cars and service orders. There is a seamless pagination that displays one car per page and one service order per page:
![](customer-add-new-car.png)
![](customer-service-order.png)

## Employees

Employees and admins can create and modify customers, their vehicles as well as other employee accounts.

Adding a new customer:

![](customer-add-new.png)

Searching and filtering customers:

![](customers-filter.png)

Adding new employee:

![](employee-add.png)

## Email Integration

The access to the Platform is secured via registration using email. When a new user or employee is added, she/he has to check her/his email address and follow the steps to activate her/his account:

![](email.png)

## Catalogue

The catalogue is split into 2 main parts:

- customer-related data (i.e. customers, cars)
- technical data (i.e. works, parts, rates)

Customer-related data is managed via the Customers Wiew. Technical data is managed via the Catalogue View.

The Catalogue currently has the following functionality:

![](catalogue.png)

### Rates

Employees can view, edit and create works rates. Rates pricing is predefined and should not be modified ad-hoc by employees when creating new service orders. Employees are allowed to create new rates categories.

### Works (services)

Employees can view, searching, edit and create work items. Each work item is measured in man-hours and gets a rate category from the rates. For example, the standard rate is 50 BGN/h and a work item can have 4 hours x standard rate = 200 BGN total. The work items have predefined (preferred) quantity of man-hours per work. For example oil change is usually 4 man-hours. The employee can adjust this default quantity when adding a work item to a service order:

![](catalogue-work-item-new.png)

### Parts

Employees can view and searching parts items. Parts are read-only and cannot be deleted modified, as they are usually provided by a 3rd party (it is impossible for the garage to manage all parts for all vehicles). Parts can be car-specific and generic (for all cars). Parts also have default (preferred) qty that can be updated by the employee when creating the service order.

## Service Orders (Visits)

Service Orders are created by the employee when the customer visits the garage:

![](service-order-create.png)

At first, the employee searches for the customer, then for her/his car. After that, the employee enters the description of the service order and searches for relevant parts and works. For each selected part and work the employee can adjust the qty of part items (litters of oil, number of brake pads etc.) and the amount of man-hours per work item. The employee is notified via toast message on the success/fail of each step of the process. Finally, the employee selects submit to save the service order in the system.

The newly created service order has a status "New". The other available statuses track the progress of the service order within the lifecycle. The employee updates the status of a service order and can see the progress bar of each service order, in percentages.

The service order once created shows a thorough information about each work and part line item, the qty of the parts/man-hours, the total price amount of work in the selected currency and the total price amount of parts for the selected currency:

![](service-orders.png)

## FX

The Platform offers to the customers and employees the possibility to change the currency of the amounts shown in the service orders. The currency data is taken from a public API and can be updated by the user ad-hoc. When the currency is changed by the user, the UI changes the localization of the amounts displayed (currency symbol, decimal points etc.):

![](fx-update.png)

## Search

Where applicable the Platform offers quick navigation into the catalogue and service order items by full-text search in all data. For example, a customer can search a service order by a keyword in the description, or an employee can search for works and parts related to "oil".

## History

Due to customer requirements and legislation requirements the system stores all historical data for service orders. No matter what changes are done over time in the catalogue (i.e. rate update, or man-hour qty update for a work item), the service orders history data is immutable. This is achieved via instancing (copying) all service order items (works, parts, rates) into "history" items. In this way, the customer can check that the 'standard' rate was 50 BGN/h 1 year ago and now in her/his recent service order it is 55 BGN/h (history rate data vs actual rate data in the catalogue.).

## System Requirements

### Technologies

The Platform is built using JavaScript for all of its components but the Database. The App has the following main layers:

1. Front-end - React, React-Bootstrap, Yup*, Formik*, HTML, CSS, Fetch API.
2. Back-end - Node.js, Express and MariaDB Connector
3. Database - MariaDB

   _\*Note: Yup and Formik are enabled as PoC and will be fully enabled in the next version_

### Example hosting requirements

The App can run on any hosting provider or on-prem server with the following minimum characteristics:

- Linux-based server / container / Node.js (Express) service offering 16GB RAM and dual-core CPU
- Fully secured edge network: firewall, anti-malware, DDoS etc. services should be enabled by the provider
- Backup and restore tools/storage should be provided. Backup can be performed via extracting the database and copying the source files.

### Internet Browser compatibility

- Chrome, Safari, Firefox, Edge
- All browsers and underlying operating systems should be patched to their last versions and should be supported by the vendor.

## Future improvements

### Front-end

- Further unification of the user experience by leveraging toasts and modals on all user interfaces (i.e. also for employees)
- Improving maintainability by consolidating Fetch requests and improving error handling
- Moving some computations into the back-end (for example getting the rate price for a work item)
- Creating a native app with react-native to leverage push notifications and other native mobile features

### Back-end

- Removing highly-coupled functionalities and unifying them into a holistic approach. For example the implementation of search and filter for employees/customers and catalogue is different.
- Improve error handling for the REST API. Not all error messages are handled in a way not to be shown to the end user.
- Better processing of the data taken from the database. Some data is directly passed to the front-end (rates, works) and can be aggregated in the back-end.

### Database

- Reduce the complexity of some items (catalogue) where certain tables are not needed currently (i.e. rates) and can be merged with other tables (i.e. works).
- Move some functionality (complex queries) from the database to the back-end, if the next version should be database-independent.

# Attachments

### DB EER Diagram

![](db-eer-diagram.png)

### Back-end Architecture

![](architecture.png)

### Front-end Architecture

![](architecture-front-end.png)
